<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQuizzRequest;
use App\Http\Requests\UpdateQuizzRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Quizz;
use Illuminate\Http\Request;
use Flash;
use Response;

class QuizzController extends AppBaseController
{
    /**
     * Display a listing of the Quizz.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Quizz $quizzs */
        $quizzs = Quizz::all();

        return view('quizzs.index')
            ->with('quizzs', $quizzs);
    }

    /**
     * Show the form for creating a new Quizz.
     *
     * @return Response
     */
    public function create()
    {
        return view('quizzs.create');
    }

    /**
     * Store a newly created Quizz in storage.
     *
     * @param CreateQuizzRequest $request
     *
     * @return Response
     */
    public function store(CreateQuizzRequest $request)
    {
        $input = $request->all();

        /** @var Quizz $quizz */
        $quizz = Quizz::create($input);

        Flash::success('Quizz saved successfully.');

        return redirect(route('quizzs.index'));
    }

    /**
     * Display the specified Quizz.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Quizz $quizz */
        $quizz = Quizz::find($id);

        if (empty($quizz)) {
            Flash::error('Quizz not found');

            return redirect(route('quizzs.index'));
        }

        return view('quizzs.show')->with('quizz', $quizz);
    }

    /**
     * Show the form for editing the specified Quizz.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Quizz $quizz */
        $quizz = Quizz::find($id);

        if (empty($quizz)) {
            Flash::error('Quizz not found');

            return redirect(route('quizzs.index'));
        }

        return view('quizzs.edit')->with('quizz', $quizz);
    }

    /**
     * Update the specified Quizz in storage.
     *
     * @param int $id
     * @param UpdateQuizzRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQuizzRequest $request)
    {
        /** @var Quizz $quizz */
        $quizz = Quizz::find($id);

        if (empty($quizz)) {
            Flash::error('Quizz not found');

            return redirect(route('quizzs.index'));
        }

        $quizz->fill($request->all());
        $quizz->save();

        Flash::success('Quizz updated successfully.');

        return redirect(route('quizzs.index'));
    }

    /**
     * Remove the specified Quizz from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Quizz $quizz */
        $quizz = Quizz::find($id);

        if (empty($quizz)) {
            Flash::error('Quizz not found');

            return redirect(route('quizzs.index'));
        }

        $quizz->delete();

        Flash::success('Quizz deleted successfully.');

        return redirect(route('quizzs.index'));
    }
}
