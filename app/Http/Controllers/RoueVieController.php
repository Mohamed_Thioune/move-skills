<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoueVieRequest;
use App\Http\Requests\UpdateRoueVieRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\RoueVie;
use Illuminate\Http\Request;
use Flash;
use Response;

class RoueVieController extends AppBaseController
{
    /**
     * Display a listing of the RoueVie.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var RoueVie $roueVies */
        $roueVies = RoueVie::all();

        return view('roue_vies.index')
            ->with('roueVies', $roueVies);
    }

    /**
     * Show the form for creating a new RoueVie.
     *
     * @return Response
     */
    public function create()
    {
        return view('roue_vies.create');
    }

    /**
     * Store a newly created RoueVie in storage.
     *
     * @param CreateRoueVieRequest $request
     *
     * @return Response
     */
    public function store(CreateRoueVieRequest $request)
    {
        $input = $request->all();

        /** @var RoueVie $roueVie */
        $roueVie = RoueVie::create($input);

        Flash::success('Roue Vie saved successfully.');

        return redirect(route('roueVies.index'));
    }

    /**
     * Display the specified RoueVie.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RoueVie $roueVie */
        $roueVie = RoueVie::find($id);

        if (empty($roueVie)) {
            Flash::error('Roue Vie not found');

            return redirect(route('roueVies.index'));
        }

        return view('roue_vies.show')->with('roueVie', $roueVie);
    }

    /**
     * Show the form for editing the specified RoueVie.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var RoueVie $roueVie */
        $roueVie = RoueVie::find($id);

        if (empty($roueVie)) {
            Flash::error('Roue Vie not found');

            return redirect(route('roueVies.index'));
        }

        return view('roue_vies.edit')->with('roueVie', $roueVie);
    }

    /**
     * Update the specified RoueVie in storage.
     *
     * @param int $id
     * @param UpdateRoueVieRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoueVieRequest $request)
    {
        /** @var RoueVie $roueVie */
        $roueVie = RoueVie::find($id);

        if (empty($roueVie)) {
            Flash::error('Roue Vie not found');

            return redirect(route('roueVies.index'));
        }

        $roueVie->fill($request->all());
        $roueVie->save();

        Flash::success('Roue Vie updated successfully.');

        return redirect(route('roueVies.index'));
    }

    /**
     * Remove the specified RoueVie from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RoueVie $roueVie */
        $roueVie = RoueVie::find($id);

        if (empty($roueVie)) {
            Flash::error('Roue Vie not found');

            return redirect(route('roueVies.index'));
        }

        $roueVie->delete();

        Flash::success('Roue Vie deleted successfully.');

        return redirect(route('roueVies.index'));
    }
}
