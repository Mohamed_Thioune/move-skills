<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Session;
use Mail; 
use Illuminate\Contracts\Encryption\DecryptException;
use Crypt;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Activite;
use App\Models\Parcours_formation;
use Flash;


class ActiviteController extends Controller
{
    //


    public function mes_activites()
    {
        $formateur = DB::table('formateurs')->where('user_id', Auth::user()->id)->first();

        $activites = DB::table('activites')->select('activites.*', 'parcours_formations.formateur_id','parcours_formations.libelle as libelle_perfo',
        'parcours_formations.id as idpar', 'types.libelle as libelle_type','types.id as idtyp')
        ->join('parcours_formations','parcours_formations.id', 'activites.parcours_formation_id')
        ->join('types', 'types.id',  'activites.type_id')
        ->where('parcours_formations.formateur_id', $formateur->id)->orderBy('activites.id', 'desc')->get();
        return view('activite.mes_activites', compact('activites'));
    }

    public function create_activites()
    {
        $types = DB::table('types')->get();
        $parcours = DB::table('parcours_formations')->get();
        return view('activite.creation_activites', compact('types', 'parcours'));
    }

    public function store_activites(Request $request)
    {
        
            $message = "Ajouté avec succès";

            $formateur = DB::table('formateurs')->where('user_id', Auth::user()->id)->first();

             $activite = new Activite;
             $activite->libelle =$request->get('libelle');
             $activite->type_id = $request->get('type_id'); 
             $activite->parcours_formation_id = $request->get('parcours_formation_id'); 
             $activite->video = $request->get('video'); 
             $activite->articles = $request->get('articles'); 
             $activite->audios = $request->get('audios'); 
             $activite->save();

            return back()->with(['message' => $message]);
 
    }

    public function destroy_activites($id)
    {
        $activite = Activite::find($id);
        $activite->delete();
        return back();
    }
}
