<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCitationRequest;
use App\Http\Requests\UpdateCitationRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Citation;
use Illuminate\Http\Request;
use Flash;
use Response;

class CitationController extends AppBaseController
{
    /**
     * Display a listing of the Citation.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Citation $citations */
        $citations = Citation::all();

        return view('citations.index')
            ->with('citations', $citations);
    }

    /**
     * Show the form for creating a new Citation.
     *
     * @return Response
     */
    public function create()
    {
        return view('citations.create');
    }

    /**
     * Store a newly created Citation in storage.
     *
     * @param CreateCitationRequest $request
     *
     * @return Response
     */
    public function store(CreateCitationRequest $request)
    {
        $input = $request->all();

        /** @var Citation $citation */
        $citation = Citation::create($input);

        Flash::success('Citation saved successfully.');

        return redirect(route('citations.index'));
    }

    /**
     * Display the specified Citation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Citation $citation */
        $citation = Citation::find($id);

        if (empty($citation)) {
            Flash::error('Citation not found');

            return redirect(route('citations.index'));
        }

        return view('citations.show')->with('citation', $citation);
    }

    /**
     * Show the form for editing the specified Citation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Citation $citation */
        $citation = Citation::find($id);

        if (empty($citation)) {
            Flash::error('Citation not found');

            return redirect(route('citations.index'));
        }

        return view('citations.edit')->with('citation', $citation);
    }

    /**
     * Update the specified Citation in storage.
     *
     * @param int $id
     * @param UpdateCitationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCitationRequest $request)
    {
        /** @var Citation $citation */
        $citation = Citation::find($id);

        if (empty($citation)) {
            Flash::error('Citation not found');

            return redirect(route('citations.index'));
        }

        $citation->fill($request->all());
        $citation->save();

        Flash::success('Citation updated successfully.');

        return redirect(route('citations.index'));
    }

    /**
     * Remove the specified Citation from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Citation $citation */
        $citation = Citation::find($id);

        if (empty($citation)) {
            Flash::error('Citation not found');

            return redirect(route('citations.index'));
        }

        $citation->delete();

        Flash::success('Citation deleted successfully.');

        return redirect(route('citations.index'));
    }
}
