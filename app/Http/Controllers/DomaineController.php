<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDomaineRequest;
use App\Http\Requests\UpdateDomaineRequest;
use App\Repositories\DomaineRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DomaineController extends AppBaseController
{
    /** @var DomaineRepository $domaineRepository*/
    private $domaineRepository;

    public function __construct(DomaineRepository $domaineRepo)
    {
        $this->domaineRepository = $domaineRepo;
    }

    /**
     * Display a listing of the Domaine.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $domaines = $this->domaineRepository->all();

        return view('domaines.index')
            ->with('domaines', $domaines);
    }

    /**
     * Show the form for creating a new Domaine.
     *
     * @return Response
     */
    public function create()
    {
        return view('domaines.create');
    }

    /**
     * Store a newly created Domaine in storage.
     *
     * @param CreateDomaineRequest $request
     *
     * @return Response
     */
    public function store(CreateDomaineRequest $request)
    {
        $input = $request->all();

        $domaine = $this->domaineRepository->create($input);

        Flash::success('Domaine saved successfully.');

        return redirect(route('domaines.index'));
    }

    /**
     * Display the specified Domaine.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $domaine = $this->domaineRepository->find($id);

        if (empty($domaine)) {
            Flash::error('Domaine not found');

            return redirect(route('domaines.index'));
        }

        return view('domaines.show')->with('domaine', $domaine);
    }

    /**
     * Show the form for editing the specified Domaine.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $domaine = $this->domaineRepository->find($id);

        if (empty($domaine)) {
            Flash::error('Domaine not found');

            return redirect(route('domaines.index'));
        }

        return view('domaines.edit')->with('domaine', $domaine);
    }

    /**
     * Update the specified Domaine in storage.
     *
     * @param int $id
     * @param UpdateDomaineRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDomaineRequest $request)
    {
        $domaine = $this->domaineRepository->find($id);

        if (empty($domaine)) {
            Flash::error('Domaine not found');

            return redirect(route('domaines.index'));
        }

        $domaine = $this->domaineRepository->update($request->all(), $id);

        Flash::success('Domaine updated successfully.');

        return redirect(route('domaines.index'));
    }

    /**
     * Remove the specified Domaine from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $domaine = $this->domaineRepository->find($id);

        if (empty($domaine)) {
            Flash::error('Domaine not found');

            return redirect(route('domaines.index'));
        }

        $this->domaineRepository->delete($id);

        Flash::success('Domaine deleted successfully.');

        return redirect(route('domaines.index'));
    }
}
