<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSentMailAPIRequest;
use App\Http\Requests\API\UpdateSentMailAPIRequest;
use App\Models\SentMail;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SentMailController
 * @package App\Http\Controllers\API
 */

class SentMailAPIController extends AppBaseController
{
    /**
     * Display a listing of the SentMail.
     * GET|HEAD /sentMails
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = SentMail::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $sentMails = $query->get();

        return $this->sendResponse($sentMails->toArray(), 'Sent Mails retrieved successfully');
    }

    /**
     * Store a newly created SentMail in storage.
     * POST /sentMails
     *
     * @param CreateSentMailAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSentMailAPIRequest $request)
    {
        $input = $request->all();

        /** @var SentMail $sentMail */
        $sentMail = SentMail::create($input);

        return $this->sendResponse($sentMail->toArray(), 'Sent Mail saved successfully');
    }

    /**
     * Display the specified SentMail.
     * GET|HEAD /sentMails/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SentMail $sentMail */
        $sentMail = SentMail::find($id);

        if (empty($sentMail)) {
            return $this->sendError('Sent Mail not found');
        }

        return $this->sendResponse($sentMail->toArray(), 'Sent Mail retrieved successfully');
    }

    /**
     * Update the specified SentMail in storage.
     * PUT/PATCH /sentMails/{id}
     *
     * @param int $id
     * @param UpdateSentMailAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSentMailAPIRequest $request)
    {
        /** @var SentMail $sentMail */
        $sentMail = SentMail::find($id);

        if (empty($sentMail)) {
            return $this->sendError('Sent Mail not found');
        }

        $sentMail->fill($request->all());
        $sentMail->save();

        return $this->sendResponse($sentMail->toArray(), 'SentMail updated successfully');
    }

    /**
     * Remove the specified SentMail from storage.
     * DELETE /sentMails/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SentMail $sentMail */
        $sentMail = SentMail::find($id);

        if (empty($sentMail)) {
            return $this->sendError('Sent Mail not found');
        }

        $sentMail->delete();

        return $this->sendSuccess('Sent Mail deleted successfully');
    }
}
