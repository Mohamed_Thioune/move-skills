<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use App\Models\Video;
use App\Models\Audio;
use App\Models\Article;
use App\Models\Challenge;
use App\Models\Podcast;
use App\Models\Citation;
use App\Models\Token;
use App\Models\PasserTest;
use App\Models\CanvaMiniDisq;
use App\Models\ResponseByUserDisc;
use App\Models\Couleur;
use App\Models\Apprenant;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Http\Requests\EditPassword;
use Illuminate\Support\Facades\Hash;

use DB;
use Storage;
use Illuminate\Contracts\Filesystem\Filesystem;
use League\Flysystem\AwsS3v3\AwsS3Adapter;

class UserAPIController extends AppBaseController
{
    /**
     * Update the specified RoueVie in storage.
     * PUT/PATCH /user
     *
     *
     * @return Response
    */
    public function home(Request $request)
    {
        $token = $request->header('Authorization');
        $input = $request->all();
    
        $token_user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();
    
        $input['user_id'] = $token_user->user_id;
    
        $user = User::find($input['user_id']);
    
        if(empty($user))
            return $this->sendError('User not found !');
    
        $infos = array();
        //Inspiration du jour
        $citations = Citation::all()->where('statut_display', 1)->sortDesc()->shuffle()->take(3);
        $infos['citations'] = $citations;
    
        // Ma croissance personelle
        $personal_croissance = array();
         
        $personal_croissance['competences_developped'] = 0;
        //Compétences développées 
        $mes_parcours = DB::table('cinetpay_transactions')
            ->select('parcours_formations.*')
            ->join('parcours_formations', 'cinetpay_transactions.parcours_id', 'parcours_formations.id')
            ->where('cinetpay_transactions.user_id', $user->id)
            ->get();
        $parcours_win = array();
        foreach($mes_parcours as $parcours){
            if(!isset($parcours->id))
                continue;

            $pourcentage = 0;
            $calcul_pourcentage = 0;

            $raw_videos = Video::where('parcours_formation_id', $parcours->id)->get();
            $raw_audios = Audio::where('parcours_formation_id', $parcours->id)->get();
            // $raw_articles = Article::where('parcours_formation_id', $parcours->id)->get();
            $raw_citations = Citation::where('parcours_formation_id', $parcours->id)->get();
            $raw_challenges = Challenge::where('parcours_formation_id', $parcours->id)->get();
            $raw_podcasts = Podcast::where('parcours_formation_id', $parcours->id)->get();

            /** Pourcentage **/
            $score = 0;
            
            //Videos
            $raw_videos = Video::select('id', 'libelle', 'description', 'source', 'created_at', 'updated_at', 'quizz_id')
                        ->where('parcours_formation_id', $parcours->id)
                        ->get();
            foreach($raw_videos as $video){
                $passer_test = PasserTest::where('type', 'quizz')->where('quizz_id', $video->quizz_id)->where('user_id', $user->id)->whereNotNull('point_id')->first();
                if(!empty($passer_test))
                    $score += 1;
            }

            //Audios
            $raw_audios = Audio::select('id', 'libelle', 'description', 'podcast', 'created_at', 'updated_at', 'quizz_id')
                        ->where('parcours_formation_id', $parcours->id)
                        ->get();
            foreach($raw_audios as $audio){
                $passer_test = PasserTest::where('type', 'quizz')->where('quizz_id', $audio->quizz_id)->where('user_id', $user->id)->whereNotNull('point_id')->first();
                if(!empty($passer_test))
                    $score += 1;
            }

            //Podcasts
            $raw_podcasts = Podcast::select('id', 'libelle', 'description', 'podcast', 'created_at', 'updated_at', 'quizz_id')
            ->where('parcours_formation_id', $parcours->id)
            ->get();
            foreach($raw_podcasts as $podcast){
                $read_podcast = DB::table('parcour_activites')
                ->select('parcours_id')
                ->where('type', 'podcast')
                ->where('activity_id', $podcast->id)
                ->where('user_id', $user->id)
                ->first();
        
                if(!empty($read_podcast))
                    $score += 1;

            }

            //Challenges
            $raw_challenges = Challenge::select('id', 'title', 'type', 'created_at', 'updated_at')
            ->where('parcours_formation_id', $parcours->id)
            ->get();
            $pourcentage_challenge = 0;
            foreach($raw_challenges as $challenge){
                $passer_test = PasserTest::where('type', 'challenge')->where('challenge_id', $challenge->id)->where('user_id', $user->id)->whereNotNull('point_id')->first();
                if(!empty($passer_test))
                    $score += 1;
            }

            if(isset($raw_videos[0]))
                $calcul_pourcentage += count($raw_videos);
            
            if(isset($raw_audios[0]))
                $calcul_pourcentage += count($raw_audios);

            if(isset($raw_podcasts[0]))
                $calcul_pourcentage += count($raw_podcasts);

            if(isset($raw_challenges[0]))
                $calcul_pourcentage += count($raw_challenges);

            if(!$calcul_pourcentage)
                $calcul_pourcentage = 1;

            $parcours->pourcentage = round(($score / $calcul_pourcentage) * 100);
            
            if($parcours->pourcentage == 100)
                array_push($parcours_win, $parcours->id);
        }
        $competences_developped = DB::table('parcour_centre_interets')
                            ->whereIn('parcour_centre_interets.parcour_formation_id', $parcours_win)
                            ->groupBy('parcour_centre_interets.centre_interet_id')
                            ->count();
        $personal_croissance['competences_developped'] = $competences_developped;
        
        $personal_croissance['competences_to_develop'] = 0;
        $mes_competences = 0;
        //Compétences à développer
        $mes_competences = DB::table('apprenant_competences')
                            ->join('apprenants', 'apprenants.id', 'apprenant_competences.apprenant_id')
                            ->where('apprenant_competences.state', 1)
                            ->where('apprenants.user_id', $user->id)
                            ->count();
        if($personal_croissance['competences_developped'] > $mes_competences )
            $personal_croissance['competences_to_develop'] = $mes_competences;
        else
            $personal_croissance['competences_to_develop'] = $mes_competences - $personal_croissance['competences_developped'];


        //Points
        $points = DB::table('points')
            ->where('user_id', $input['user_id'])
            ->sum('points.value');
        $personal_croissance['points'] = $points;
    
        $infos['croissance_personnel'] = (object) $personal_croissance; 
    
    
        //Le formateur de la semaine
        $formateurs = array();
        $formateur_all = DB::table('users')
                        ->select('users.id', 'users.nom', 'users.prenom', 'users.email','users.avatar' ,'users.phone', 'users.whatsapp', 'users.bio', 'users.bio', 'users.linkedin', 'users.facebook', 'users.twitter', 'users.code_promo', 'users.role_id', 'users.address_id', 'users.created_at', 'users.updated_at', 'formateurs.id as formateur_id')
                        ->join('formateurs', 'formateurs.user_id', 'users.id')
                        ->where('statut_display', 1)
                        ->where('users.role_id', 3)
                        ->whereNull('users.deleted_at')
                        ->get();
        $formateur_all = $formateur_all->shuffle();
        foreach($formateur_all as $key => $formateur) {
            $formateur->avatar_formateur = Storage::disk('s3')->url('users/'. $formateur->avatar); 
    
            $number_parcours = DB::table('parcours_formations')
            ->whereNull('parcours_formations.deleted_at')
            ->where('parcours_formations.formateur_id', $formateur->formateur_id)
            ->count();
            $formateur->number_parcours = $number_parcours;
    
            $number_apprenants = DB::table('cinetpay_transactions')
            ->where('cinetpay_transactions.user_id', $formateur->id)
            ->count();
            $formateur->number_apprenants = $number_apprenants;
            array_push($formateurs, $formateur);
        }
        
        /*$keys = array_column($formateurs, 'number_apprenants');
        array_multisort($keys, SORT_DESC, $formateurs);*/
    
        $formateurs = array_slice($formateurs, 0, 2, true);
    
        $infos['formateurs'] = $formateurs; 
    
        return $this->sendResponse($infos, 'Home visualisation !');
    }

    public function update(Request $request)
    {
        $token = $request->header('Authorization');
        $user_token = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $inputs = $request->all();

        if(isset($inputs['password']))
            return response()->json(['success' => false, 'message' => 'No password please !'], 201);

        /** @var User $user */
        $user = User::find($user_token->user_id);

        if (empty($user)) 
            return response()->json(['success' => false, 'message' => 'Something went wrong !'], 201);

        $user->fill($request->all());
        $user->save();

        if(isset($inputs['avatar'])){
            //Get Pre-signed Urls for uploading objects
            /** @var \Illuminate\Filesystem\FilesystemAdapter $fs */
            $fs = Storage::disk('s3');
            /** @var \League\Flysystem\Filesystem $driver */
            $driver = $fs->getDriver();
            /** @var \League\Flysystem\AwsS3v3\AwsS3Adapter $adapter */
            $adapter = $driver->getAdapter();
            /** @var \Aws\S3\S3Client $client */
            $client = $adapter->getClient();
        
            $command = $client->getCommand('PutObject', [
                'Bucket' => 'moveskills',
                'Key'    => 'users/' . $inputs['avatar'],
                'ACL' => 'public-read'
            ]);  
            
            $request = $client->createPresignedRequest($command, '+2 hours');

            $signedUrl = (string)$request->getUri();
            
            $user->signed_url = $signedUrl;
        }

        return $this->sendResponse($user, 'Informations updated successfully');
    }

    public function update_password(EditPassword $request)
    {
        $token = $request->header('Authorization');
        $user_token = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $inputs = $request->all();

        /** @var User $user */
        $user = User::find($user_token->user_id);

        if (empty($user)) 
            return response()->json(['success' => false, 'message' => 'Somethin went wrong !'], 201);
        
        if(!Hash::check($inputs['password'], $user->password))
            return response()->json(['success' => false, 'message' => 'Old password do not match !'], 201);

        $user->password = bcrypt($inputs['new_password']);
        $user->save();

        return $this->sendResponse($user, 'Informations updated successfully');
    }

    public function show(Request $request){
        $token = $request->header('Authorization');
        $input = $request->all();

        $token_user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();
        $input['user_id'] = $token_user->user_id;
        /** @var User $user */
        $user = User::find($input['user_id']);

        if (empty($user)) 
            return $this->sendError('User not found !');

        if($user->avatar)
            $user->avatar = Storage::disk('s3')->url('users/'. $user->avatar);
        else
            $user->avatar = null;

        return $this->sendResponse($user, 'User retrieved successfully !');
    }

    public function recap(Request $request){
        $token = $request->header('Authorization');
        $input = $request->all();

        $token_user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $input['user_id'] = $token_user->user_id;

        $user = User::find($input['user_id']);
          
        //Apprenant found
        $apprenant = Apprenant::where('user_id', $input['user_id'])->first();
        if(empty($apprenant)) 
            return $this->sendError('No apprenant found !');
                   
        $infos = array();

        //Roue de la vie - note informations
        $wheel = array();
        $passer_test = NULL; 
        $passer_test = DB::table('passer_tests')
        ->select('passer_tests.id', 'passer_tests.type', 'passer_tests.roue_de_vie_id', 'passer_tests.mini_disc_id',  'passer_tests.competence_id', 'passer_tests.quizz_id', 'passer_tests.challenge_id', 'passer_tests.point_id', 'passer_tests.updated_at')
        ->where('type', 'roue_de_vie')
        ->where('user_id', $input['user_id'])
        ->orderBy('id', 'desc')
        ->first();
        if(!empty($passer_test)){
            $wheel['domaines'] = DB::table('response_by_users')
            ->select('domaines.id as domaine_id', 'domaines.libelle', 'response_by_users.note', 'domaines.image_second_id as image')
            ->join('questions', 'questions.id', 'response_by_users.question_id')
            ->join('domaines', 'domaines.id', 'questions.domaine_id')
            ->where('user_id', $input['user_id'])
            ->where('passer_test_id', $passer_test->id)
            ->orderBy('response_by_users.note', 'asc')
            ->get();

            $wheel['competences'] = DB::table('apprenant_competences')
            ->select('competences.id', 'competences.content')
            ->join('competences', 'competences.id', 'apprenant_competences.competence_id')
            ->where('apprenant_id', $apprenant->id)
            ->where('apprenant_competences.state', 1)
            ->where('competences.type', "personnelle")
            ->groupBy('competences.id','competences.content')
            ->get();
            //return $wheel
        }
        $infos['wheel_date_passed'] = $passer_test->updated_at;
        $infos['wheel'] = $wheel;

       //Mini disque 
       $mini_disque = array();
       $passer_test = NULL; 
       $passer_test = DB::table('passer_tests')
       ->select('passer_tests.id', 'passer_tests.type', 'passer_tests.roue_de_vie_id', 'passer_tests.mini_disc_id',  'passer_tests.competence_id', 'passer_tests.quizz_id', 'passer_tests.challenge_id', 'passer_tests.point_id', 'passer_tests.updated_at')
       ->where('type', 'mini_disq')
       ->where('user_id', $input['user_id'])
       ->orderBy('id', 'desc')
       ->first();

        if(!empty($passer_test)){
           //foreach column goes through them
           $score = collect(['Rouge' => 0, 'Jaune' => 0, 'Vert' => 0, 'Bleue' => 0]);
           $red = 0; //:1
           $yellow = 0; //:2
           $green = 0; //:3
           $blue = 0; //:4
   
           foreach (range(1, 4) as $number) {
               $red_column = 0; //:1
               $yellow_column = 0; //:2
               $green_column = 0; //:3
               $blue_column = 0; //:4
               $canva_mini_discs = CanvaMiniDisq::where('column', $number)->get();
               $row = 0; 
               foreach($canva_mini_discs as $case){
                   $row++;
                   $response_user = ResponseByUserDisc::where('user_id', $input['user_id'])
                   ->where('passer_test_id', $passer_test->id)
                   ->where('row', $row)
                   ->where('column', $number)
                   ->first();

                   if($response_user){
                       if($case->couleur_id == 1)
                           $red_column += $response_user->point;
                       if($case->couleur_id == 2)
                           $yellow_column += $response_user->point;
                       if($case->couleur_id == 3)
                           $green_column += $response_user->point;
                       if($case->couleur_id == 4)
                           $blue_column += $response_user->point;
                   }
               }
   
               $red += $red_column;
               $yellow += $yellow_column;
               $green += $green_column;
               $blue += $blue_column;
           }
   
           $score['Rouge'] = $red;
           $score['Jaune'] = $yellow;
           $score['Vert'] = $green;
           $score['Bleue'] = $blue;

           $score = $score->sortDesc();
           $first = $score->take(1);

           $eky = $first->keys();

           $couleur = Couleur::where('libelle', $eky)->first();

           $mini_disque['caracteristiques'] = DB::table('caracteristiques')
                                       ->select('libelle')
                                       ->where('couleur_id', $couleur->id)
                                       ->get();
                                       
           $couleur_image = DB::table('images')
           ->select('libelle')
           ->where('id', $couleur->image_id)
           ->first();

           $mascotte_image = DB::table('images')
           ->select('libelle')
           ->where('id', $couleur->image_second_id)
           ->first();

           $mini_disque['couleur'] = $couleur->libelle; 
           $mini_disque['mascotte'] = $couleur->mascotte;
           $mini_disque['competences'] =  DB::table('apprenant_competences')
           ->select('competences.id', 'competences.content')
           ->join('competences', 'competences.id', 'apprenant_competences.competence_id')
           ->where('apprenant_id', $apprenant->id)
           ->where('apprenant_competences.state', 1)
           ->where('competences.type', "professionnelle")
           ->groupBy('competences.id','competences.content')
           ->get();
           //return $mini_disque;
        }
   
        $infos['minidisque_date_passed'] = $passer_test->updated_at;
        $infos['minidisque'] = $mini_disque;

        return $this->sendResponse($infos, 'History tests informations displayed successfully !');
    }

    public function destroy(Request $request)
    {
        $token = $request->header('Authorization');
        $input = $request->all();

        $token_user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();
        $input['user_id'] = $token_user->user_id;
        /** @var User $user */
        $user = User::find($input['user_id']);

        if (empty($user)) 
            return $this->sendError('User not found !');

        $user->delete();

        return $this->sendSuccess('User deactivate successfully !');
    }

    public function get_current_points(Request $request){
        $token = $request->header('Authorization');
        $input = $request->all();

        $token_user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $input['user_id'] = $token_user->user_id;
        $score = DB::table('points')
        ->where('user_id', $input['user_id'])
        ->sum('points.value');
        $score = intval($score);

        $infos['score'] = $score;

        return $this->sendResponse($infos, 'Total points gained by this user !');
    }
}
