<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateApprenantCompetenceAPIRequest;
use App\Http\Requests\API\UpdateApprenantCompetenceAPIRequest;
use App\Models\ApprenantCompetence;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ApprenantCompetenceController
 * @package App\Http\Controllers\API
 */

class ApprenantCompetenceAPIController extends AppBaseController
{
    /**
     * Display a listing of the ApprenantCompetence.
     * GET|HEAD /apprenantCompetences
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = ApprenantCompetence::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $apprenantCompetences = $query->get();

        return $this->sendResponse($apprenantCompetences->toArray(), 'Apprenant Competences retrieved successfully');
    }

    /**
     * Store a newly created ApprenantCompetence in storage.
     * POST /apprenantCompetences
     *
     * @param CreateApprenantCompetenceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateApprenantCompetenceAPIRequest $request)
    {
        $input = $request->all();

        /** @var ApprenantCompetence $apprenantCompetence */
        $apprenantCompetence = ApprenantCompetence::create($input);

        return $this->sendResponse($apprenantCompetence->toArray(), 'Apprenant Competence saved successfully');
    }

    /**
     * Display the specified ApprenantCompetence.
     * GET|HEAD /apprenantCompetences/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ApprenantCompetence $apprenantCompetence */
        $apprenantCompetence = ApprenantCompetence::find($id);

        if (empty($apprenantCompetence)) {
            return $this->sendError('Apprenant Competence not found');
        }

        return $this->sendResponse($apprenantCompetence->toArray(), 'Apprenant Competence retrieved successfully');
    }

    /**
     * Update the specified ApprenantCompetence in storage.
     * PUT/PATCH /apprenantCompetences/{id}
     *
     * @param int $id
     * @param UpdateApprenantCompetenceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApprenantCompetenceAPIRequest $request)
    {
        /** @var ApprenantCompetence $apprenantCompetence */
        $apprenantCompetence = ApprenantCompetence::find($id);

        if (empty($apprenantCompetence)) {
            return $this->sendError('Apprenant Competence not found');
        }

        $apprenantCompetence->fill($request->all());
        $apprenantCompetence->save();

        return $this->sendResponse($apprenantCompetence->toArray(), 'ApprenantCompetence updated successfully');
    }

    /**
     * Remove the specified ApprenantCompetence from storage.
     * DELETE /apprenantCompetences/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ApprenantCompetence $apprenantCompetence */
        $apprenantCompetence = ApprenantCompetence::find($id);

        if (empty($apprenantCompetence)) {
            return $this->sendError('Apprenant Competence not found');
        }

        $apprenantCompetence->delete();

        return $this->sendSuccess('Apprenant Competence deleted successfully');
    }
}
