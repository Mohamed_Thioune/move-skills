<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatechallengeAPIRequest;
use App\Http\Requests\API\UpdatechallengeAPIRequest;

use App\Models\Challenge;
use App\Models\Citation;
use App\Models\PasserTest;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

use Storage;
use DB;

/**
 * Class ChallengeController
 * @package App\Http\Controllers\API
 */

class ChallengeAPIController extends AppBaseController
{
    /**
     * Display a listing of the challenge.
     * GET|HEAD /challenges
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Challenge::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $challenges = $query->get();

        return $this->sendResponse($challenges->toArray(), 'Challenges retrieved successfully');
    }

    /**
     * Store a newly created challenge in storage.
     * POST /challenges
     *
     * @param CreatechallengeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatechallengeAPIRequest $request)
    {
        $input = $request->all();

        /** @var challenge $challenge */
        $challenge = Challenge::create($input);

        return $this->sendResponse($challenge->toArray(), 'Challenge saved successfully');
    }

    /**
     * Display the specified challenge.
     * GET|HEAD /challenges/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $token = $request->header('Authorization');
        $user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        /** @var challenge $challenge */
        $challenge = Challenge::find($id);

        if (empty($challenge)) {
            return $this->sendError('Challenge not found');
        }
        
        //Questions
        $questions = DB::table('question_challenges')
        ->select('question_challenges.id', 'question_challenges.libelle', 'question_challenges.link')
        ->where('question_challenges.challenge_id', $challenge->id)
        ->get();
        $challenge->questions = $questions;

        $formateur = DB::table('challenges')
        ->select('users.nom', 'users.prenom')
        ->join('parcours_formations', 'parcours_formations.id', 'challenges.parcours_formation_id')
        ->join('formateurs', 'formateurs.id', 'parcours_formations.formateur_id')
        ->join('users', 'users.id', 'formateurs.user_id')
        ->first();

        //Nom & Prenom formateur
        $challenge->nom_formateur = $formateur->nom;
        $challenge->prenom_formateur = $formateur->prenom;

        //Citation
        $challenge->pourcentage = 0;
        $citation = Citation::where('challenge_id', $challenge->id)->first();
        $challenge->citation = $citation;

        //Pourcentage
        $challenge->pourcentage = 0;
        $passer_test = PasserTest::where('challenge_id', $challenge->challenge_id)->where('user_id', $user->user_id)->whereNotNull('point_id')->first();
        if(!empty($passer_test))
            $challenge->pourcentage = 100;

        return $this->sendResponse($challenge->toArray(), 'Challenge retrieved successfully');
    }

    /**
     * Update the specified challenge in storage.
     * PUT/PATCH /challenges/{id}
     *
     * @param int $id
     * @param UpdatechallengeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatechallengeAPIRequest $request)
    {
        /** @var challenge $challenge */
        $challenge = Challenge::find($id);

        if (empty($challenge)) {
            return $this->sendError('Challenge not found');
        }

        $challenge->fill($request->all());
        $challenge->save();

        return $this->sendResponse($challenge->toArray(), 'challenge updated successfully');
    }

    /**
     * Remove the specified challenge from storage.
     * DELETE /challenges/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var challenge $challenge */
        $challenge = Challenge::find($id);

        if (empty($challenge)) {
            return $this->sendError('Challenge not found');
        }

        $challenge->delete();

        return $this->sendSuccess('Challenge deleted successfully');
    }
}
