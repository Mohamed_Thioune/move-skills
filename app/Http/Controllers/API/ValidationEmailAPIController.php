<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateValidationEmailAPIRequest;
use App\Http\Requests\API\UpdateValidationEmailAPIRequest;
use App\Models\ValidationEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ValidationEmailController
 * @package App\Http\Controllers\API
 */

class ValidationEmailAPIController extends AppBaseController
{
    /**
     * Display a listing of the ValidationEmail.
     * GET|HEAD /validationEmails
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = ValidationEmail::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $validationEmails = $query->get();

        return $this->sendResponse($validationEmails->toArray(), 'Validation Emails retrieved successfully');
    }

    /**
     * Store a newly created ValidationEmail in storage.
     * POST /validationEmails
     *
     * @param CreateValidationEmailAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateValidationEmailAPIRequest $request)
    {
        $input = $request->all();

        /** @var ValidationEmail $validationEmail */
        $validationEmail = ValidationEmail::create($input);

        return $this->sendResponse($validationEmail->toArray(), 'Validation Email saved successfully');
    }

    /**
     * Display the specified ValidationEmail.
     * GET|HEAD /validationEmails/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ValidationEmail $validationEmail */
        $validationEmail = ValidationEmail::find($id);

        if (empty($validationEmail)) {
            return $this->sendError('Validation Email not found');
        }

        return $this->sendResponse($validationEmail->toArray(), 'Validation Email retrieved successfully');
    }

    /**
     * Update the specified ValidationEmail in storage.
     * PUT/PATCH /validationEmails/{id}
     *
     * @param int $id
     * @param UpdateValidationEmailAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateValidationEmailAPIRequest $request)
    {
        /** @var ValidationEmail $validationEmail */
        $validationEmail = ValidationEmail::find($id);

        if (empty($validationEmail)) {
            return $this->sendError('Validation Email not found');
        }

        $validationEmail->fill($request->all());
        $validationEmail->save();

        return $this->sendResponse($validationEmail->toArray(), 'ValidationEmail updated successfully');
    }

    /**
     * Remove the specified ValidationEmail from storage.
     * DELETE /validationEmails/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ValidationEmail $validationEmail */
        $validationEmail = ValidationEmail::find($id);

        if (empty($validationEmail)) {
            return $this->sendError('Validation Email not found');
        }

        $validationEmail->delete();

        return $this->sendSuccess('Validation Email deleted successfully');
    }
}
