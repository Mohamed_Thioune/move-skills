<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateParcoursFormationAPIRequest;
use App\Http\Requests\API\UpdateParcoursFormationAPIRequest;

use App\Models\User;
use App\Models\Parcours_formation;
use App\Models\Video;
use App\Models\Audio;
use App\Models\Article;
use App\Models\Challenge;
use App\Models\Citation;
use App\Models\Podcast;
use App\Models\Commentaire;
use App\Models\PasserTest;
use App\Models\CinetpayTransaction;
use App\Models\CanvaMiniDisq;
use App\Models\ResponseByUserDisc;
use App\Models\Couleur;
use App\Models\SentMail;

use Illuminate\Contracts\Filesystem\Filesystem;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use Storage;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

use DB;

/**
 * Class ParcoursFormationController
 * @package App\Http\Controllers\API
 */

class ParcoursFormationAPIController extends AppBaseController
{
    /**
     * Display a listing of the ParcoursFormation.
     * GET|HEAD /parcoursFormations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $token = $request->header('Authorization');
        $input = $request->all();

        $token_user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $input['user_id'] = $token_user->user_id;

        $user = User::find($input['user_id']);

        if(empty($user))
            return $this->sendError('User not found !');

        $parcours_ids = array();
        $parcours_ids = DB::table('apprenant_competences')
                            ->select('parcour_centre_interets.parcour_formation_id')
                            ->join('apprenants', 'apprenants.id', 'apprenant_competences.apprenant_id')
                            ->join('parcour_centre_interets', 'parcour_centre_interets.centre_interet_id', 'apprenant_competences.competence_id')
                            ->join('parcours_formations', 'parcour_centre_interets.parcour_formation_id', 'parcours_formations.id')
                            ->whereNull('parcours_formations.deleted_at')
                            ->where('parcours_formations.publier', 1)
                            ->where('apprenants.user_id', $user->id)
                            ->where('apprenant_competences.state', 1)
                            ->groupBy('parcour_centre_interets.parcour_formation_id')
                            ->get();

        $parcours_formation = array();
        $raw_parcours_formation  = array();
                
        if(isset($parcours_ids[0]))
            foreach($parcours_ids as $item){

                $parcour = Parcours_formation::find($item->parcour_formation_id);
                if(!$parcour)
                    continue;
                if(isset($parcour->libelle))
                    $parcour->titre = $parcour->libelle;

                $formateur = DB::table('formateurs')
                ->select('users.nom', 'users.prenom', 'users.avatar')
                ->join('users', 'users.id', 'formateurs.user_id')
                ->where('formateurs.id', $parcour->formateur_id)
                ->first();

                $parcour->nom_formateur = (isset($formateur->nom) && $formateur->nom) ? $formateur->nom : '' ; 
                $parcour->prenom_formateur = (isset($formateur->prenom) && $formateur->prenom) ? $formateur->prenom : '' ; 

                array_push($raw_parcours_formation, $parcour);
            }
            
        if(empty($raw_parcours_formation))
            $raw_parcours_formation =  DB::table('parcours_formations')
            ->select('parcours_formations.id', 'parcours_formations.libelle as titre', 'parcours_formations.description', 'parcours_formations.prix', 'parcours_formations.prix_promo', 'parcours_formations.duree', 'users.nom as nom_formateur', 'users.prenom as prenom_formateur', 'users.avatar as avatar_formateur', 'parcours_formations.win_points', 'parcours_formations.require_credits')
            ->join('formateurs', 'formateurs.id', 'parcours_formations.formateur_id')
            ->join('users', 'users.id', 'formateurs.user_id')
            ->whereNull('parcours_formations.deleted_at')
            ->orderBy('id', 'DESC')
            ->take(4)
            ->get();
            
        foreach($raw_parcours_formation as $parcours){
            if(!isset($parcours->id))
                continue;

            $pourcentage = 0;
            $calcul_pourcentage = 0;
            $win_total_points = 0;

            $raw_videos = Video::where('parcours_formation_id', $parcours->id)->get();
            $raw_audios = Audio::where('parcours_formation_id', $parcours->id)->get();
            // $raw_articles = Article::where('parcours_formation_id', $parcours->id)->get();
            $raw_citations = Citation::where('parcours_formation_id', $parcours->id)->get();
            $raw_challenges = Challenge::where('parcours_formation_id', $parcours->id)->get();
            $raw_podcasts = Podcast::where('parcours_formation_id', $parcours->id)->get();

            //Types
            $types = array();
            if(isset($raw_videos[0]))
                array_push($types, 'Videos');
            if(isset($raw_audios[0]))
                array_push($types, 'Audios');
            // if(isset($raw_articles[0]))
            //     array_push($types, 'Articles');
            if(isset($raw_citations[0]))
                array_push($types, 'Citations');
            if(isset($raw_challenges[0]))
                array_push($types, 'Challenges');
            if(isset($raw_podcasts[0]))
                array_push($types, 'Podcasts');
                
            //Count Type
            $type_count = array();
            if(isset($raw_videos[0]))
                array_push($type_count, count($raw_videos));
            if(isset($raw_audios[0]))
                array_push($type_count, count($raw_audios));
            // if(isset($raw_articles[0]))
            //     array_push($types, 'Articles');
            if(isset($raw_citations[0]))
                array_push($type_count, count($raw_citations));
            if(isset($raw_challenges[0]))
                array_push($type_count, count($raw_challenges));
            if(isset($raw_podcasts[0]))
                array_push($type_count, count($raw_podcasts));

            //Paid
            $paid = 0;
            $paid_parcours = DB::table('cinetpay_transactions')
            ->select('cinetpay_transactions.parcours_id')
            ->where('cinetpay_transactions.user_id', $user->id)
            ->where('cinetpay_transactions.parcours_id', $parcours->id)
            ->first();
            if(!empty($paid_parcours))
                $paid = 1;

            if($parcours->avatar_formateur)
                $parcours->avatar_formateur = Storage::disk('s3')->url('users/'. $parcours->avatar_formateur); 
            else
                $parcours->avatar_formateur = null;

            $parcours->paid = $paid;
            $parcours->types = $types;
            $parcours->numbers = $type_count;


            /** Pourcentage **/
            $score = 0;
            //Videos
            $raw_videos = Video::select('id', 'libelle', 'description', 'source', 'created_at', 'updated_at', 'quizz_id')
                        ->where('parcours_formation_id', $parcours->id)
                        ->get();
            foreach($raw_videos as $video){
                $win_total_points += 15;
                $passer_test = PasserTest::where('type', 'quizz')->where('quizz_id', $video->quizz_id)->where('user_id', $user->id)->whereNotNull('point_id')->first();
                if(!empty($passer_test))
                    $score += 1;
            }

            //Audios
            $raw_audios = Audio::select('id', 'libelle', 'description', 'podcast', 'created_at', 'updated_at', 'quizz_id')
                        ->where('parcours_formation_id', $parcours->id)
                        ->get();
            foreach($raw_audios as $audio){
                $passer_test = PasserTest::where('type', 'quizz')->where('quizz_id', $audio->quizz_id)->where('user_id', $user->id)->whereNotNull('point_id')->first();
                if(!empty($passer_test))
                    $score += 1;
            }

            //Podcasts
            $raw_podcasts = Podcast::select('id', 'libelle', 'description', 'podcast', 'created_at', 'updated_at', 'quizz_id')
            ->where('parcours_formation_id', $parcours->id)
            ->get();
            foreach($raw_podcasts as $podcast){
                $win_total_points += 10;
                $read_podcast = DB::table('parcour_activites')
                ->select('parcours_id')
                ->where('type', 'podcast')
                ->where('activity_id', $podcast->id)
                ->where('user_id', $user->id)
                ->first();
        
                $podcast->read = 0;
                if(!empty($read_podcast))
                    $score += 1;

            }

            //Challenges
            $raw_challenges = Challenge::select('id', 'title', 'type', 'created_at', 'updated_at')
            ->where('parcours_formation_id', $parcours->id)
            ->get();
            $pourcentage_challenge = 0;
            foreach($raw_challenges as $challenge){
                $win_total_points += 15;
                $passer_test = PasserTest::where('type', 'challenge')->where('challenge_id', $challenge->id)->where('user_id', $user->id)->whereNotNull('point_id')->first();
                if(!empty($passer_test))
                    $score += 1;
            }

            if(isset($raw_videos[0]))
                $calcul_pourcentage += count($raw_videos);
            
            if(isset($raw_audios[0]))
                $calcul_pourcentage += count($raw_audios);

            if(isset($raw_podcasts[0]))
                $calcul_pourcentage += count($raw_podcasts);

            if(isset($raw_challenges[0]))
                $calcul_pourcentage += count($raw_challenges);

            if(!$calcul_pourcentage)
                $calcul_pourcentage = 1;

            $parcours->win_points = $win_total_points;
            $parcours->pourcentage = round(($score / $calcul_pourcentage) * 100);
            
            array_push($parcours_formation, $parcours);
        }

        return $this->sendResponse($parcours_formation, 'Parcours Formations retrieved successfully');
    }

    /**
     * Store a newly created ParcoursFormation in storage.
     * POST /parcoursFormations
     *
     * @param CreateParcoursFormationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateParcoursFormationAPIRequest $request)
    {
        $input = $request->all();

        /** @var ParcoursFormation $parcoursFormation */
        $parcoursFormation = Parcours_formation::create($input);

        return $this->sendResponse($parcoursFormation->toArray(), 'Parcours Formation saved successfully');
    }

    /**
     * Display the specified ParcoursFormation.
     * GET|HEAD /parcoursFormations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(Request $request, $id)
    {
        // $articles = array();
        // $articles_object = array();

        /** @var ParcoursFormation $parcoursFormation */
        $parcours = Parcours_formation::find($id);

        if (empty($parcours)) {
            return $this->sendError('Parcours Formation not found');
        }

        $token = $request->header('Authorization');
        $user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $infos = array();

        $types = array();
        $videos = array();
        $videos_object = array();
        $audios = array();
        $audios_object = array();
        $podcasts = array();
        $podcasts_object = array();
        $challenges = array();
        $challenges_object = array();
        $win_points = 15;
        $win_total_points = 0;
        $win_podcast_points = 15;
        $score = 0;

        $calcul_pourcentage = 0;

        //Paid
        $paid_parcours = DB::table('cinetpay_transactions')
        ->select('cinetpay_transactions.parcours_id')
        ->where('cinetpay_transactions.user_id', $user->user_id)
        ->where('cinetpay_transactions.parcours_id', $parcours->id)
        ->first();
    
        if(!$paid_parcours && !$parcours->prix)
            CinetpayTransaction::create(
                [
                  'amount' => 0,
                  'type_payment' => 'free',
                  'parcours_id' => $parcours->id,
                  'user_id' =>  $user->user_id,
                ]
            );
        $paid = 1;

        $infos['parcours'] =  DB::table('parcours_formations')
        ->select('parcours_formations.id', 'parcours_formations.libelle as titre', 'parcours_formations.description', 'parcours_formations.prix', 'parcours_formations.duree', 'users.nom as nom_formateur', 'users.prenom as prenom_formateur', 'users.avatar as avatar_formateur', 'parcours_formations.win_points', 'parcours_formations.require_credits')
        ->join('formateurs', 'formateurs.id', 'parcours_formations.formateur_id')
        ->join('users', 'users.id', 'formateurs.user_id')
        ->where('parcours_formations.id', $parcours->id)
        ->first();
        if($infos['parcours']->avatar_formateur)
            $infos['parcours']->avatar_formateur = Storage::disk('s3')->url('users/'. $infos['parcours']->avatar_formateur); 
        else
            $infos['parcours']->avatar_formateur = null;

        //Videos
        $raw_videos = Video::select('id', 'libelle', 'description', 'source', 'created_at', 'updated_at', 'quizz_id')
                      ->where('parcours_formation_id', $parcours->id)
                      ->get();
        $score_video = 0;
        $pourcentage_video = 0; 
        foreach($raw_videos as $video){
            $win_total_points += 15;
            $video->win_points = $win_points;
            $video->pourcentage = 0;
            $video->source_url = Storage::disk('s3')->url('activities/videos/'. $video->source);
            $passer_test = PasserTest::where('type', 'quizz')->where('quizz_id', $video->quizz_id)->where('user_id', $user->user_id)->whereNotNull('point_id')->first();
            if(!empty($passer_test)){
                $score += 1;
                $score_video += 1;
                $video->pourcentage = 100;
            }
            array_push($videos, $video);
        }
        if(!empty($videos))
            $pourcentage_video = round(($score_video / count($videos)) * 100);

        //Audios
        $raw_audios = Audio::select('id', 'libelle', 'description', 'podcast', 'created_at', 'updated_at', 'quizz_id')
                      ->where('parcours_formation_id', $parcours->id)
                      ->get();
        $score_audio = 0;
        $pourcentage_audio = 0; 
        foreach($raw_audios as $audio){
            $audio->win_points = $win_points;
            $audio->pourcentage = 0;
            $audio->podcast_url = Storage::disk('s3')->url('activities/audios/'. $audio->podcast);
            $passer_test = PasserTest::where('type', 'quizz')->where('quizz_id', $audio->quizz_id)->where('user_id', $user->user_id)->whereNotNull('point_id')->first();
            if(!empty($passer_test)){
                $score += 1;
                $score_audio += 1;
                $audio->pourcentage = 100;
            }
            array_push($audios, $audio);
        }
        if(!empty($audios))
            $pourcentage_audio = round(($score_audio / count($audios)) * 100);

        //Podcasts
        $raw_podcasts = Podcast::select('id', 'libelle', 'description', 'podcast', 'created_at', 'updated_at', 'quizz_id')
        ->where('parcours_formation_id', $parcours->id)
        ->get();
        $pourcentage_podcast = 0;
        $score_podcast = 0;
        foreach($raw_podcasts as $podcast){
            $win_total_points += 15;
            $podcast->win_points = $win_points;
            $podcast->podcast_url =  $podcast->podcast;

            $read_podcast = DB::table('parcour_activites')
            ->select('parcours_id')
            ->where('type', 'podcast')
            ->where('activity_id', $podcast->id)
            ->where('user_id', $user->user_id)
            ->first();
    
            $podcast->read = 0;
            if(!empty($read_podcast)){
                $score += 1;
                $score_podcast += 1;
                $podcast->read = 1;
                $podcast->pourcentage = 100;
            }
            
            array_push($podcasts, $podcast);
        }
        if(!empty($podcasts))
            $pourcentage_podcast = round(($score_podcast / count($podcasts)) * 100);

        //Articles
        // $raw_articles = Article::select('id', 'libelle', 'content', 'link', 'created_at', 'updated_at', 'quizz_id as quizz')
        //                 ->where('parcours_formation_id', $parcours->id)
        //                 ->get();
        // $pourcentage_article = 0;
        // $score = 0;
        // foreach($raw_articles as $article){
        //     $article->win_points = $win_points;
        //     $article->pourcentage = 0;
        //     $passer_test = PasserTest::where('quizz_id', $article->quizz_id)->where('user_id', $user->user_id)->whereNotNull('point_id')->first();
        //     if(!empty($passer_test)){
        //         $score += 1;
        //         $article->pourcentage = 100;
        //     }
        //     array_push($articles, $article);
        // }
        // $pourcentage_article = round(($score / 3) * 100);

        //Challenges
        $score_challenge = 0;
        $raw_challenges = Challenge::select('id', 'title', 'type', 'created_at', 'updated_at')
                        ->where('parcours_formation_id', $parcours->id)
                        ->get();
        foreach($raw_challenges as $challenge){
            $win_total_points += 15;
            $challenge->pourcentage = 0;
            $questions = DB::table('question_challenges')
                        ->select('question_challenges.id', 'question_challenges.libelle', 'question_challenges.link')
                        ->where('question_challenges.challenge_id', $challenge->id)
                        ->get();
            $challenge->questions = $questions;
            $citation = Citation::where('challenge_id', $challenge->id)->first();
            $challenge->citation = $citation;
            $passer_test = PasserTest::where('type', 'challenge')->where('challenge_id', $challenge->id)->where('user_id', $user->user_id)->whereNotNull('point_id')->first();
            if(!empty($passer_test)){
                $score += 1;
                $score_challenge += 1;
                $challenge->pourcentage = 100;
            }
            array_push($challenges, $challenge);
        }
        $pourcentage_challenge = 0;
        if(!empty($challenges))
            $pourcentage_challenge = round(($score_challenge / count($challenges)) * 100);

        $pourcentage = 0;
        if(isset($raw_videos[0])){
            $calcul_pourcentage += count($raw_videos);
            array_push($types, 'Videos');
            $videos_object['total_win_points'] = count($videos) * $win_points;
            $videos_object['pourcentage'] = round($pourcentage_video);
            $videos_object['videos_list'] = $videos;
            $infos['videos'] = $videos_object;

        }
        if(isset($raw_audios[0])){
            $calcul_pourcentage += count($raw_audios);
            array_push($types, 'Audios');
            $audios_object['total_win_points'] = count($audios) * $win_points;
            $audios_object['pourcentage'] = round($pourcentage_audio);
            $audios_object['audios_list'] = $audios;
            $infos['audios'] = $audios_object;
        }

        if(isset($raw_podcasts[0])){
            $calcul_pourcentage += count($raw_podcasts);
            array_push($types, 'Podcasts');
            $podcasts_object['total_win_points'] = count($podcasts) * $win_podcast_points;
            $podcasts_object['pourcentage'] = round($pourcentage_podcast);
            $podcasts_object['podcasts_list'] = $podcasts;
            $infos['podcasts'] = $podcasts_object;
        }

        if(isset($raw_challenges[0])){
            $calcul_pourcentage += count($raw_challenges);
            array_push($types, 'Challenges');
            $challenges_object['total_win_points'] = count($raw_challenges) * $win_points;
            $challenges_object['pourcentage'] = $pourcentage_challenge;
            $challenges_object['challenges_list'] = $challenges;
            $pourcentage += $pourcentage_challenge;
            $infos['challenges'] = $challenges_object;
        }
        
        //Count Type
        $type_count = array();
        if(isset($raw_videos[0]))
            array_push($type_count, count($raw_videos));
        if(isset($raw_audios[0]))
            array_push($type_count, count($raw_audios));
        // if(isset($raw_articles[0]))
        //     array_push($types, 'Articles');
        if(isset($raw_citations[0]))
            array_push($type_count, count($raw_citations));
        if(isset($raw_challenges[0]))
            array_push($type_count, count($raw_challenges));
        if(isset($raw_podcasts[0]))
            array_push($type_count, count($raw_podcasts));

        $infos['parcours']->paid = $paid;
        $infos['parcours']->types = $types;
        $infos['parcours']->numbers = $type_count;

        if(!$calcul_pourcentage)
            $calcul_pourcentage = 1;

        $infos['parcours']->win_points = $win_total_points;
        $infos['pourcentage'] = round(($score / $calcul_pourcentage) * 100);
        
        return $this->sendResponse($infos, 'Parcours Formation retrieved successfully');
    }

    public function formations(Request $request, $id)
    {
        // $articles = array();
        // $articles_object = array();

        /** @var ParcoursFormation $parcoursFormation */
        $parcours = Parcours_formation::find($id);

        if (empty($parcours)) {
            return $this->sendError('Parcours Formation not found');
        }

        $token = $request->header('Authorization');
        $user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $infos = array();

        $videos = array();
        $videos_object = array();
        $audios = array();
        $audios_object = array();
        $calcul_pourcentage = 0;
        $win_points = 15;
        $win_total_points = 0;
        $score = 0;

        //Videos
        $raw_videos = Video::select('id', 'libelle', 'description', 'source', 'created_at', 'updated_at', 'quizz_id')
                      ->where('parcours_formation_id', $parcours->id)
                      ->get();
        $pourcentage_video = 0;
        $score_video = 0;
        foreach($raw_videos as $video){
            $win_total_points += 15;
            $video->win_points = $win_points;
            $video->pourcentage = 0;
            $video->source_url = Storage::disk('s3')->url('activities/videos/'. $video->source);
            $passer_test = PasserTest::where('type', 'quizz')->where('quizz_id', $video->quizz_id)->where('user_id', $user->user_id)->whereNotNull('point_id')->first();
            if(!empty($passer_test)){
                $score += 1;
                $score_video += 1;
                $video->pourcentage = 100;
            }
            array_push($videos, $video);
        }
        if(!empty($videos))
            $pourcentage_video = round(($score_video / count($raw_videos)) * 100);

        //Audios
        $raw_audios = Audio::select('id', 'libelle', 'description', 'podcast', 'created_at', 'updated_at', 'quizz_id')
                      ->where('parcours_formation_id', $parcours->id)
                      ->get();
        $pourcentage_audio = 0;
        $score_audio = 0;
        foreach($raw_audios as $audio){
            $audio->win_points = $win_points;
            $audio->pourcentage = 0;
            $audio->podcast_url = Storage::disk('s3')->url('activities/audios/'. $audio->podcast);
            $passer_test = PasserTest::where('type', 'quizz')->where('quizz_id', $audio->quizz_id)->where('user_id', $user->user_id)->whereNotNull('point_id')->first();
            if(!empty($passer_test)){
                $score += 1;
                $score_audio += 1;
                $audio->pourcentage = 100;
            }
            array_push($audios, $audio);
        }
        if(!empty($audios))
            $pourcentage_audio = round(($score_audio / count($raw_audios)) * 100);

        $pourcentage = 0;
        if(isset($raw_videos[0])){
            $calcul_pourcentage += count($raw_videos);
            $videos_object['total_win'] = count($videos) * $win_points;
            $videos_object['pourcentage'] = round($pourcentage_video);
            $videos_object['videos_list'] = $videos;
            $infos['videos'] = $videos_object;

        }
        if(isset($raw_audios[0])){
            $calcul_pourcentage += count($raw_audios);
            $audios_object['total_win_points'] = count($audios) * $win_points;
            $audios_object['pourcentage'] = round($pourcentage_audio);
            $audios_object['audios_list'] = $audios;
            $infos['audios'] = $audios_object;
        }

        if(!$calcul_pourcentage)
            $calcul_pourcentage = 1;

        $infos['pourcentage'] = round(($score / $calcul_pourcentage) * 100);
            
        return $this->sendResponse($infos, 'List formation retrieved successfully !');
    }

    public function podcasts(Request $request, $id)
    {
        /** @var ParcoursFormation $parcoursFormation */
        $parcours = Parcours_formation::find($id);

        if (empty($parcours)) {
            return $this->sendError('Parcours Formation not found');
        }

        $token = $request->header('Authorization');
        $user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $infos = array();
       
        $podcasts = array();
        $podcasts_object = array();
        $win_points = 10;
        $calcul_pourcentage = 0;
        $score = 0;

        //Podcasts
        $raw_podcasts = Podcast::select('id', 'libelle', 'description', 'podcast', 'created_at', 'updated_at', 'quizz_id')
        ->where('parcours_formation_id', $parcours->id)
        ->get();
        $pourcentage_podcast = 0;
        $score_podcast = 0;
        foreach($raw_podcasts as $podcast){
            $podcast->win_points = $win_points;
            $podcast->podcast_url =  $podcast->podcast;

            $read_podcast = DB::table('parcour_activites')
            ->select('parcours_id')
            ->where('type', 'podcast')
            ->where('activity_id', $podcast->id)
            ->where('user_id', $user->user_id)
            ->first();
    
            $podcast->read = 0;
            if(!empty($read_podcast)){
                $score += 1;
                $score_podcast += 1;
                $podcast->read = 1;
                $podcast->pourcentage = 100;
            }
            
            array_push($podcasts, $podcast);
        }
        if(!empty($podcasts))
            $pourcentage_podcast = round(($score_podcast / count($podcasts)) * 100);

        $pourcentage = 0;
        if(isset($raw_podcasts[0])){
            $calcul_pourcentage += count($raw_podcasts);
            $pourcentage += $pourcentage_podcast;
            $podcasts_object['total_win_points'] = count($podcasts) * $win_points;
            $podcasts_object['pourcentage'] = round($pourcentage_podcast);
            $podcasts_object['podcasts_list'] = $podcasts;
            $infos['podcasts'] = $podcasts_object;
        }

        if(!$calcul_pourcentage)
            $calcul_pourcentage = 1;

        $infos['pourcentage'] = round(($score / $calcul_pourcentage) * 100);
        
        return $this->sendResponse($infos, 'List podcast retrieved successfull !');
    }

    public function challenges(Request $request, $id)
    {
        // $articles = array();
        // $articles_object = array();

        /** @var ParcoursFormation $parcoursFormation */
        $parcours = Parcours_formation::find($id);

        if (empty($parcours)) {
            return $this->sendError('Parcours Formation not found');
        }

        $token = $request->header('Authorization');
        $user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $infos = array();

        $challenges = array();
        $challenges_object = array();
        $win_points = 15;
        $calcul_pourcentage = 0;

        //Challenges
        $score = 0;
        $raw_challenges = Challenge::select('id', 'title', 'type', 'created_at', 'updated_at')
                        ->where('parcours_formation_id', $parcours->id)
                        ->get();
        foreach($raw_challenges as $challenge){
            $challenge->pourcentage = 0;
            $questions = DB::table('question_challenges')
                        ->select('question_challenges.id', 'question_challenges.libelle', 'question_challenges.link')
                        ->where('question_challenges.challenge_id', $challenge->id)
                        ->get();
            $challenge->questions = $questions;
            $citation = Citation::where('challenge_id', $challenge->id)->first();
            $challenge->citation = $citation;
            $passer_test = PasserTest::where('type', 'challenge')->where('challenge_id', $challenge->id)->where('user_id', $user->user_id)->whereNotNull('point_id')->first();
            if(!empty($passer_test)){
                $score += 1;
                $challenge->pourcentage = 100;
            }
            array_push($challenges, $challenge);
        }
        $pourcentage_challenge = 0;
        if(!empty($challenges))
            $pourcentage_challenge = round(($score / count($challenges)) * 100);

        $pourcentage = 0;
         
        if(isset($raw_challenges[0])){
            $calcul_pourcentage += count($raw_challenges);
            $challenges_object['total_win_points'] = count($raw_challenges) * $win_points;
            $challenges_object['pourcentage'] = $pourcentage_challenge;
            $challenges_object['challenges_list'] = $challenges;
            $pourcentage += $pourcentage_challenge;
            $infos['challenges'] = $challenges_object;
        }

        if(!$calcul_pourcentage)
            $calcul_pourcentage = 1;

        $infos['pourcentage'] = round(($score / $calcul_pourcentage) * 100);
        
        return $this->sendResponse($infos, 'List challenge retrieved successfully !');
    }

    /**
     * Update the specified ParcoursFormation in storage.
     * PUT/PATCH /parcoursFormations/{id}
     *
     * @param int $id
     * @param UpdateParcoursFormationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateParcoursFormationAPIRequest $request)
    {
        /** @var ParcoursFormation $parcoursFormation */
        $parcoursFormation = Parcours_formation::find($id);

        if (empty($parcoursFormation)) {
            return $this->sendError('Parcours Formation not found');
        }

        $parcoursFormation->fill($request->all());
        $parcoursFormation->save();

        return $this->sendResponse($parcoursFormation->toArray(), 'ParcoursFormation updated successfully');
    }

    /**
     * Remove the specified ParcoursFormation from storage.
     * DELETE /parcoursFormations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ParcoursFormation $parcoursFormation */
        $parcoursFormation = Parcours_formation::find($id);

        if (empty($parcoursFormation)) {
            return $this->sendError('Parcours Formation not found');
        }

        $parcoursFormation->delete();

        return $this->sendSuccess('Parcours Formation deleted successfully');
    }
}
