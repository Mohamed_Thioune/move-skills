<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Token;
use App\Models\Apprenant;
use App\Models\Formateur;
use App\Models\Parcours_formation;
use App\Models\CinetpayTransaction;
use App\Models\Points;

use App\Models\ValidationEmail;

use App\Models\Invitation_accepted;

use App\Http\Requests\LoginAPIRequest;
use App\Http\Requests\RefreshTokenAPIRequest;
use App\Http\Requests\SignUpAPIRequest;
use App\Http\Requests\EmailLogAPIRequest;
use App\Http\Requests\API\CodePasswordAPIRequest;
use App\Http\Requests\API\ResetPasswordAPIRequest;
use App\Http\Requests\API\ForgotPasswordAPIRequest;
use App\Http\Requests\API\AfiliationAPIRequest;

use GuzzleHttp\Exception\ClientException;

use App\Http\Controllers\AppBaseController;
use Laravel\Socialite\Facades\Socialite;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use DB;

use Carbon\Carbon;
use Storage;

define('DURATION', '3456000');

class LoginController extends AppBaseController
{
    public function login(LoginAPIRequest $request){
        $inputs = $request->all();

        $user = User::where('email', $inputs['email'])->first();

        if(!empty($user))
            if(Hash::check($inputs['password'], $user->password)){
                $refresh_token = 'Bearer ' . Str::random(50);
                $access_token = 'Bearer ' . Str::random(50);

                $expired_at = Carbon::now()
                            ->addSeconds(DURATION)
                            ->format('Y-m-d H:i:s');
                $current_date = Carbon::now()
                            ->format('Y-m-d H:i:s');

                $check_token = Token::where('user_id', $user->id)->first();
                $bool = false;
                if($check_token)
                    if($check_token->expired_at){
                        $bool = true;
                        DB::table('tokens')
                            ->where('tokens.user_id', $user->id)
                            ->where('expired_at', '<', $current_date )
                            ->update(['token' => $access_token, 'refresh_token' => $refresh_token, 'expired_at' => $expired_at]);
                    }

                if(!$bool)
                    DB::table('tokens')
                    ->where('tokens.user_id', $user->id)
                    ->update(['token' => $access_token, 'refresh_token' => $refresh_token, 'expired_at' => $expired_at]);

                $token_key =  DB::table('tokens')
                              ->where('tokens.user_id', $user->id)
                              ->first();

                $output = User::find($user->id);
                $output->token = $token_key->token;
                $output->refresh_token = $token_key->refresh_token;
                //Avatar user
                if($user->avatar)
                    $output->avatar = Storage::disk('s3')->url('users/'. $user->avatar);
                else
                    $output->avatar = null;

                return $this->sendResponse($output,'Successfully connected !');
            }

        return $this->sendError('Credentials filled are incorrect !');
    }

    public function refresh_token(RefreshTokenAPIRequest $request){
        $inputs = $request->all();

        $access_token = 'Bearer ' . Str::random(50);
        $expire_at = Carbon::now()
                    ->addSeconds(DURATION)
                    ->format('Y-m-d H:i:s');
        $current_date = Carbon::now()
                    ->format('Y-m-d H:i:s');

        DB::table('tokens')
            ->where('tokens.refresh_token', $inputs['refresh_token'])
            ->where('expired_at', '<', $current_date )
            ->update(['token' => $access_token, 'expired_at' => $expire_at]);

        $token = Token::select('token')->where('refresh_token', $inputs['refresh_token'])->first();

        if(!empty($token))
            return $this->sendResponse($token, 'Access token refresh successfully !');
        else
            return $this->sendError('Refresh token invalid !');
    }

    public function register(SignUpAPIRequest $request){
        $inputs = $request->all();

        $password = bcrypt($inputs["password"]);
        $user = null;
        $user = DB::transaction(function() use ($inputs, $request, $password) {
            $inputs = $request->all();

            $code_rand = mt_rand(10, 99)
                       . mt_rand(10, 99);

            $uni_prenom = substr( $inputs["prenom"], 0, 1);
            $uni_nom = substr( $inputs["nom"], 0, 1);
            $code = strtoupper($uni_nom) . strtoupper($uni_prenom) . $code_rand;

            //Create the User
            $user = User::create([
                'nom' => $inputs["nom"],
                'prenom' => $inputs["prenom"],
                'email' => $inputs["email"],
                'avatar' => NULL,
                'role_id' => 2,
                'address_id' => NULL,
                'password' => $password,
            ]);

            $uni_prenom = substr($user->prenom, 0, 1);
            $uni_nom = substr($user->nom, 0, 1);
            $code = strtoupper($uni_nom) . strtoupper($uni_prenom) . $code_rand;

            DB::table('users')->where('id', $user->id)->update(['code_promo' => $code]);

            //Followed by the 'apprenant'
            Apprenant::create([
                'user_id' => $user->id,
                'couleur_id' => NULL,
                'whatsapp' => NULL,
            ]);

            //Create token dedicated to the user
            if(!empty($user)){
                $expired_at = Carbon::now()
                            ->addSeconds(DURATION)
                            ->format('Y-m-d H:i:s');
                $token_id = Str::random(50);
                $refresh_token_id =  Str::random(50);

                $token = Token::create([
                    'tokenable_type' => "\Model\Apprenant",
                    'token' => 'Bearer ' . $token_id,
                    'user_id' => $user->id,
                    'scope' => 'User',
                    'state' => 1,
                    'refresh_token' => 'Bearer ' . $refresh_token_id,
                    'expired_at' => $expired_at,
                ]);
                $user->code_promo = $code;
                $user->token = $token->token;
                $user->refresh_token = $token->refresh_token;

                //Avatar user
                if($user->avatar)
                    $user->avatar = Storage::disk('s3')->url('users/'. $user->avatar);
                else
                    $user->avatar = null;
            }

            return $user;
        }, 1);

        if(!empty($user)){
            \Mail::to($user->email)->send(new \App\Mail\Welcome($user));
            return $this->sendResponse($user, 'Successfully registered !');
        }

        return $this->sendError('Something went wrong !');
    }

    public function apply_promo(AfiliationAPIRequest $request){
        $token = $request->header('Authorization');
        $inputs = $request->all();

        $token_user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $inputs['user_id'] = $token_user->user_id;

        $user = User::find($inputs['user_id']);

        $user_host = User::where('code_promo', $inputs["code_promo"])->first();
 
        if($user_host){
            if($user_host->avatar)
                $user_host->avatar = Storage::disk('s3')->url('users/'. $user_host->avatar);
            else
                $user_host->avatar = null;

            //Get type user 
            if($user_host->role_id == 2)
                $user_host->type = "Apprenant";
            else if($user_host->role_id == 3)
                $user_host->type = "Coach";

            Invitation_accepted::firstOrCreate(
                ['user_guest_id' => $user->id],

                ['user_host_id' => $user_host->id, 'user_guest_id' => $user->id]
            );

            $point = Points::create(array(
                'value' => 10,
                'user_id'=> $user_host->id
            ));

            // $score = DB::table('points')
            // ->where('user_id', $user->id)
            // ->sum('points.value');
        
            return $this->sendResponse($user_host, 'Code afiliation applied !');
        }
        else
            return response()->json(['success' => false, 'message' => 'Code incorrect, please try with another one ?'], 201);

    }

    public function see_my_guests(Request $request)
    {
        $token = $request->header('Authorization');
        $user_token = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $invitations = array();

        /** @var User $user */
        $user = User::find($user_token->user_id);
        $guests = DB::table('invitation_accepteds')
                    ->where('user_host_id', $user->id)
                    ->get();

        foreach($guests as $value){
            $guest = User::find($value->user_guest_id);
            if($guest)
                if($guest->avatar)
                    $guest->avatar = Storage::disk('s3')->url('users/'. $guest->avatar);
                else
                    $guest->avatar = null;
            array_push($invitations, $guest);
        }

        return $this->sendResponse($invitations, 'These users you have sponsored !');
    }

    public function logout(Request $request){
		$token = $request->header('Authorization');
        $state = DB::table('tokens')
            ->where('tokens.token', $token)
            ->update(['token' => NULL, 'refresh_token' => NULL,'expired_at' => NULL]);

		if($state)
			return $this->sendSuccess('Disconnection successfully done !');
		else
			return $this->sendError('Error encountered, make sure you are already connected !');
	}

    /**
     * Social Login
     */
    public function socialLogin(Request $request, $provider)
    {
        $validated = $this->validateProvider($provider);
        if (!is_null($validated)) {
            return $validated;
        }

        $token = $request->input('access_token');
        // get the provider's user. (In the provider server)
        try {
            $providerUser = Socialite::driver($provider)->userFromToken($token);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $this->sendError($e->getMessage());
        }

        // check if access token exists etc..
        // search for a user in our server with the specified provider id and provider name
        // $user = User::where('provider_name', $provider)->where('provider_id', $providerUser->id)->first();
        $user = User::where('email', $providerUser->email)->first();
        if(!$providerUser->email || !$providerUser->name)
            return $this->sendError('Something went wrong with the provider !');

        $full_name = $providerUser->name;
        $full_names = explode(' ', $full_name);
        $password = Str::random(6);

        $current_date = Carbon::now()
                        ->format('Y-m-d H:i:s');
        $expired_at = Carbon::now()
        ->addSeconds(DURATION)
        ->format('Y-m-d H:i:s');
        $refresh_token = 'Bearer ' . Str::random(50);
        $access_token = 'Bearer ' . Str::random(50);

        if(!$user){
            $user = User::create([
                'provider_name' => $provider,
                'provider_id' => $providerUser->id,
                'avatar' => NULL,
                'nom' => $full_names[0],
                'prenom' => $full_names[1],
                'password' => bcrypt($password),
                'email' => $providerUser->email
            ]);

            //Followed by the 'apprenant'
            Apprenant::create([
                'user_id' => $user->id,
                'couleur_id' => NULL,
                'whatsapp' => NULL,
            ]);

            $code_rand = mt_rand(10, 99)
                        . mt_rand(10, 99);
            $uni_prenom = substr($full_names[0], 0, 1);
            $uni_nom = substr($full_names[1], 0, 1);
            $code = strtoupper($uni_nom) . strtoupper($uni_prenom) . $code_rand;
            DB::table('users')->where('id', $user->id)->update(['code_promo' => $code]);

            // Create a token for the user, so they can login
            $token = Token::create([
                'tokenable_type' => "\Model\Apprenant",
                'token' => $access_token,
                'user_id' => $user->id,
                'scope' => 'User',
                'state' => 1,
                'refresh_token' => $refresh_token,
                'expired_at' => $expired_at,
            ]);
            $output = User::find($user->id);
            $output->token = $token->token;
            $output->refresh_token = $token->refresh_token;
            //Avatar user
            if($user->avatar)
                $output->avatar = Storage::disk('s3')->url('users/'. $user->avatar);
            else
                $output->avatar = null;

            return $this->sendResponse($output,'Successfully registered !');
        }
        else{
            // create a token for the user, so they can login
            $check_token = Token::where('user_id', $user->id)->first();
            $bool = false;
            if($check_token)
                if($check_token->expired_at){
                    $bool = true;
                    DB::table('tokens')
                        ->where('tokens.user_id', $user->id)
                        ->where('expired_at', '<', $current_date )
                        ->update(['token' => $access_token, 'refresh_token' => $refresh_token, 'expired_at' => $expired_at]);
                }

            if(!$bool)
                DB::table('tokens')
                ->where('tokens.user_id', $user->id)
                ->update(['token' => $access_token, 'refresh_token' => $refresh_token, 'expired_at' => $expired_at]);

            $token_key = DB::table('tokens')
                            ->where('tokens.user_id', $user->id)
                            ->first();

            $output = User::find($user->id);
            $output->token = $token_key->token;
            $output->refresh_token = $token_key->refresh_token;

            return $this->sendResponse($output,'Successfully connected !');
        }
    }

    /**
     * @param $provider
     * @return JsonResponse
     */
    protected function validateProvider($provider)
    {
        if (!in_array($provider, ['linkedin', 'google', 'apple'])) {
            return response()->json(['error' => 'Please login using linkedin, apple or google'], 422);
        }
    }

    public function forgot_password(ForgotPasswordAPIRequest $request){
        $input = $request->all();

        $user = User::where('email', $input['email'])->first();

        if(!empty($user)){
            //Generate code
            $code = random_int(100000, 999999);
            $input = [
                'email' => $user->email,
                'code' => $code
            ];
            $validationEmail = ValidationEmail::create($input);
            //Send email
            \Mail::to($user->email)->send(new \App\Mail\ForgotPassword($code));
            return $this->sendResponse($code, 'Code generated successfully !');
        }
        else
            return response()->json(['error' => 'Something went wrong !'], 201);
    }

    public function match_code(CodePasswordAPIRequest $request){
        $input = $request->all();
        $verification = ValidationEmail::where('code', $input['code'])->first();
        if(empty($verification))
            return response()->json(['success' => false, 'message' => 'Verification code not found !'], 201);
        else
            return $this->sendSuccess("Code matched successfully !");
    }

    public function reset_password(ResetPasswordAPIRequest $request){
        $inputs = $request->all();
        $verification = ValidationEmail::where('code', $inputs['code'])->first();
        if(empty($verification))
            return response()->json(['success' => false, 'message' => 'Something went wrong !'], 201);

        $user = User::where('email', $verification->email)->first();

        $user->password = bcrypt($inputs['new_password']);
        $user->save();

        $verification->delete();

        return $this->sendSuccess('Password reset successfully ! ');
    }

    public function callback_cinetpay(){
        return $this->sendSuccess('Callback cinetpay passed !');
    }
    
    public function callback_paydunya(){

        //Instructions here - exploitin data comin through paydunya
        $data = $_POST['data'];
        $status = $data["status"];
        
        //DB archived informations
        // DB::table('test_callback_paydunya')->insert(
        //     ['data' => "Trigger applied"]
        // );
        
        // DB::table('test_callback_paydunya')->insert(
        //     ['data' => $status]
        // );
        
        if($status != "completed")
            return response()->json(['error' => 'Error on transaction !'], 201);

        /** 
         * Transaction completed - add informations into database 
        **/
        $amount = $data['invoice']['total_amount'];
        $user_id = $data['custom_data']['user_id'];
        $parcours_id = $data['custom_data']['parcours_id'];

        $input = array();
        $input['amount'] = intval($amount);
        $input['user_id'] = intval($user_id);
        $input['parcours_formation_id'] = intval($parcours_id);
        $input['type_payment'] = "genuine";
        
        $parcours = Parcours_formation::find($input['parcours_formation_id']);
        if(empty($parcours))
            return $this->sendError('Parcours not found !');

        if($input['amount'] < 0 || $input['amount'] < $parcours->prix )
            return $this->sendError('Incorrect amount !');

        $check = DB::table('cinetpay_transactions')
              ->select('parcours_id', 'user_id', 'amount', 'type_payment', 'updated_at', 'created_at', 'id')
              ->where('parcours_id', $input['parcours_formation_id'])
              ->where('user_id', $input['user_id'])
              ->first();

        if(!empty($check)){
            return $this->sendResponse($check, 'Transaction already exists !');
        }else{    
            $transaction = new CinetpayTransaction;
            $transaction->parcours_id = $input['parcours_formation_id'];
            $transaction->type_payment = $input['type_payment'];
            $transaction->user_id = $input['user_id'];
            $transaction->amount = $input['amount'];
            $transaction->save();

            return $this->sendResponse($transaction, 'Transaction applied !');
        }
        
    }

    public function facture_commands(Request $request){

        $token = $request->header('Authorization');
        $input = $request->all();

        $credit_token = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $input['user_id'] = $credit_token->user_id;
        
        $parcours = Parcours_formation::find($input['parcours_formation_id']);
        if(empty($parcours))
            return response()->json(['error' => 'Parcours not found !'], 201);

        $transaction = DB::table('cinetpay_transactions')
              ->select('parcours_id', 'user_id', 'amount', 'type_payment', 'updated_at', 'created_at', 'id')
              ->where('parcours_id', $input['parcours_formation_id'])
              ->where('user_id', $input['user_id'])
              ->first();

        if(empty($transaction))
            return response()->json(['error' => 'No transaction found !'], 201);
         
        //Get informations User   
        $user = User::find($input['user_id']);
        $parcours = Parcours_formation::find($input['parcours_formation_id']);
        
        $invoice = array();
        $invoice['nom'] = $user->nom;
        $invoice['prenom'] = $user->prenom;
        $invoice['e-mail'] = $user->email;
        $invoice['telephone'] = $user->phone;
        $invoice['adresse'] = "Liberté 1, Scat Urbam";
        $invoice['pays'] = "Senegal";
        $invoice['nom_parcours'] = $parcours->libelle;
        $invoice['description'] = $parcours->description;
        $invoice['quantite'] = 1;
        $invoice['prix'] = $transaction->amount;
        $invoice['date_created_at'] = $transaction->created_at;
        
        return $this->sendResponse($invoice, 'Invoice informations displayed !');
        
    }
    
    public function historiq_commands(Request $request){
        
        $token = $request->header('Authorization');

        $credit_token = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();
        
        $input = array();

        $input['user_id'] = $credit_token->user_id;

        $transactions = DB::table('cinetpay_transactions')
              ->select('parcours_id', 'user_id', 'amount', 'type_payment', 'updated_at', 'created_at', 'id')
              ->where('user_id', $input['user_id'])
              ->get();

        if(empty($transactions))
            return response()->json(['error' => 'No transaction found !'], 201);
            
        $invoices = array();
        foreach($transactions as $transaction){ 
            //Get informations User & Parcours   
            $user = User::find($input['user_id']);
            $parcours = Parcours_formation::find($transaction->parcours_id);
            
            if(!$parcours->prix)
                continue;
                
            $invoice = array();
            // $invoice['nom'] = $user->nom;
            // $invoice['prenom'] = $user->prenom;
            // $invoice['e-mail'] = $user->email;
            // $invoice['telephone'] = $user->phone;
            // $invoice['adresse'] = "Liberté 1, Scat Urbam";
            // $invoice['pays'] = "Senegal";
            $invoice['nom_parcours'] = $parcours->libelle;
            $invoice['description'] = $parcours->description;
            $invoice['quantite'] = 1;
            $invoice['prix'] = $transaction->amount;
            $invoice['date_created_at'] = $transaction->created_at;
            
            array_push($invoices, $invoice);
        }
        
        return $this->sendResponse($invoices, 'Historique commands displayed !');
        
    }

}
