<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCaracteristiqueAPIRequest;
use App\Http\Requests\API\UpdateCaracteristiqueAPIRequest;
use App\Models\Parcours_formation;
use App\Models\Certification;
use App\Models\Points;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Storage;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3V3Adapter;
use League\Flysystem\Filesystem;

use Illuminate\Support\Str;

class CertificationAPIController extends AppBaseController
{

    public function index(Request $request){
        $token = $request->header('Authorization');

        $token_user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        //Certifications
        $certifications = array();
        $raw_certifications = DB::table('certifications')
        ->select('parcours_formations.libelle', 'certifications.url')
        ->join('parcours_formations', 'parcours_formations.id', 'certifications.parcours_formation_id')
        ->where('user_id', $token_user->user_id)
        ->get();

        foreach($raw_certifications as $certification){
            $certification->url = Storage::disk('s3')->url('certifications/'. $certification->url);
            array_push($certifications, $certification);
        }

        if(empty($certifications))
            return $this->sendResponse($certifications, 'No certification !');
        else
            return $this->sendResponse($certifications, 'Certification generated successfully !');

    }

    public function store(Request $request, $parcours_formation_id)
    {
        $token = $request->header('Authorization');

        $token_user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $parcours = Parcours_formation::find($parcours_formation_id);
        if(empty($parcours))
            return $this->sendError('Parcours not found !');

        $check = DB::table('certifications')
              ->where('parcours_formation_id', $parcours_formation_id)
              ->where('user_id', $token_user->user_id)
              ->first();
        if(!empty($check)){
            $certified = Storage::disk('s3')->url('certifications/'. $check->url);

            return $this->sendResponse($certified, 'Certification generated successfully !');
        }else{
            //Score
            $point = Points::create(array(
                'value' => 10,
                'user_id'=> $token_user->user_id
            ));       
            $score = DB::table('points')
            ->where('user_id', $token_user->user_id)
            ->sum('points.value');

            $certification = new Certification;
            $certification->parcours_formation_id = $parcours_formation_id;
            $certification->user_id = $token_user->user_id;
            $certification->save();

            $certification_name = Str::random(20). '.pdf';

            $pdf = Pdf::loadView('certifications/certification_parcours', compact('certification'));
            $filePath = 'certifications/';
            \Storage::disk('s3')->put( $filePath . $certification_name, $pdf->output(), 'public');

            $certified = Storage::disk('s3')->url('certifications/'. $certification_name);

            $grant_certification = DB::table('certifications')
                  ->where('id', $certification->id)
                  ->update(['url' => $certification_name]);

            return $this->sendResponse($certified, 'Certification generated successfully !');
        }
    }
}
