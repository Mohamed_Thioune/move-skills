<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateParcourActiviteAPIRequest;
use App\Http\Requests\API\UpdateParcourActiviteAPIRequest;
use App\Models\ParcourActivite;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ParcourActiviteController
 * @package App\Http\Controllers\API
 */

class ParcourActiviteAPIController extends AppBaseController
{
    /**
     * Display a listing of the ParcourActivite.
     * GET|HEAD /parcourActivites
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = ParcourActivite::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $parcourActivites = $query->get();

        return $this->sendResponse($parcourActivites->toArray(), 'Parcour Activites retrieved successfully');
    }

    /**
     * Store a newly created ParcourActivite in storage.
     * POST /parcourActivites
     *
     * @param CreateParcourActiviteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateParcourActiviteAPIRequest $request)
    {
        $input = $request->all();

        /** @var ParcourActivite $parcourActivite */
        $parcourActivite = ParcourActivite::create($input);

        return $this->sendResponse($parcourActivite->toArray(), 'Parcour Activite saved successfully');
    }

    /**
     * Display the specified ParcourActivite.
     * GET|HEAD /parcourActivites/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ParcourActivite $parcourActivite */
        $parcourActivite = ParcourActivite::find($id);

        if (empty($parcourActivite)) {
            return $this->sendError('Parcour Activite not found');
        }

        return $this->sendResponse($parcourActivite->toArray(), 'Parcour Activite retrieved successfully');
    }

    /**
     * Update the specified ParcourActivite in storage.
     * PUT/PATCH /parcourActivites/{id}
     *
     * @param int $id
     * @param UpdateParcourActiviteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateParcourActiviteAPIRequest $request)
    {
        /** @var ParcourActivite $parcourActivite */
        $parcourActivite = ParcourActivite::find($id);

        if (empty($parcourActivite)) {
            return $this->sendError('Parcour Activite not found');
        }

        $parcourActivite->fill($request->all());
        $parcourActivite->save();

        return $this->sendResponse($parcourActivite->toArray(), 'ParcourActivite updated successfully');
    }

    /**
     * Remove the specified ParcourActivite from storage.
     * DELETE /parcourActivites/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ParcourActivite $parcourActivite */
        $parcourActivite = ParcourActivite::find($id);

        if (empty($parcourActivite)) {
            return $this->sendError('Parcour Activite not found');
        }

        $parcourActivite->delete();

        return $this->sendSuccess('Parcour Activite deleted successfully');
    }
}
