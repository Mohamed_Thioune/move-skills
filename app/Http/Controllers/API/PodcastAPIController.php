<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePodcastAPIRequest;
use App\Http\Requests\API\UpdatePodcastAPIRequest;

use App\Models\Podcast;
use App\Models\Commentaire;
use App\Models\PasserTest;
use App\Models\ParcourActivite;
use App\Models\Points;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

use Storage;
use DB;
/**
 * Class PodcastController
 * @package App\Http\Controllers\API
 */

class PodcastAPIController extends AppBaseController
{
    /**
     * Display a listing of the Podcast.
     * GET|HEAD /podcasts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Podcast::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $podcasts = $query->get();

        return $this->sendResponse($podcasts->toArray(), 'Podcasts retrieved successfully');
    }

    /**
     * Store a newly created Podcast in storage.
     * POST /podcasts
     *
     * @param CreatePodcastAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePodcastAPIRequest $request)
    {
        $input = $request->all();

        /** @var Podcast $podcast */
        $podcast = Podcast::create($input);

        return $this->sendResponse($podcast->toArray(), 'Podcast saved successfully');
    }

    /**
     * Display the specified Podcast.
     * GET|HEAD /podcasts/{id}
     *
     * @param int $id
     *
     * @return Response
     */

    public function read(Request $request, $id)
    {
        /** @var Podcast $podcast */
        $activity = Podcast::find($id);
        $infos = array();

        if (empty($activity)) 
            return $this->sendError('Podcast not found');

        $token = $request->header('Authorization');
        $input = $request->all();

        $token_user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        //Read Activity
        $read = DB::table('parcour_activites')
        ->select('parcours_id')
        ->where('type', $input['type'])
        ->where('activity_id', $activity->id)
        ->where('user_id', $token_user->user_id)
        ->first();

        if(empty($read)){
            $read_activity = new ParcourActivite;
            $read_activity->type = $input['type'];
            $read_activity->activity_id = $activity->id;
            $read_activity->user_id = $token_user->user_id;
            $read_activity->save();

            Points::create(array(
                'value' => 10,
                'user_id'=> $token_user->user_id
            ));
        }

        $score = DB::table('points')
        ->where('user_id', $token_user->user_id)
        ->sum('points.value');
        $infos['score'] = $score;
        $infos['podcast'] = $activity->podcast;
        
        if($activity->podcast)
            return $this->sendResponse($infos, 'Podcast successfully played !');
        else
            return $this->sendError('Podcast content not found !');
    }

    public function show(Request $request, $id)
    {
        /** @var Podcast $podcast */
        $podcast = Podcast::find($id);

        if (empty($podcast)) 
            return $this->sendError('Podcast not found');
        
        $token = $request->header('Authorization');

        $token_user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        $points = 10; 
        $input = array();
        $input['user_id'] = $token_user->user_id;
        $input['type'] = 'podcast';
        $input['podcast_id'] = $podcast->id;

        $read = 0;
        //Read Activity
        $read_podcast = DB::table('parcour_activites')
        ->select('parcours_id')
        ->where('type', 'podcast')
        ->where('activity_id', $podcast->id)
        ->where('user_id', $token_user->user_id)
        ->first();

        if(!empty($read_podcast)){
            $read = 1;
            $podcast->pourcentage = 100;

            $redundance = PasserTest::where('user_id', $input['user_id'])
            ->where('type', $input['type'])
            ->where('podcast_id', $podcast->id)
            ->first();

            if(empty($redundance)){
                $passer_test = new PasserTest();
                $point = Points::create(array(
                'value' => $points,
                'user_id'=> $input['user_id']
                ));

                $input['point_id'] = $point->id;
                $passer_test->fill($input);
                $passer_test->save();
            }   
        }
        
        $formateur = DB::table('podcasts')
                    ->select('users.nom', 'users.prenom')
                    ->join('parcours_formations', 'parcours_formations.id', 'podcasts.parcours_formation_id')
                    ->join('formateurs', 'formateurs.id', 'parcours_formations.formateur_id')
                    ->join('users', 'users.id', 'formateurs.user_id')
                    ->first();

        $podcast->read = $read;

        //Nom & Prenom formateur
        $podcast->nom_formateur = $formateur->nom;
        $podcast->prenom_formateur = $formateur->prenom;

        $token = $request->header('Authorization');
        $user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();

        //Get the quizzes 
        $quizzs = array();
        $raw_quizzs = DB::table('quizzs')
                ->select('question_quizzs.id', 'quizzs.title', 'question_quizzs.libelle')
                ->join('question_quizzs', 'question_quizzs.quizz_id', 'quizzs.id')
                ->where('quizzs.id', $podcast->quizz_id)
                ->get();

        $score = 0;
        foreach($raw_quizzs as $quizz){
            $responses = DB::table('question_quizzs')
                        ->select('responses_quizzs.id', 'responses_quizzs.valeur', 'responses_quizzs.bonne_reponse as correct_response')
                        ->join('responses_quizzs', 'responses_quizzs.question_quizz_id', 'question_quizzs.id')
                        ->where('question_quizzs.id', $quizz->id)
                        ->get();
            $quizz->responses = $responses;
            array_push($quizzs, $quizz);
        }

        // $podcast->quizz = $quizzs;

        $podcast->podcast_url = $podcast->podcast;
              
        $comments = array();
        //Get comments
        $raw_comments = DB::table('commentaires')
                        ->select('text', 'nom', 'prenom', 'avatar', 'commentaires.created_at', 'commentaires.updated_at')
                        ->join('users', 'users.id', 'commentaires.user_id')
                        ->where('podcast_id', $podcast->id)->get();
        foreach($raw_comments as $comment){
            if($comment->avatar)
                $comment->avatar = Storage::disk('s3')->url('images/'. $comment->avatar); 
            else
                $comment->avatar = null;
            array_push($comments, $comment);
        }
       
        if(!empty($comments))
            $podcast->comments = $comments;
      
        return $this->sendResponse($podcast, 'Podcast retrieved successfully');
    }

    /**
     * Update the specified Podcast in storage.
     * PUT/PATCH /podcasts/{id}
     *
     * @param int $id
     * @param UpdatePodcastAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePodcastAPIRequest $request)
    {
        /** @var Podcast $podcast */
        $podcast = Podcast::find($id);

        if (empty($podcast)) {
            return $this->sendError('Podcast not found');
        }

        $podcast->fill($request->all());
        $podcast->save();

        return $this->sendResponse($podcast->toArray(), 'Podcast updated successfully');
    }

    /**
     * Remove the specified Podcast from storage.
     * DELETE /podcasts/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Podcast $podcast */
        $podcast = Podcast::find($id);

        if (empty($podcast)) {
            return $this->sendError('Podcast not found');
        }

        $podcast->delete();

        return $this->sendSuccess('Podcast deleted successfully');
    }
}
