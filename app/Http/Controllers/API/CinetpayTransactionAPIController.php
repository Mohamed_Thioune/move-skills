<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;

use App\Models\CinetpayTransaction;
use App\Models\Parcours_formation;

use DB;

class CinetpayTransactionAPIController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $token = $request->header('Authorization');

        $token_user = DB::table('tokens')
        ->select('tokens.user_id')
        ->where('tokens.token', $token)
        ->first();
        
        $parcours = Parcours_formation::find($input['parcours_formation_id']);
        if(empty($parcours))
            return $this->sendError('Parcours not found !');

        if($input['amount'] < 0 || $input['amount'] < $parcours->prix )
            return $this->sendError('Incorrect amount !');

        $check = DB::table('cinetpay_transactions')
              ->select('parcours_id', 'user_id', 'amount', 'type_payment', 'updated_at', 'created_at', 'id')
              ->where('parcours_id', $input['parcours_formation_id'])
              ->where('user_id', $token_user->user_id)
              ->first();

        if(!empty($check)){
            return $this->sendResponse($check, 'Transaction already exists !');
        }else{    
            $transaction = new CinetpayTransaction;
            $transaction->parcours_id = $input['parcours_formation_id'];
            $transaction->type_payment = $input['type_payment'];
            $transaction->user_id = $token_user->user_id;
            $transaction->amount = $input['amount'];
            $transaction->save();

            return $this->sendResponse($transaction, 'Transaction applied !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
