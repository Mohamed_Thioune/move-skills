<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQuestionChallengeRequest;
use App\Http\Requests\UpdateQuestionChallengeRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\QuestionChallenge;
use Illuminate\Http\Request;
use Flash;
use Response;

class QuestionChallengeController extends AppBaseController
{
    /**
     * Display a listing of the QuestionChallenge.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var QuestionChallenge $questionChallenges */
        $questionChallenges = QuestionChallenge::all();

        return view('question_challenges.index')
            ->with('questionChallenges', $questionChallenges);
    }

    /**
     * Show the form for creating a new QuestionChallenge.
     *
     * @return Response
     */
    public function create()
    {
        return view('question_challenges.create');
    }

    /**
     * Store a newly created QuestionChallenge in storage.
     *
     * @param CreateQuestionChallengeRequest $request
     *
     * @return Response
     */
    public function store(CreateQuestionChallengeRequest $request)
    {
        $input = $request->all();

        /** @var QuestionChallenge $questionChallenge */
        $questionChallenge = QuestionChallenge::create($input);

        Flash::success('Question Challenge saved successfully.');

        return redirect(route('questionChallenges.index'));
    }

    /**
     * Display the specified QuestionChallenge.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var QuestionChallenge $questionChallenge */
        $questionChallenge = QuestionChallenge::find($id);

        if (empty($questionChallenge)) {
            Flash::error('Question Challenge not found');

            return redirect(route('questionChallenges.index'));
        }

        return view('question_challenges.show')->with('questionChallenge', $questionChallenge);
    }

    /**
     * Show the form for editing the specified QuestionChallenge.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var QuestionChallenge $questionChallenge */
        $questionChallenge = QuestionChallenge::find($id);

        if (empty($questionChallenge)) {
            Flash::error('Question Challenge not found');

            return redirect(route('questionChallenges.index'));
        }

        return view('question_challenges.edit')->with('questionChallenge', $questionChallenge);
    }

    /**
     * Update the specified QuestionChallenge in storage.
     *
     * @param int $id
     * @param UpdateQuestionChallengeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQuestionChallengeRequest $request)
    {
        /** @var QuestionChallenge $questionChallenge */
        $questionChallenge = QuestionChallenge::find($id);

        if (empty($questionChallenge)) {
            Flash::error('Question Challenge not found');

            return redirect(route('questionChallenges.index'));
        }

        $questionChallenge->fill($request->all());
        $questionChallenge->save();

        Flash::success('Question Challenge updated successfully.');

        return redirect(route('questionChallenges.index'));
    }

    /**
     * Remove the specified QuestionChallenge from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var QuestionChallenge $questionChallenge */
        $questionChallenge = QuestionChallenge::find($id);

        if (empty($questionChallenge)) {
            Flash::error('Question Challenge not found');

            return redirect(route('questionChallenges.index'));
        }

        $questionChallenge->delete();

        Flash::success('Question Challenge deleted successfully.');

        return redirect(route('questionChallenges.index'));
    }
}
