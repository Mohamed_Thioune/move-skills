<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateApprenantCompetenceRequest;
use App\Http\Requests\UpdateApprenantCompetenceRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\ApprenantCompetence;
use Illuminate\Http\Request;
use Flash;
use Response;

class ApprenantCompetenceController extends AppBaseController
{
    /**
     * Display a listing of the ApprenantCompetence.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var ApprenantCompetence $apprenantCompetences */
        $apprenantCompetences = ApprenantCompetence::all();

        return view('apprenant_competences.index')
            ->with('apprenantCompetences', $apprenantCompetences);
    }

    /**
     * Show the form for creating a new ApprenantCompetence.
     *
     * @return Response
     */
    public function create()
    {
        return view('apprenant_competences.create');
    }

    /**
     * Store a newly created ApprenantCompetence in storage.
     *
     * @param CreateApprenantCompetenceRequest $request
     *
     * @return Response
     */
    public function store(CreateApprenantCompetenceRequest $request)
    {
        $input = $request->all();

        /** @var ApprenantCompetence $apprenantCompetence */
        $apprenantCompetence = ApprenantCompetence::create($input);

        Flash::success('Apprenant Competence saved successfully.');

        return redirect(route('apprenantCompetences.index'));
    }

    /**
     * Display the specified ApprenantCompetence.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ApprenantCompetence $apprenantCompetence */
        $apprenantCompetence = ApprenantCompetence::find($id);

        if (empty($apprenantCompetence)) {
            Flash::error('Apprenant Competence not found');

            return redirect(route('apprenantCompetences.index'));
        }

        return view('apprenant_competences.show')->with('apprenantCompetence', $apprenantCompetence);
    }

    /**
     * Show the form for editing the specified ApprenantCompetence.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var ApprenantCompetence $apprenantCompetence */
        $apprenantCompetence = ApprenantCompetence::find($id);

        if (empty($apprenantCompetence)) {
            Flash::error('Apprenant Competence not found');

            return redirect(route('apprenantCompetences.index'));
        }

        return view('apprenant_competences.edit')->with('apprenantCompetence', $apprenantCompetence);
    }

    /**
     * Update the specified ApprenantCompetence in storage.
     *
     * @param int $id
     * @param UpdateApprenantCompetenceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApprenantCompetenceRequest $request)
    {
        /** @var ApprenantCompetence $apprenantCompetence */
        $apprenantCompetence = ApprenantCompetence::find($id);

        if (empty($apprenantCompetence)) {
            Flash::error('Apprenant Competence not found');

            return redirect(route('apprenantCompetences.index'));
        }

        $apprenantCompetence->fill($request->all());
        $apprenantCompetence->save();

        Flash::success('Apprenant Competence updated successfully.');

        return redirect(route('apprenantCompetences.index'));
    }

    /**
     * Remove the specified ApprenantCompetence from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ApprenantCompetence $apprenantCompetence */
        $apprenantCompetence = ApprenantCompetence::find($id);

        if (empty($apprenantCompetence)) {
            Flash::error('Apprenant Competence not found');

            return redirect(route('apprenantCompetences.index'));
        }

        $apprenantCompetence->delete();

        Flash::success('Apprenant Competence deleted successfully.');

        return redirect(route('apprenantCompetences.index'));
    }
}
