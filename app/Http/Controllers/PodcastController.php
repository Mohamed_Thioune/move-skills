<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePodcastRequest;
use App\Http\Requests\UpdatePodcastRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Podcast;
use Illuminate\Http\Request;
use Flash;
use Response;

class PodcastController extends AppBaseController
{
    /**
     * Display a listing of the Podcast.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Podcast $podcasts */
        $podcasts = Podcast::all();

        return view('podcasts.index')
            ->with('podcasts', $podcasts);
    }

    /**
     * Show the form for creating a new Podcast.
     *
     * @return Response
     */
    public function create()
    {
        return view('podcasts.create');
    }

    /**
     * Store a newly created Podcast in storage.
     *
     * @param CreatePodcastRequest $request
     *
     * @return Response
     */
    public function store(CreatePodcastRequest $request)
    {
        $input = $request->all();

        /** @var Podcast $podcast */
        $podcast = Podcast::create($input);

        Flash::success('Podcast saved successfully.');

        return redirect(route('podcasts.index'));
    }

    /**
     * Display the specified Podcast.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Podcast $podcast */
        $podcast = Podcast::find($id);

        if (empty($podcast)) {
            Flash::error('Podcast not found');

            return redirect(route('podcasts.index'));
        }

        return view('podcasts.show')->with('podcast', $podcast);
    }

    /**
     * Show the form for editing the specified Podcast.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Podcast $podcast */
        $podcast = Podcast::find($id);

        if (empty($podcast)) {
            Flash::error('Podcast not found');

            return redirect(route('podcasts.index'));
        }

        return view('podcasts.edit')->with('podcast', $podcast);
    }

    /**
     * Update the specified Podcast in storage.
     *
     * @param int $id
     * @param UpdatePodcastRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePodcastRequest $request)
    {
        /** @var Podcast $podcast */
        $podcast = Podcast::find($id);

        if (empty($podcast)) {
            Flash::error('Podcast not found');

            return redirect(route('podcasts.index'));
        }

        $podcast->fill($request->all());
        $podcast->save();

        Flash::success('Podcast updated successfully.');

        return redirect(route('podcasts.index'));
    }

    /**
     * Remove the specified Podcast from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Podcast $podcast */
        $podcast = Podcast::find($id);

        if (empty($podcast)) {
            Flash::error('Podcast not found');

            return redirect(route('podcasts.index'));
        }

        $podcast->delete();

        Flash::success('Podcast deleted successfully.');

        return redirect(route('podcasts.index'));
    }
}
