<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateResponseUserQuizzRequest;
use App\Http\Requests\UpdateResponseUserQuizzRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\ResponseUserQuizz;
use Illuminate\Http\Request;
use Flash;
use Response;

class ResponseUserQuizzController extends AppBaseController
{
    /**
     * Display a listing of the ResponseUserQuizz.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var ResponseUserQuizz $responseUserQuizzs */
        $responseUserQuizzs = ResponseUserQuizz::all();

        return view('response_user_quizzs.index')
            ->with('responseUserQuizzs', $responseUserQuizzs);
    }

    /**
     * Show the form for creating a new ResponseUserQuizz.
     *
     * @return Response
     */
    public function create()
    {
        return view('response_user_quizzs.create');
    }

    /**
     * Store a newly created ResponseUserQuizz in storage.
     *
     * @param CreateResponseUserQuizzRequest $request
     *
     * @return Response
     */
    public function store(CreateResponseUserQuizzRequest $request)
    {
        $input = $request->all();

        /** @var ResponseUserQuizz $responseUserQuizz */
        $responseUserQuizz = ResponseUserQuizz::create($input);

        Flash::success('Response User Quizz saved successfully.');

        return redirect(route('responseUserQuizzs.index'));
    }

    /**
     * Display the specified ResponseUserQuizz.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ResponseUserQuizz $responseUserQuizz */
        $responseUserQuizz = ResponseUserQuizz::find($id);

        if (empty($responseUserQuizz)) {
            Flash::error('Response User Quizz not found');

            return redirect(route('responseUserQuizzs.index'));
        }

        return view('response_user_quizzs.show')->with('responseUserQuizz', $responseUserQuizz);
    }

    /**
     * Show the form for editing the specified ResponseUserQuizz.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var ResponseUserQuizz $responseUserQuizz */
        $responseUserQuizz = ResponseUserQuizz::find($id);

        if (empty($responseUserQuizz)) {
            Flash::error('Response User Quizz not found');

            return redirect(route('responseUserQuizzs.index'));
        }

        return view('response_user_quizzs.edit')->with('responseUserQuizz', $responseUserQuizz);
    }

    /**
     * Update the specified ResponseUserQuizz in storage.
     *
     * @param int $id
     * @param UpdateResponseUserQuizzRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResponseUserQuizzRequest $request)
    {
        /** @var ResponseUserQuizz $responseUserQuizz */
        $responseUserQuizz = ResponseUserQuizz::find($id);

        if (empty($responseUserQuizz)) {
            Flash::error('Response User Quizz not found');

            return redirect(route('responseUserQuizzs.index'));
        }

        $responseUserQuizz->fill($request->all());
        $responseUserQuizz->save();

        Flash::success('Response User Quizz updated successfully.');

        return redirect(route('responseUserQuizzs.index'));
    }

    /**
     * Remove the specified ResponseUserQuizz from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ResponseUserQuizz $responseUserQuizz */
        $responseUserQuizz = ResponseUserQuizz::find($id);

        if (empty($responseUserQuizz)) {
            Flash::error('Response User Quizz not found');

            return redirect(route('responseUserQuizzs.index'));
        }

        $responseUserQuizz->delete();

        Flash::success('Response User Quizz deleted successfully.');

        return redirect(route('responseUserQuizzs.index'));
    }
}
