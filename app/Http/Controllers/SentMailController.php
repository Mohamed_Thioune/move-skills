<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSentMailRequest;
use App\Http\Requests\UpdateSentMailRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\SentMail;
use Illuminate\Http\Request;
use Flash;
use Response;

class SentMailController extends AppBaseController
{
    /**
     * Display a listing of the SentMail.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var SentMail $sentMails */
        $sentMails = SentMail::all();

        return view('sent_mails.index')
            ->with('sentMails', $sentMails);
    }

    /**
     * Show the form for creating a new SentMail.
     *
     * @return Response
     */
    public function create()
    {
        return view('sent_mails.create');
    }

    /**
     * Store a newly created SentMail in storage.
     *
     * @param CreateSentMailRequest $request
     *
     * @return Response
     */
    public function store(CreateSentMailRequest $request)
    {
        $input = $request->all();

        /** @var SentMail $sentMail */
        $sentMail = SentMail::create($input);

        Flash::success('Sent Mail saved successfully.');

        return redirect(route('sentMails.index'));
    }

    /**
     * Display the specified SentMail.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SentMail $sentMail */
        $sentMail = SentMail::find($id);

        if (empty($sentMail)) {
            Flash::error('Sent Mail not found');

            return redirect(route('sentMails.index'));
        }

        return view('sent_mails.show')->with('sentMail', $sentMail);
    }

    /**
     * Show the form for editing the specified SentMail.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var SentMail $sentMail */
        $sentMail = SentMail::find($id);

        if (empty($sentMail)) {
            Flash::error('Sent Mail not found');

            return redirect(route('sentMails.index'));
        }

        return view('sent_mails.edit')->with('sentMail', $sentMail);
    }

    /**
     * Update the specified SentMail in storage.
     *
     * @param int $id
     * @param UpdateSentMailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSentMailRequest $request)
    {
        /** @var SentMail $sentMail */
        $sentMail = SentMail::find($id);

        if (empty($sentMail)) {
            Flash::error('Sent Mail not found');

            return redirect(route('sentMails.index'));
        }

        $sentMail->fill($request->all());
        $sentMail->save();

        Flash::success('Sent Mail updated successfully.');

        return redirect(route('sentMails.index'));
    }

    /**
     * Remove the specified SentMail from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SentMail $sentMail */
        $sentMail = SentMail::find($id);

        if (empty($sentMail)) {
            Flash::error('Sent Mail not found');

            return redirect(route('sentMails.index'));
        }

        $sentMail->delete();

        Flash::success('Sent Mail deleted successfully.');

        return redirect(route('sentMails.index'));
    }
}
