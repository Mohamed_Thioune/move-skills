<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQuestionQuizzRequest;
use App\Http\Requests\UpdateQuestionQuizzRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\QuestionQuizz;
use Illuminate\Http\Request;
use Flash;
use Response;

class QuestionQuizzController extends AppBaseController
{
    /**
     * Display a listing of the QuestionQuizz.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var QuestionQuizz $QuestionQuizzs */
        $QuestionQuizzs = QuestionQuizz::all();

        return view('questions_quizzs.index')
            ->with('QuestionQuizzs', $QuestionQuizzs);
    }

    /**
     * Show the form for creating a new QuestionQuizz.
     *
     * @return Response
     */
    public function create()
    {
        return view('questions_quizzs.create');
    }

    /**
     * Store a newly created QuestionQuizz in storage.
     *
     * @param CreateQuestionQuizzRequest $request
     *
     * @return Response
     */
    public function store(CreateQuestionQuizzRequest $request)
    {
        $input = $request->all();

        /** @var QuestionQuizz $QuestionQuizz */
        $QuestionQuizz = QuestionQuizz::create($input);

        Flash::success('Questions Quizz saved successfully.');

        return redirect(route('QuestionQuizzs.index'));
    }

    /**
     * Display the specified QuestionQuizz.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var QuestionQuizz $QuestionQuizz */
        $QuestionQuizz = QuestionQuizz::find($id);

        if (empty($QuestionQuizz)) {
            Flash::error('Questions Quizz not found');

            return redirect(route('QuestionQuizzs.index'));
        }

        return view('questions_quizzs.show')->with('QuestionQuizz', $QuestionQuizz);
    }

    /**
     * Show the form for editing the specified QuestionQuizz.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var QuestionQuizz $QuestionQuizz */
        $QuestionQuizz = QuestionQuizz::find($id);

        if (empty($QuestionQuizz)) {
            Flash::error('Questions Quizz not found');

            return redirect(route('QuestionQuizzs.index'));
        }

        return view('questions_quizzs.edit')->with('QuestionQuizz', $QuestionQuizz);
    }

    /**
     * Update the specified QuestionQuizz in storage.
     *
     * @param int $id
     * @param UpdateQuestionQuizzRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQuestionQuizzRequest $request)
    {
        /** @var QuestionQuizz $QuestionQuizz */
        $QuestionQuizz = QuestionQuizz::find($id);

        if (empty($QuestionQuizz)) {
            Flash::error('Questions Quizz not found');

            return redirect(route('QuestionQuizzs.index'));
        }

        $QuestionQuizz->fill($request->all());
        $QuestionQuizz->save();

        Flash::success('Questions Quizz updated successfully.');

        return redirect(route('QuestionQuizzs.index'));
    }

    /**
     * Remove the specified QuestionQuizz from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var QuestionQuizz $QuestionQuizz */
        $QuestionQuizz = QuestionQuizz::find($id);

        if (empty($QuestionQuizz)) {
            Flash::error('Questions Quizz not found');

            return redirect(route('QuestionQuizzs.index'));
        }

        $QuestionQuizz->delete();

        Flash::success('Questions Quizz deleted successfully.');

        return redirect(route('QuestionQuizzs.index'));
    }
}
