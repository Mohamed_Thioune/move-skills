<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateResponsesQuizzRequest;
use App\Http\Requests\UpdateResponsesQuizzRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\ResponsesQuizz;
use Illuminate\Http\Request;
use Flash;
use Response;

class ResponsesQuizzController extends AppBaseController
{
    /**
     * Display a listing of the ResponsesQuizz.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var ResponsesQuizz $responsesQuizzs */
        $responsesQuizzs = ResponsesQuizz::all();

        return view('responses_quizzs.index')
            ->with('responsesQuizzs', $responsesQuizzs);
    }

    /**
     * Show the form for creating a new ResponsesQuizz.
     *
     * @return Response
     */
    public function create()
    {
        return view('responses_quizzs.create');
    }

    /**
     * Store a newly created ResponsesQuizz in storage.
     *
     * @param CreateResponsesQuizzRequest $request
     *
     * @return Response
     */
    public function store(CreateResponsesQuizzRequest $request)
    {
        $input = $request->all();

        /** @var ResponsesQuizz $responsesQuizz */
        $responsesQuizz = ResponsesQuizz::create($input);

        Flash::success('Responses Quizz saved successfully.');

        return redirect(route('responsesQuizzs.index'));
    }

    /**
     * Display the specified ResponsesQuizz.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ResponsesQuizz $responsesQuizz */
        $responsesQuizz = ResponsesQuizz::find($id);

        if (empty($responsesQuizz)) {
            Flash::error('Responses Quizz not found');

            return redirect(route('responsesQuizzs.index'));
        }

        return view('responses_quizzs.show')->with('responsesQuizz', $responsesQuizz);
    }

    /**
     * Show the form for editing the specified ResponsesQuizz.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var ResponsesQuizz $responsesQuizz */
        $responsesQuizz = ResponsesQuizz::find($id);

        if (empty($responsesQuizz)) {
            Flash::error('Responses Quizz not found');

            return redirect(route('responsesQuizzs.index'));
        }

        return view('responses_quizzs.edit')->with('responsesQuizz', $responsesQuizz);
    }

    /**
     * Update the specified ResponsesQuizz in storage.
     *
     * @param int $id
     * @param UpdateResponsesQuizzRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResponsesQuizzRequest $request)
    {
        /** @var ResponsesQuizz $responsesQuizz */
        $responsesQuizz = ResponsesQuizz::find($id);

        if (empty($responsesQuizz)) {
            Flash::error('Responses Quizz not found');

            return redirect(route('responsesQuizzs.index'));
        }

        $responsesQuizz->fill($request->all());
        $responsesQuizz->save();

        Flash::success('Responses Quizz updated successfully.');

        return redirect(route('responsesQuizzs.index'));
    }

    /**
     * Remove the specified ResponsesQuizz from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ResponsesQuizz $responsesQuizz */
        $responsesQuizz = ResponsesQuizz::find($id);

        if (empty($responsesQuizz)) {
            Flash::error('Responses Quizz not found');

            return redirect(route('responsesQuizzs.index'));
        }

        $responsesQuizz->delete();

        Flash::success('Responses Quizz deleted successfully.');

        return redirect(route('responsesQuizzs.index'));
    }
}
