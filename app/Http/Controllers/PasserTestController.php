<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePasserTestRequest;
use App\Http\Requests\UpdatePasserTestRequest;
use App\Repositories\PasserTestRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PasserTestController extends AppBaseController
{
    /** @var PasserTestRepository $passerTestRepository*/
    private $passerTestRepository;

    public function __construct(PasserTestRepository $passerTestRepo)
    {
        $this->passerTestRepository = $passerTestRepo;
    }

    /**
     * Display a listing of the PasserTest.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $passerTests = $this->passerTestRepository->all();

        return view('passer_tests.index')
            ->with('passerTests', $passerTests);
    }

    /**
     * Show the form for creating a new PasserTest.
     *
     * @return Response
     */
    public function create()
    {
        return view('passer_tests.create');
    }

    /**
     * Store a newly created PasserTest in storage.
     *
     * @param CreatePasserTestRequest $request
     *
     * @return Response
     */
    public function store(CreatePasserTestRequest $request)
    {
        $input = $request->all();

        $passerTest = $this->passerTestRepository->create($input);

        Flash::success('Passer Test saved successfully.');

        return redirect(route('passerTests.index'));
    }

    /**
     * Display the specified PasserTest.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $passerTest = $this->passerTestRepository->find($id);

        if (empty($passerTest)) {
            Flash::error('Passer Test not found');

            return redirect(route('passerTests.index'));
        }

        return view('passer_tests.show')->with('passerTest', $passerTest);
    }

    /**
     * Show the form for editing the specified PasserTest.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $passerTest = $this->passerTestRepository->find($id);

        if (empty($passerTest)) {
            Flash::error('Passer Test not found');

            return redirect(route('passerTests.index'));
        }

        return view('passer_tests.edit')->with('passerTest', $passerTest);
    }

    /**
     * Update the specified PasserTest in storage.
     *
     * @param int $id
     * @param UpdatePasserTestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePasserTestRequest $request)
    {
        $passerTest = $this->passerTestRepository->find($id);

        if (empty($passerTest)) {
            Flash::error('Passer Test not found');

            return redirect(route('passerTests.index'));
        }

        $passerTest = $this->passerTestRepository->update($request->all(), $id);

        Flash::success('Passer Test updated successfully.');

        return redirect(route('passerTests.index'));
    }

    /**
     * Remove the specified PasserTest from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $passerTest = $this->passerTestRepository->find($id);

        if (empty($passerTest)) {
            Flash::error('Passer Test not found');

            return redirect(route('passerTests.index'));
        }

        $this->passerTestRepository->delete($id);

        Flash::success('Passer Test deleted successfully.');

        return redirect(route('passerTests.index'));
    }
}
