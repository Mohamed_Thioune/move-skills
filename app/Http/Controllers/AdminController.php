<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;
use Auth;
use Mail; 
use Illuminate\Contracts\Encryption\DecryptException;
use Crypt;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Formateur;

use Session;
use Flash;

class AdminController extends Controller
{
    //

     use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    } 


    public function dashboard()
    {
     
     return view('home');
    }

    public function login(Request $request)
    {
      
      if($request->isMethod('post')){
            $admin = "Bienvenue dans l'espace formateur";
            $message = 'Email ou mot de passe incorrect';

            $data = $request->input();
            
            if(Auth::attempt(['email'=>$data['email'], 'password'=>$data['password'], 'role_id'=> '2'])){

                //echo "succes"; die;
                return redirect('/admin/dashboard')->with(['admin' => $admin]);
            }
           
             
            else{
                Flash::error($message);

                return redirect('/connexion')->with(['message' => $message]);  
            }
        }
        return view('login');
    }

    public function connexion()
    {
     
     return view('login');
    }

    public function inscription()
    {
     
     return view('register');
    }

    public function inscription_store(Request $request)
     {
         //
 
         request()->validate([
             'nom' => 'required|string|max:255',
             'prenom' => 'required|string|max:255',
             'email' => 'required|string|email|max:255|unique:users',
             'password' => 'required|string|min:6|confirmed',
 
     ]);
 
         
             $message = "Ajouté avec succès";

             $user = new User;
             $user->prenom = $request->get('prenom'); 
             $user->nom = $request->get('nom'); 
             $user->email = $request->get('email'); 
             $user->role_id = 2; 
             $user->password = Hash::make($request->get('password'));

             if($user->save()) 
             {
                $formateur = new Formateur;
                $formateur->user_id = $user->id;
                $formateur->save();

                 Auth::login($user);
                 return redirect('/admin/dashboard')->with(['message' => $message]);
     
             }
             else
             {
                 flash('user not saved')->error();
     
             }
     
     
     return back()->with(['message' => $message]);
 
     }


}
