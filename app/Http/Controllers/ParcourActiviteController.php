<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateParcourActiviteRequest;
use App\Http\Requests\UpdateParcourActiviteRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\ParcourActivite;
use Illuminate\Http\Request;
use Flash;
use Response;

class ParcourActiviteController extends AppBaseController
{
    /**
     * Display a listing of the ParcourActivite.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var ParcourActivite $parcourActivites */
        $parcourActivites = ParcourActivite::all();

        return view('parcour_activites.index')
            ->with('parcourActivites', $parcourActivites);
    }

    /**
     * Show the form for creating a new ParcourActivite.
     *
     * @return Response
     */
    public function create()
    {
        return view('parcour_activites.create');
    }

    /**
     * Store a newly created ParcourActivite in storage.
     *
     * @param CreateParcourActiviteRequest $request
     *
     * @return Response
     */
    public function store(CreateParcourActiviteRequest $request)
    {
        $input = $request->all();

        /** @var ParcourActivite $parcourActivite */
        $parcourActivite = ParcourActivite::create($input);

        Flash::success('Parcour Activite saved successfully.');

        return redirect(route('parcourActivites.index'));
    }

    /**
     * Display the specified ParcourActivite.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ParcourActivite $parcourActivite */
        $parcourActivite = ParcourActivite::find($id);

        if (empty($parcourActivite)) {
            Flash::error('Parcour Activite not found');

            return redirect(route('parcourActivites.index'));
        }

        return view('parcour_activites.show')->with('parcourActivite', $parcourActivite);
    }

    /**
     * Show the form for editing the specified ParcourActivite.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var ParcourActivite $parcourActivite */
        $parcourActivite = ParcourActivite::find($id);

        if (empty($parcourActivite)) {
            Flash::error('Parcour Activite not found');

            return redirect(route('parcourActivites.index'));
        }

        return view('parcour_activites.edit')->with('parcourActivite', $parcourActivite);
    }

    /**
     * Update the specified ParcourActivite in storage.
     *
     * @param int $id
     * @param UpdateParcourActiviteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateParcourActiviteRequest $request)
    {
        /** @var ParcourActivite $parcourActivite */
        $parcourActivite = ParcourActivite::find($id);

        if (empty($parcourActivite)) {
            Flash::error('Parcour Activite not found');

            return redirect(route('parcourActivites.index'));
        }

        $parcourActivite->fill($request->all());
        $parcourActivite->save();

        Flash::success('Parcour Activite updated successfully.');

        return redirect(route('parcourActivites.index'));
    }

    /**
     * Remove the specified ParcourActivite from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ParcourActivite $parcourActivite */
        $parcourActivite = ParcourActivite::find($id);

        if (empty($parcourActivite)) {
            Flash::error('Parcour Activite not found');

            return redirect(route('parcourActivites.index'));
        }

        $parcourActivite->delete();

        Flash::success('Parcour Activite deleted successfully.');

        return redirect(route('parcourActivites.index'));
    }
}
