<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCaracteristiqueRequest;
use App\Http\Requests\UpdateCaracteristiqueRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Caracteristique;
use Illuminate\Http\Request;
use Flash;
use Response;

class CaracteristiqueController extends AppBaseController
{
    /**
     * Display a listing of the Caracteristique.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Caracteristique $caracteristiques */
        $caracteristiques = Caracteristique::all();

        return view('caracteristiques.index')
            ->with('caracteristiques', $caracteristiques);
    }

    /**
     * Show the form for creating a new Caracteristique.
     *
     * @return Response
     */
    public function create()
    {
        return view('caracteristiques.create');
    }

    /**
     * Store a newly created Caracteristique in storage.
     *
     * @param CreateCaracteristiqueRequest $request
     *
     * @return Response
     */
    public function store(CreateCaracteristiqueRequest $request)
    {
        $input = $request->all();

        /** @var Caracteristique $caracteristique */
        $caracteristique = Caracteristique::create($input);

        Flash::success('Caracteristique saved successfully.');

        return redirect(route('caracteristiques.index'));
    }

    /**
     * Display the specified Caracteristique.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Caracteristique $caracteristique */
        $caracteristique = Caracteristique::find($id);

        if (empty($caracteristique)) {
            Flash::error('Caracteristique not found');

            return redirect(route('caracteristiques.index'));
        }

        return view('caracteristiques.show')->with('caracteristique', $caracteristique);
    }

    /**
     * Show the form for editing the specified Caracteristique.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Caracteristique $caracteristique */
        $caracteristique = Caracteristique::find($id);

        if (empty($caracteristique)) {
            Flash::error('Caracteristique not found');

            return redirect(route('caracteristiques.index'));
        }

        return view('caracteristiques.edit')->with('caracteristique', $caracteristique);
    }

    /**
     * Update the specified Caracteristique in storage.
     *
     * @param int $id
     * @param UpdateCaracteristiqueRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCaracteristiqueRequest $request)
    {
        /** @var Caracteristique $caracteristique */
        $caracteristique = Caracteristique::find($id);

        if (empty($caracteristique)) {
            Flash::error('Caracteristique not found');

            return redirect(route('caracteristiques.index'));
        }

        $caracteristique->fill($request->all());
        $caracteristique->save();

        Flash::success('Caracteristique updated successfully.');

        return redirect(route('caracteristiques.index'));
    }

    /**
     * Remove the specified Caracteristique from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Caracteristique $caracteristique */
        $caracteristique = Caracteristique::find($id);

        if (empty($caracteristique)) {
            Flash::error('Caracteristique not found');

            return redirect(route('caracteristiques.index'));
        }

        $caracteristique->delete();

        Flash::success('Caracteristique deleted successfully.');

        return redirect(route('caracteristiques.index'));
    }
}
