<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateResponseByUserDiscRequest;
use App\Http\Requests\UpdateResponseByUserDiscRequest;
use App\Repositories\ResponseByUserDiscRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ResponseByUserDiscController extends AppBaseController
{
    /** @var ResponseByUserDiscRepository $responseByUserDiscRepository*/
    private $responseByUserDiscRepository;

    public function __construct(ResponseByUserDiscRepository $responseByUserDiscRepo)
    {
        $this->responseByUserDiscRepository = $responseByUserDiscRepo;
    }

    /**
     * Display a listing of the ResponseByUserDisc.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $responseByUserDiscs = $this->responseByUserDiscRepository->all();

        return view('response_by_user_discs.index')
            ->with('responseByUserDiscs', $responseByUserDiscs);
    }

    /**
     * Show the form for creating a new ResponseByUserDisc.
     *
     * @return Response
     */
    public function create()
    {
        return view('response_by_user_discs.create');
    }

    /**
     * Store a newly created ResponseByUserDisc in storage.
     *
     * @param CreateResponseByUserDiscRequest $request
     *
     * @return Response
     */
    public function store(CreateResponseByUserDiscRequest $request)
    {
        $input = $request->all();

        $responseByUserDisc = $this->responseByUserDiscRepository->create($input);

        Flash::success('Response By User Disc saved successfully.');

        return redirect(route('responseByUserDiscs.index'));
    }

    /**
     * Display the specified ResponseByUserDisc.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $responseByUserDisc = $this->responseByUserDiscRepository->find($id);

        if (empty($responseByUserDisc)) {
            Flash::error('Response By User Disc not found');

            return redirect(route('responseByUserDiscs.index'));
        }

        return view('response_by_user_discs.show')->with('responseByUserDisc', $responseByUserDisc);
    }

    /**
     * Show the form for editing the specified ResponseByUserDisc.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $responseByUserDisc = $this->responseByUserDiscRepository->find($id);

        if (empty($responseByUserDisc)) {
            Flash::error('Response By User Disc not found');

            return redirect(route('responseByUserDiscs.index'));
        }

        return view('response_by_user_discs.edit')->with('responseByUserDisc', $responseByUserDisc);
    }

    /**
     * Update the specified ResponseByUserDisc in storage.
     *
     * @param int $id
     * @param UpdateResponseByUserDiscRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResponseByUserDiscRequest $request)
    {
        $responseByUserDisc = $this->responseByUserDiscRepository->find($id);

        if (empty($responseByUserDisc)) {
            Flash::error('Response By User Disc not found');

            return redirect(route('responseByUserDiscs.index'));
        }

        $responseByUserDisc = $this->responseByUserDiscRepository->update($request->all(), $id);

        Flash::success('Response By User Disc updated successfully.');

        return redirect(route('responseByUserDiscs.index'));
    }

    /**
     * Remove the specified ResponseByUserDisc from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $responseByUserDisc = $this->responseByUserDiscRepository->find($id);

        if (empty($responseByUserDisc)) {
            Flash::error('Response By User Disc not found');

            return redirect(route('responseByUserDiscs.index'));
        }

        $this->responseByUserDiscRepository->delete($id);

        Flash::success('Response By User Disc deleted successfully.');

        return redirect(route('responseByUserDiscs.index'));
    }
}
