<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateApprenantRequest;
use App\Http\Requests\UpdateApprenantRequest;
use App\Repositories\ApprenantRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ApprenantController extends AppBaseController
{
    /** @var ApprenantRepository $apprenantRepository*/
    private $apprenantRepository;

    public function __construct(ApprenantRepository $apprenantRepo)
    {
        $this->apprenantRepository = $apprenantRepo;
    }

    /**
     * Display a listing of the Apprenant.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $apprenants = $this->apprenantRepository->all();

        return view('apprenants.index')
            ->with('apprenants', $apprenants);
    }

    /**
     * Show the form for creating a new Apprenant.
     *
     * @return Response
     */
    public function create()
    {
        return view('apprenants.create');
    }

    /**
     * Store a newly created Apprenant in storage.
     *
     * @param CreateApprenantRequest $request
     *
     * @return Response
     */
    public function store(CreateApprenantRequest $request)
    {
        $input = $request->all();

        $apprenant = $this->apprenantRepository->create($input);

        Flash::success('Apprenant saved successfully.');

        return redirect(route('apprenants.index'));
    }

    /**
     * Display the specified Apprenant.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $apprenant = $this->apprenantRepository->find($id);

        if (empty($apprenant)) {
            Flash::error('Apprenant not found');

            return redirect(route('apprenants.index'));
        }

        return view('apprenants.show')->with('apprenant', $apprenant);
    }

    /**
     * Show the form for editing the specified Apprenant.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $apprenant = $this->apprenantRepository->find($id);

        if (empty($apprenant)) {
            Flash::error('Apprenant not found');

            return redirect(route('apprenants.index'));
        }

        return view('apprenants.edit')->with('apprenant', $apprenant);
    }

    /**
     * Update the specified Apprenant in storage.
     *
     * @param int $id
     * @param UpdateApprenantRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApprenantRequest $request)
    {
        $apprenant = $this->apprenantRepository->find($id);

        if (empty($apprenant)) {
            Flash::error('Apprenant not found');

            return redirect(route('apprenants.index'));
        }

        $apprenant = $this->apprenantRepository->update($request->all(), $id);

        Flash::success('Apprenant updated successfully.');

        return redirect(route('apprenants.index'));
    }

    /**
     * Remove the specified Apprenant from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $apprenant = $this->apprenantRepository->find($id);

        if (empty($apprenant)) {
            Flash::error('Apprenant not found');

            return redirect(route('apprenants.index'));
        }

        $this->apprenantRepository->delete($id);

        Flash::success('Apprenant deleted successfully.');

        return redirect(route('apprenants.index'));
    }
}
