<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatechallengeRequest;
use App\Http\Requests\UpdatechallengeRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Challenge;
use Illuminate\Http\Request;
use Flash;
use Response;

class ChallengeController extends AppBaseController
{
    /**
     * Display a listing of the challenge.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var challenge $challenges */
        $challenges = Challenge::all();

        return view('challenges.index')
            ->with('challenges', $challenges);
    }

    /**
     * Show the form for creating a new challenge.
     *
     * @return Response
     */
    public function create()
    {
        return view('challenges.create');
    }

    /**
     * Store a newly created challenge in storage.
     *
     * @param CreatechallengeRequest $request
     *
     * @return Response
     */
    public function store(CreatechallengeRequest $request)
    {
        $input = $request->all();

        /** @var challenge $challenge */
        $challenge = Challenge::create($input);

        Flash::success('Challenge saved successfully.');

        return redirect(route('challenges.index'));
    }

    /**
     * Display the specified challenge.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var challenge $challenge */
        $challenge = Challenge::find($id);

        if (empty($challenge)) {
            Flash::error('Challenge not found');

            return redirect(route('challenges.index'));
        }

        return view('challenges.show')->with('challenge', $challenge);
    }

    /**
     * Show the form for editing the specified challenge.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var challenge $challenge */
        $challenge = Challenge::find($id);

        if (empty($challenge)) {
            Flash::error('Challenge not found');

            return redirect(route('challenges.index'));
        }

        return view('challenges.edit')->with('challenge', $challenge);
    }

    /**
     * Update the specified challenge in storage.
     *
     * @param int $id
     * @param UpdatechallengeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatechallengeRequest $request)
    {
        /** @var challenge $challenge */
        $challenge = Challenge::find($id);

        if (empty($challenge)) {
            Flash::error('Challenge not found');

            return redirect(route('challenges.index'));
        }

        $challenge->fill($request->all());
        $challenge->save();

        Flash::success('Challenge updated successfully.');

        return redirect(route('challenges.index'));
    }

    /**
     * Remove the specified challenge from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var challenge $challenge */
        $challenge = Challenge::find($id);

        if (empty($challenge)) {
            Flash::error('Challenge not found');

            return redirect(route('challenges.index'));
        }

        $challenge->delete();

        Flash::success('Challenge deleted successfully.');

        return redirect(route('challenges.index'));
    }
}
