<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCommentaireRequest;
use App\Http\Requests\UpdateCommentaireRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Commentaire;
use Illuminate\Http\Request;
use Flash;
use Response;

class CommentaireController extends AppBaseController
{
    /**
     * Display a listing of the Commentaire.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Commentaire $commentaires */
        $commentaires = Commentaire::all();

        return view('commentaires.index')
            ->with('commentaires', $commentaires);
    }

    /**
     * Show the form for creating a new Commentaire.
     *
     * @return Response
     */
    public function create()
    {
        return view('commentaires.create');
    }

    /**
     * Store a newly created Commentaire in storage.
     *
     * @param CreateCommentaireRequest $request
     *
     * @return Response
     */
    public function store(CreateCommentaireRequest $request)
    {
        $input = $request->all();

        /** @var Commentaire $commentaire */
        $commentaire = Commentaire::create($input);

        Flash::success('Commentaire saved successfully.');

        return redirect(route('commentaires.index'));
    }

    /**
     * Display the specified Commentaire.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Commentaire $commentaire */
        $commentaire = Commentaire::find($id);

        if (empty($commentaire)) {
            Flash::error('Commentaire not found');

            return redirect(route('commentaires.index'));
        }

        return view('commentaires.show')->with('commentaire', $commentaire);
    }

    /**
     * Show the form for editing the specified Commentaire.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Commentaire $commentaire */
        $commentaire = Commentaire::find($id);

        if (empty($commentaire)) {
            Flash::error('Commentaire not found');

            return redirect(route('commentaires.index'));
        }

        return view('commentaires.edit')->with('commentaire', $commentaire);
    }

    /**
     * Update the specified Commentaire in storage.
     *
     * @param int $id
     * @param UpdateCommentaireRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommentaireRequest $request)
    {
        /** @var Commentaire $commentaire */
        $commentaire = Commentaire::find($id);

        if (empty($commentaire)) {
            Flash::error('Commentaire not found');

            return redirect(route('commentaires.index'));
        }

        $commentaire->fill($request->all());
        $commentaire->save();

        Flash::success('Commentaire updated successfully.');

        return redirect(route('commentaires.index'));
    }

    /**
     * Remove the specified Commentaire from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Commentaire $commentaire */
        $commentaire = Commentaire::find($id);

        if (empty($commentaire)) {
            Flash::error('Commentaire not found');

            return redirect(route('commentaires.index'));
        }

        $commentaire->delete();

        Flash::success('Commentaire deleted successfully.');

        return redirect(route('commentaires.index'));
    }
}
