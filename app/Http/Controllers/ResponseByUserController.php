<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateResponseByUserRequest;
use App\Http\Requests\UpdateResponseByUserRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\ResponseByUser;
use Illuminate\Http\Request;
use Flash;
use Response;

class ResponseByUserController extends AppBaseController
{
    /**
     * Display a listing of the ResponseByUser.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var ResponseByUser $responseByUsers */
        $responseByUsers = ResponseByUser::all();

        return view('response_by_users.index')
            ->with('responseByUsers', $responseByUsers);
    }

    /**
     * Show the form for creating a new ResponseByUser.
     *
     * @return Response
     */
    public function create()
    {
        return view('response_by_users.create');
    }

    /**
     * Store a newly created ResponseByUser in storage.
     *
     * @param CreateResponseByUserRequest $request
     *
     * @return Response
     */
    public function store(CreateResponseByUserRequest $request)
    {
        $input = $request->all();

        /** @var ResponseByUser $responseByUser */
        $responseByUser = ResponseByUser::create($input);

        Flash::success('Response By User saved successfully.');

        return redirect(route('responseByUsers.index'));
    }

    /**
     * Display the specified ResponseByUser.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ResponseByUser $responseByUser */
        $responseByUser = ResponseByUser::find($id);

        if (empty($responseByUser)) {
            Flash::error('Response By User not found');

            return redirect(route('responseByUsers.index'));
        }

        return view('response_by_users.show')->with('responseByUser', $responseByUser);
    }

    /**
     * Show the form for editing the specified ResponseByUser.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var ResponseByUser $responseByUser */
        $responseByUser = ResponseByUser::find($id);

        if (empty($responseByUser)) {
            Flash::error('Response By User not found');

            return redirect(route('responseByUsers.index'));
        }

        return view('response_by_users.edit')->with('responseByUser', $responseByUser);
    }

    /**
     * Update the specified ResponseByUser in storage.
     *
     * @param int $id
     * @param UpdateResponseByUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResponseByUserRequest $request)
    {
        /** @var ResponseByUser $responseByUser */
        $responseByUser = ResponseByUser::find($id);

        if (empty($responseByUser)) {
            Flash::error('Response By User not found');

            return redirect(route('responseByUsers.index'));
        }

        $responseByUser->fill($request->all());
        $responseByUser->save();

        Flash::success('Response By User updated successfully.');

        return redirect(route('responseByUsers.index'));
    }

    /**
     * Remove the specified ResponseByUser from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ResponseByUser $responseByUser */
        $responseByUser = ResponseByUser::find($id);

        if (empty($responseByUser)) {
            Flash::error('Response By User not found');

            return redirect(route('responseByUsers.index'));
        }

        $responseByUser->delete();

        Flash::success('Response By User deleted successfully.');

        return redirect(route('responseByUsers.index'));
    }
}
