<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateParcoursFormationRequest;
use App\Http\Requests\UpdateParcoursFormationRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\ParcoursFormation;
use Illuminate\Http\Request;
use Flash;
use Response;

class ParcoursFormationController extends AppBaseController
{
    /**
     * Display a listing of the ParcoursFormation.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var ParcoursFormation $parcoursFormations */
        $parcoursFormations = ParcoursFormation::all();

        return view('parcours_formations.index')
            ->with('parcoursFormations', $parcoursFormations);
    }

    /**
     * Show the form for creating a new ParcoursFormation.
     *
     * @return Response
     */
    public function create()
    {
        return view('parcours_formations.create');
    }

    /**
     * Store a newly created ParcoursFormation in storage.
     *
     * @param CreateParcoursFormationRequest $request
     *
     * @return Response
     */
    public function store(CreateParcoursFormationRequest $request)
    {
        $input = $request->all();

        /** @var ParcoursFormation $parcoursFormation */
        $parcoursFormation = ParcoursFormation::create($input);

        Flash::success('Parcours Formation saved successfully.');

        return redirect(route('parcoursFormations.index'));
    }

    /**
     * Display the specified ParcoursFormation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ParcoursFormation $parcoursFormation */
        $parcoursFormation = ParcoursFormation::find($id);

        if (empty($parcoursFormation)) {
            Flash::error('Parcours Formation not found');

            return redirect(route('parcoursFormations.index'));
        }

        return view('parcours_formations.show')->with('parcoursFormation', $parcoursFormation);
    }

    /**
     * Show the form for editing the specified ParcoursFormation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var ParcoursFormation $parcoursFormation */
        $parcoursFormation = ParcoursFormation::find($id);

        if (empty($parcoursFormation)) {
            Flash::error('Parcours Formation not found');

            return redirect(route('parcoursFormations.index'));
        }

        return view('parcours_formations.edit')->with('parcoursFormation', $parcoursFormation);
    }

    /**
     * Update the specified ParcoursFormation in storage.
     *
     * @param int $id
     * @param UpdateParcoursFormationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateParcoursFormationRequest $request)
    {
        /** @var ParcoursFormation $parcoursFormation */
        $parcoursFormation = ParcoursFormation::find($id);

        if (empty($parcoursFormation)) {
            Flash::error('Parcours Formation not found');

            return redirect(route('parcoursFormations.index'));
        }

        $parcoursFormation->fill($request->all());
        $parcoursFormation->save();

        Flash::success('Parcours Formation updated successfully.');

        return redirect(route('parcoursFormations.index'));
    }

    /**
     * Remove the specified ParcoursFormation from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ParcoursFormation $parcoursFormation */
        $parcoursFormation = ParcoursFormation::find($id);

        if (empty($parcoursFormation)) {
            Flash::error('Parcours Formation not found');

            return redirect(route('parcoursFormations.index'));
        }

        $parcoursFormation->delete();

        Flash::success('Parcours Formation deleted successfully.');

        return redirect(route('parcoursFormations.index'));
    }
}
