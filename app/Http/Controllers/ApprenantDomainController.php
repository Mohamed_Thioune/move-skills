<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateApprenantDomainRequest;
use App\Http\Requests\UpdateApprenantDomainRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\ApprenantDomain;
use Illuminate\Http\Request;
use Flash;
use Response;

class ApprenantDomainController extends AppBaseController
{
    /**
     * Display a listing of the ApprenantDomain.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var ApprenantDomain $apprenantDomains */
        $apprenantDomains = ApprenantDomain::all();

        return view('apprenant_domains.index')
            ->with('apprenantDomains', $apprenantDomains);
    }

    /**
     * Show the form for creating a new ApprenantDomain.
     *
     * @return Response
     */
    public function create()
    {
        return view('apprenant_domains.create');
    }

    /**
     * Store a newly created ApprenantDomain in storage.
     *
     * @param CreateApprenantDomainRequest $request
     *
     * @return Response
     */
    public function store(CreateApprenantDomainRequest $request)
    {
        $input = $request->all();

        /** @var ApprenantDomain $apprenantDomain */
        $apprenantDomain = ApprenantDomain::create($input);

        Flash::success('Apprenant Domain saved successfully.');

        return redirect(route('apprenantDomains.index'));
    }

    /**
     * Display the specified ApprenantDomain.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ApprenantDomain $apprenantDomain */
        $apprenantDomain = ApprenantDomain::find($id);

        if (empty($apprenantDomain)) {
            Flash::error('Apprenant Domain not found');

            return redirect(route('apprenantDomains.index'));
        }

        return view('apprenant_domains.show')->with('apprenantDomain', $apprenantDomain);
    }

    /**
     * Show the form for editing the specified ApprenantDomain.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var ApprenantDomain $apprenantDomain */
        $apprenantDomain = ApprenantDomain::find($id);

        if (empty($apprenantDomain)) {
            Flash::error('Apprenant Domain not found');

            return redirect(route('apprenantDomains.index'));
        }

        return view('apprenant_domains.edit')->with('apprenantDomain', $apprenantDomain);
    }

    /**
     * Update the specified ApprenantDomain in storage.
     *
     * @param int $id
     * @param UpdateApprenantDomainRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApprenantDomainRequest $request)
    {
        /** @var ApprenantDomain $apprenantDomain */
        $apprenantDomain = ApprenantDomain::find($id);

        if (empty($apprenantDomain)) {
            Flash::error('Apprenant Domain not found');

            return redirect(route('apprenantDomains.index'));
        }

        $apprenantDomain->fill($request->all());
        $apprenantDomain->save();

        Flash::success('Apprenant Domain updated successfully.');

        return redirect(route('apprenantDomains.index'));
    }

    /**
     * Remove the specified ApprenantDomain from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ApprenantDomain $apprenantDomain */
        $apprenantDomain = ApprenantDomain::find($id);

        if (empty($apprenantDomain)) {
            Flash::error('Apprenant Domain not found');

            return redirect(route('apprenantDomains.index'));
        }

        $apprenantDomain->delete();

        Flash::success('Apprenant Domain deleted successfully.');

        return redirect(route('apprenantDomains.index'));
    }
}
