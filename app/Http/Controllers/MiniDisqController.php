<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMiniDisqRequest;
use App\Http\Requests\UpdateMiniDisqRequest;
use App\Repositories\MiniDisqRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class MiniDisqController extends AppBaseController
{
    /** @var MiniDisqRepository $miniDisqRepository*/
    private $miniDisqRepository;

    public function __construct(MiniDisqRepository $miniDisqRepo)
    {
        $this->miniDisqRepository = $miniDisqRepo;
    }

    /**
     * Display a listing of the MiniDisq.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $miniDisqs = $this->miniDisqRepository->all();

        return view('mini_disqs.index')
            ->with('miniDisqs', $miniDisqs);
    }

    /**
     * Show the form for creating a new MiniDisq.
     *
     * @return Response
     */
    public function create()
    {
        return view('mini_disqs.create');
    }

    /**
     * Store a newly created MiniDisq in storage.
     *
     * @param CreateMiniDisqRequest $request
     *
     * @return Response
     */
    public function store(CreateMiniDisqRequest $request)
    {
        $input = $request->all();

        $miniDisq = $this->miniDisqRepository->create($input);

        Flash::success('Mini Disq saved successfully.');

        return redirect(route('miniDisqs.index'));
    }

    /**
     * Display the specified MiniDisq.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $miniDisq = $this->miniDisqRepository->find($id);

        if (empty($miniDisq)) {
            Flash::error('Mini Disq not found');

            return redirect(route('miniDisqs.index'));
        }

        return view('mini_disqs.show')->with('miniDisq', $miniDisq);
    }

    /**
     * Show the form for editing the specified MiniDisq.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $miniDisq = $this->miniDisqRepository->find($id);

        if (empty($miniDisq)) {
            Flash::error('Mini Disq not found');

            return redirect(route('miniDisqs.index'));
        }

        return view('mini_disqs.edit')->with('miniDisq', $miniDisq);
    }

    /**
     * Update the specified MiniDisq in storage.
     *
     * @param int $id
     * @param UpdateMiniDisqRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMiniDisqRequest $request)
    {
        $miniDisq = $this->miniDisqRepository->find($id);

        if (empty($miniDisq)) {
            Flash::error('Mini Disq not found');

            return redirect(route('miniDisqs.index'));
        }

        $miniDisq = $this->miniDisqRepository->update($request->all(), $id);

        Flash::success('Mini Disq updated successfully.');

        return redirect(route('miniDisqs.index'));
    }

    /**
     * Remove the specified MiniDisq from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $miniDisq = $this->miniDisqRepository->find($id);

        if (empty($miniDisq)) {
            Flash::error('Mini Disq not found');

            return redirect(route('miniDisqs.index'));
        }

        $this->miniDisqRepository->delete($id);

        Flash::success('Mini Disq deleted successfully.');

        return redirect(route('miniDisqs.index'));
    }
}
