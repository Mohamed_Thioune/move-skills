<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCanvaMiniDisqRequest;
use App\Http\Requests\UpdateCanvaMiniDisqRequest;
use App\Repositories\CanvaMiniDisqRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CanvaMiniDisqController extends AppBaseController
{
    /** @var CanvaMiniDisqRepository $canvaMiniDisqRepository*/
    private $canvaMiniDisqRepository;

    public function __construct(CanvaMiniDisqRepository $canvaMiniDisqRepo)
    {
        $this->canvaMiniDisqRepository = $canvaMiniDisqRepo;
    }

    /**
     * Display a listing of the CanvaMiniDisq.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $canvaMiniDisqs = $this->canvaMiniDisqRepository->all();

        return view('canva_mini_disqs.index')
            ->with('canvaMiniDisqs', $canvaMiniDisqs);
    }

    /**
     * Show the form for creating a new CanvaMiniDisq.
     *
     * @return Response
     */
    public function create()
    {
        return view('canva_mini_disqs.create');
    }

    /**
     * Store a newly created CanvaMiniDisq in storage.
     *
     * @param CreateCanvaMiniDisqRequest $request
     *
     * @return Response
     */
    public function store(CreateCanvaMiniDisqRequest $request)
    {
        $input = $request->all();

        $canvaMiniDisq = $this->canvaMiniDisqRepository->create($input);

        Flash::success('Canva Mini Disq saved successfully.');

        return redirect(route('canvaMiniDisqs.index'));
    }

    /**
     * Display the specified CanvaMiniDisq.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $canvaMiniDisq = $this->canvaMiniDisqRepository->find($id);

        if (empty($canvaMiniDisq)) {
            Flash::error('Canva Mini Disq not found');

            return redirect(route('canvaMiniDisqs.index'));
        }

        return view('canva_mini_disqs.show')->with('canvaMiniDisq', $canvaMiniDisq);
    }

    /**
     * Show the form for editing the specified CanvaMiniDisq.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $canvaMiniDisq = $this->canvaMiniDisqRepository->find($id);

        if (empty($canvaMiniDisq)) {
            Flash::error('Canva Mini Disq not found');

            return redirect(route('canvaMiniDisqs.index'));
        }

        return view('canva_mini_disqs.edit')->with('canvaMiniDisq', $canvaMiniDisq);
    }

    /**
     * Update the specified CanvaMiniDisq in storage.
     *
     * @param int $id
     * @param UpdateCanvaMiniDisqRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCanvaMiniDisqRequest $request)
    {
        $canvaMiniDisq = $this->canvaMiniDisqRepository->find($id);

        if (empty($canvaMiniDisq)) {
            Flash::error('Canva Mini Disq not found');

            return redirect(route('canvaMiniDisqs.index'));
        }

        $canvaMiniDisq = $this->canvaMiniDisqRepository->update($request->all(), $id);

        Flash::success('Canva Mini Disq updated successfully.');

        return redirect(route('canvaMiniDisqs.index'));
    }

    /**
     * Remove the specified CanvaMiniDisq from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $canvaMiniDisq = $this->canvaMiniDisqRepository->find($id);

        if (empty($canvaMiniDisq)) {
            Flash::error('Canva Mini Disq not found');

            return redirect(route('canvaMiniDisqs.index'));
        }

        $this->canvaMiniDisqRepository->delete($id);

        Flash::success('Canva Mini Disq deleted successfully.');

        return redirect(route('canvaMiniDisqs.index'));
    }
}
