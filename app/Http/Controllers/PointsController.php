<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePointsRequest;
use App\Http\Requests\UpdatePointsRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Points;
use Illuminate\Http\Request;
use Flash;
use Response;

class PointsController extends AppBaseController
{
    /**
     * Display a listing of the Points.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Points $points */
        $points = Points::all();

        return view('points.index')
            ->with('points', $points);
    }

    /**
     * Show the form for creating a new Points.
     *
     * @return Response
     */
    public function create()
    {
        return view('points.create');
    }

    /**
     * Store a newly created Points in storage.
     *
     * @param CreatePointsRequest $request
     *
     * @return Response
     */
    public function store(CreatePointsRequest $request)
    {
        $input = $request->all();

        /** @var Points $points */
        $points = Points::create($input);

        Flash::success('Points saved successfully.');

        return redirect(route('points.index'));
    }

    /**
     * Display the specified Points.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Points $points */
        $points = Points::find($id);

        if (empty($points)) {
            Flash::error('Points not found');

            return redirect(route('points.index'));
        }

        return view('points.show')->with('points', $points);
    }

    /**
     * Show the form for editing the specified Points.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Points $points */
        $points = Points::find($id);

        if (empty($points)) {
            Flash::error('Points not found');

            return redirect(route('points.index'));
        }

        return view('points.edit')->with('points', $points);
    }

    /**
     * Update the specified Points in storage.
     *
     * @param int $id
     * @param UpdatePointsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePointsRequest $request)
    {
        /** @var Points $points */
        $points = Points::find($id);

        if (empty($points)) {
            Flash::error('Points not found');

            return redirect(route('points.index'));
        }

        $points->fill($request->all());
        $points->save();

        Flash::success('Points updated successfully.');

        return redirect(route('points.index'));
    }

    /**
     * Remove the specified Points from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Points $points */
        $points = Points::find($id);

        if (empty($points)) {
            Flash::error('Points not found');

            return redirect(route('points.index'));
        }

        $points->delete();

        Flash::success('Points deleted successfully.');

        return redirect(route('points.index'));
    }
}
