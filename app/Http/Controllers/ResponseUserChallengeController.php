<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateResponseUserChallengeRequest;
use App\Http\Requests\UpdateResponseUserChallengeRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\ResponseUserChallenge;
use Illuminate\Http\Request;
use Flash;
use Response;

class ResponseUserChallengeController extends AppBaseController
{
    /**
     * Display a listing of the ResponseUserChallenge.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var ResponseUserChallenge $responseUserChallenges */
        $responseUserChallenges = ResponseUserChallenge::all();

        return view('response_user_challenges.index')
            ->with('responseUserChallenges', $responseUserChallenges);
    }

    /**
     * Show the form for creating a new ResponseUserChallenge.
     *
     * @return Response
     */
    public function create()
    {
        return view('response_user_challenges.create');
    }

    /**
     * Store a newly created ResponseUserChallenge in storage.
     *
     * @param CreateResponseUserChallengeRequest $request
     *
     * @return Response
     */
    public function store(CreateResponseUserChallengeRequest $request)
    {
        $input = $request->all();

        /** @var ResponseUserChallenge $responseUserChallenge */
        $responseUserChallenge = ResponseUserChallenge::create($input);

        Flash::success('Response User Challenge saved successfully.');

        return redirect(route('responseUserChallenges.index'));
    }

    /**
     * Display the specified ResponseUserChallenge.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ResponseUserChallenge $responseUserChallenge */
        $responseUserChallenge = ResponseUserChallenge::find($id);

        if (empty($responseUserChallenge)) {
            Flash::error('Response User Challenge not found');

            return redirect(route('responseUserChallenges.index'));
        }

        return view('response_user_challenges.show')->with('responseUserChallenge', $responseUserChallenge);
    }

    /**
     * Show the form for editing the specified ResponseUserChallenge.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var ResponseUserChallenge $responseUserChallenge */
        $responseUserChallenge = ResponseUserChallenge::find($id);

        if (empty($responseUserChallenge)) {
            Flash::error('Response User Challenge not found');

            return redirect(route('responseUserChallenges.index'));
        }

        return view('response_user_challenges.edit')->with('responseUserChallenge', $responseUserChallenge);
    }

    /**
     * Update the specified ResponseUserChallenge in storage.
     *
     * @param int $id
     * @param UpdateResponseUserChallengeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResponseUserChallengeRequest $request)
    {
        /** @var ResponseUserChallenge $responseUserChallenge */
        $responseUserChallenge = ResponseUserChallenge::find($id);

        if (empty($responseUserChallenge)) {
            Flash::error('Response User Challenge not found');

            return redirect(route('responseUserChallenges.index'));
        }

        $responseUserChallenge->fill($request->all());
        $responseUserChallenge->save();

        Flash::success('Response User Challenge updated successfully.');

        return redirect(route('responseUserChallenges.index'));
    }

    /**
     * Remove the specified ResponseUserChallenge from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ResponseUserChallenge $responseUserChallenge */
        $responseUserChallenge = ResponseUserChallenge::find($id);

        if (empty($responseUserChallenge)) {
            Flash::error('Response User Challenge not found');

            return redirect(route('responseUserChallenges.index'));
        }

        $responseUserChallenge->delete();

        Flash::success('Response User Challenge deleted successfully.');

        return redirect(route('responseUserChallenges.index'));
    }
}
