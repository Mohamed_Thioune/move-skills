<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Session;
use Mail; 
use Illuminate\Contracts\Encryption\DecryptException;
use Crypt;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Parcours_formation;
use Flash;

class ParcourFormationController extends Controller
{
    //

    public function mes_parcours()
    {
        $formateur = DB::table('formateurs')->where('user_id', Auth::user()->id)->first();

        $parcours = DB::table('parcours_formations')->where('formateur_id', $formateur->id)->orderBy('id', 'desc')->get();
        return view('parcourFormation.mes_parcours', compact('parcours'));
    }

    public function create_parcours()
    {
        return view('parcourFormation.creation_parcours_formation');
    }

    public function create_parcours_activite()
    {
        return view('creation-parcours-formation-activite');
    }

    public function store_parcours(Request $request)
    {
        
        $message = "Ajouté avec succès";

        $formateur = DB::table('formateurs')->where('user_id', Auth::user()->id)->first();

        $parcour = new Parcours_formation;
        $parcour->libelle =$request->get('libelle');
        $parcour->description = $request->get('description'); 
        $parcour->formateur_id = $formateur->id; 
        $parcour->parcours_transformation_id = $request->get('parcours_transformation_id'); 
        $parcour->prix = $request->get('prix'); 
        $parcour->save();

        return back()->with(['message' => $message]);
 
    }

    public function destroy_parcour($id)
    {
        $parcour = Parcours_formation::find($id);
        $parcour->delete();
        return back();
    }

}
