<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MoveSkillDisque extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $mini_disque;
    public $competences;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $mini_disque, $competences)
    {
        $this->user = $user;
        $this->mini_disque = $mini_disque;
        $this->competences = $competences;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = $this->user->prenom . " , Voici les résultats de ton Team Member Profile (Mini Disc) !";
        return $this->subject($title)
                ->view('mail.move_skills_disque', ['user' => $this->user, 'mini_disque' => $this->mini_disque, 'competences' => $this->competences]);
    }
}
