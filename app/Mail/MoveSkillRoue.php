<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MoveSkillRoue extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $wheel;
    public $competences;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $wheel, $competences)
    {
        $this->user = $user;
        $this->wheel = $wheel;
        $this->competences = $competences;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = $this->user->prenom . " , Voici les résultats de ta roue de la vie  !";
        return $this->subject($title)
                ->view('mail.move_skills_roue', ['user' => $this->user, 'wheel' => $this->wheel, 'competences' => $this->competences]);
    }
}
