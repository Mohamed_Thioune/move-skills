<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ParcourActivite
 * @package App\Models
 * @version March 19, 2023, 5:35 pm UTC
 *
 * @property string $type
 * @property unsignedInteger $user_id
 * @property unsignedInteger $parcours_id
 * @property unsignedInteger $activity�_id
 */
class ParcourActivite extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'parcour_activites';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'type',
        'user_id',
        'parcours_id',
        'activity_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type' => 'required',
        'user_id' => 'required',
        'parcours_id' => 'required',
        'activity_id' => 'required'
    ];

    
}
