<div class="table-responsive">
    <table class="table" id="responseByUserDiscs-table">
        <thead>
        <tr>
            <th>Row</th>
        <th>Column</th>
        <th>Point</th>
        <th>User Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($responseByUserDiscs as $responseByUserDisc)
            <tr>
                <td>{{ $responseByUserDisc->row }}</td>
            <td>{{ $responseByUserDisc->column }}</td>
            <td>{{ $responseByUserDisc->point }}</td>
            <td>{{ $responseByUserDisc->user_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['responseByUserDiscs.destroy', $responseByUserDisc->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('responseByUserDiscs.show', [$responseByUserDisc->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('responseByUserDiscs.edit', [$responseByUserDisc->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
