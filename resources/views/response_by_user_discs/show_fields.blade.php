<!-- Row Field -->
<div class="col-sm-12">
    {!! Form::label('row', 'Row:') !!}
    <p>{{ $responseByUserDisc->row }}</p>
</div>

<!-- Column Field -->
<div class="col-sm-12">
    {!! Form::label('column', 'Column:') !!}
    <p>{{ $responseByUserDisc->column }}</p>
</div>

<!-- Point Field -->
<div class="col-sm-12">
    {!! Form::label('point', 'Point:') !!}
    <p>{{ $responseByUserDisc->point }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $responseByUserDisc->user_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $responseByUserDisc->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $responseByUserDisc->updated_at }}</p>
</div>

