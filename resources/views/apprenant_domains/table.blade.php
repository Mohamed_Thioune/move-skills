<div class="table-responsive">
    <table class="table" id="apprenantDomains-table">
        <thead>
        <tr>
            <th>Apprenant Id</th>
        <th>Domain Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($apprenantDomains as $apprenantDomain)
            <tr>
                <td>{{ $apprenantDomain->apprenant_id }}</td>
            <td>{{ $apprenantDomain->domain_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['apprenantDomains.destroy', $apprenantDomain->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('apprenantDomains.show', [$apprenantDomain->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('apprenantDomains.edit', [$apprenantDomain->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
