<!-- Apprenant Id Field -->
<div class="col-sm-12">
    {!! Form::label('apprenant_id', 'Apprenant Id:') !!}
    <p>{{ $apprenantDomain->apprenant_id }}</p>
</div>

<!-- Domain Id Field -->
<div class="col-sm-12">
    {!! Form::label('domain_id', 'Domain Id:') !!}
    <p>{{ $apprenantDomain->domain_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $apprenantDomain->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $apprenantDomain->updated_at }}</p>
</div>

