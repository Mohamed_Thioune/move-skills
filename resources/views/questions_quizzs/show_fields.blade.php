<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{{ $questionsQuizz->libelle }}</p>
</div>

<!-- Quizz Id Field -->
<div class="col-sm-12">
    {!! Form::label('quizz_id', 'Quizz Id:') !!}
    <p>{{ $questionsQuizz->quizz_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $questionsQuizz->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $questionsQuizz->updated_at }}</p>
</div>

