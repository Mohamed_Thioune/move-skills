<div class="table-responsive">
    <table class="table" id="questionsQuizzs-table">
        <thead>
        <tr>
            <th>Libelle</th>
        <th>Quizz Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($questionsQuizzs as $questionsQuizz)
            <tr>
                <td>{{ $questionsQuizz->libelle }}</td>
            <td>{{ $questionsQuizz->quizz_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['questionsQuizzs.destroy', $questionsQuizz->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('questionsQuizzs.show', [$questionsQuizz->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('questionsQuizzs.edit', [$questionsQuizz->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
