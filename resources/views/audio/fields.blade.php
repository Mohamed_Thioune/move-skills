<!-- Libelle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle', 'Libelle:') !!}
    {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Podcast Field -->
<div class="form-group col-sm-6">
    {!! Form::label('podcast', 'Podcast:') !!}
    {!! Form::text('podcast', null, ['class' => 'form-control']) !!}
</div>