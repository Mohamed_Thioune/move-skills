<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{{ $audio->libelle }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $audio->description }}</p>
</div>

<!-- Podcast Field -->
<div class="col-sm-12">
    {!! Form::label('podcast', 'Podcast:') !!}
    <p>{{ $audio->podcast }}</p>
</div>

<!-- Parcours Formation Id Field -->
<div class="col-sm-12">
    {!! Form::label('parcours_formation_id', 'Parcours Formation Id:') !!}
    <p>{{ $audio->parcours_formation_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $audio->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $audio->updated_at }}</p>
</div>

