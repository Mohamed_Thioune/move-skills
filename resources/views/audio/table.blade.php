<div class="table-responsive">
    <table class="table" id="audio-table">
        <thead>
        <tr>
            <th>Libelle</th>
        <th>Description</th>
        <th>Podcast</th>
        <th>Parcours Formation Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($audio as $audio)
            <tr>
                <td>{{ $audio->libelle }}</td>
            <td>{{ $audio->description }}</td>
            <td>{{ $audio->podcast }}</td>
            <td>{{ $audio->parcours_formation_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['audio.destroy', $audio->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('audio.show', [$audio->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('audio.edit', [$audio->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
