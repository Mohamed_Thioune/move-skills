<div class="table-responsive">
    <table class="table" id="questions-table">
        <thead>
        <tr>
            <th>Libelle</th>
        <th>Type</th>
        <th>Quizz Id</th>
        <th>Roue De Vie Id</th>
        <th>Canva Mini Disq Id</th>
        <th>Domaine Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($questions as $question)
            <tr>
                <td>{{ $question->libelle }}</td>
            <td>{{ $question->type }}</td>
            <td>{{ $question->quizz_id }}</td>
            <td>{{ $question->roue_de_vie_id }}</td>
            <td>{{ $question->canva_mini_disq_id }}</td>
            <td>{{ $question->domaine_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['questions.destroy', $question->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('questions.show', [$question->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('questions.edit', [$question->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
