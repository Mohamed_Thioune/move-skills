<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{{ $question->libelle }}</p>
</div>

<!-- Type Field -->
<div class="col-sm-12">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $question->type }}</p>
</div>

<!-- Quizz Id Field -->
<div class="col-sm-12">
    {!! Form::label('quizz_id', 'Quizz Id:') !!}
    <p>{{ $question->quizz_id }}</p>
</div>

<!-- Roue De Vie Id Field -->
<div class="col-sm-12">
    {!! Form::label('roue_de_vie_id', 'Roue De Vie Id:') !!}
    <p>{{ $question->roue_de_vie_id }}</p>
</div>

<!-- Canva Mini Disq Id Field -->
<div class="col-sm-12">
    {!! Form::label('canva_mini_disq_id', 'Canva Mini Disq Id:') !!}
    <p>{{ $question->canva_mini_disq_id }}</p>
</div>

<!-- Domaine Id Field -->
<div class="col-sm-12">
    {!! Form::label('domaine_id', 'Domaine Id:') !!}
    <p>{{ $question->domaine_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $question->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $question->updated_at }}</p>
</div>

