@extends('layouts.default')
@section('content')
<div class="content-add-parcours">
    <h1>Créer un parcours de formation</h1>
    <div class="content-form">
        <form action="">
            <div class="form-one">
                <h2>Etape 1  : Configuration de base du parcours  </h2>
                <div class="form-group">
                    <label for="name">Nom du parcours de formation</label>
                    <input type="text" class="form-control" id="name" aria-describedby="name" placeholder="La confiance en soi une philosophie">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect2">Example multiple select</label>
                    <select multiple class="form-control" id="exampleFormControlSelect2">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Example textarea</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>


    </div>
</div>


@stop
