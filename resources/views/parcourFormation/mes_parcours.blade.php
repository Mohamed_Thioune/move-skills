@extends('layouts.default')
@section('content')
<div class="content-mes-parcours">
    <h1>Mes parcours</h1>
   <div class="text-right">
       <a href="/creation-parcours-formation">
        <button class="btn btn-parcours-formation">
           <img src="/img/btnPlus.png" alt="">
           Créer un parcours de formation
       </button>
        </a>
   </div>
    <div class="card-element">
        <table id="table"

               data-search="true"
               data-filter-control="true"
               data-pagination="true"
               data-toolbar="#toolbar"
               class="table-parcours">
            <thead>
            <tr>
                <th data-field="Parcours-de-formation" data-filter-control="input" data-sortable="true" class="title-formation">Parcours de formation</th>
                <th data-field="Apprenants" data-filter-control="select" data-sortable="true" class="title-apprenants">descriptions</th>
                <th data-field="Apprenants" data-filter-control="select" data-sortable="true" class="title-apprenants">Apprenants</th>
                <th data-field="Mes-commissions" data-filter-control="select" data-sortable="true" class="title-commission">Prix</th>
                <th data-field="modifier-parcours" data-sortable="false"></th>
            </tr>
            </thead>
            <tbody>
                @foreach($parcours as $parcour)
            <tr>
                <td class="confiance-block-td"> {{($parcour->libelle) ? $parcour->libelle : '--'}}</td>
                <td class="text-center">{{($parcour->description) ? $parcour->description : '--'}}</td>
                <td class="somme-center">En attente</td>
                <td class="somme-table">{{($parcour->prix) ? $parcour->prix : '--'}}</td>
                <td class="text-center">
                <form action="{{ route('delete_parcour',$parcour->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                    <button type="submit" class="btn btn-delete">Delete</button>
                </form>
                </td>
            </tr>
            @endforeach
            <!-- <tr>
                <td class="management-block-td">Le management 3.0 et le bonheur au travail</td>
                <td class="text-center">50</td>
                <td class="somme-table">150 000 FCFA</td>
                <td class="text-center">
                    <button class="btn btn-delete">Delete</button>
                </td>
            </tr> -->
            </tbody>
        </table>
    </div>
</div>


@stop
