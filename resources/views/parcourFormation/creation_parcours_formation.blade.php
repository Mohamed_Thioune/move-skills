@extends('layouts.default')
@section('content')
<div class="content-add-parcours">
    <h1>Créer un parcours de formation</h1>
    <div class="content-form">
    <h6> 
              @if (session('message'))
                  <div class="alert alert-success" style="background-color:lightgreen" role="alert">
                      {{ session('message') }}
                  </div>  
              @endif
            </h6>
    <form action="{{route('store_parcours_formation')}}" method="post" class="login-form">
                        @csrf
            <div class="form-one">
                <h2>Etape 1  : Configuration de base du parcours  </h2>
                <div class="form-group">
                    <label for="name">Nom du parcours de formation</label>
                    <input type="text" class="form-control" name="libelle" id="name" aria-describedby="name" placeholder="">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Description du parcours de formation</label>
                    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <label for="name">Prix nom du parcours de formation</label>
                    <input type="number" class="form-control" name="prix" id="name" aria-describedby="name" placeholder="">
                </div>
                <!-- <div class="form-group">
                    <label for="exampleFormControlSelect2">Example multiple select</label>
                    <select multiple class="form-control" id="exampleFormControlSelect2">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div> -->
                

                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>


    </div>
</div>


@stop
