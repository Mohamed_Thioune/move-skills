<div class="table-responsive">
    <table class="table" id="miniDisqs-table">
        <thead>
        <tr>
            <th>Libelle</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($miniDisqs as $miniDisq)
            <tr>
                <td>{{ $miniDisq->libelle }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['miniDisqs.destroy', $miniDisq->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('miniDisqs.show', [$miniDisq->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('miniDisqs.edit', [$miniDisq->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
