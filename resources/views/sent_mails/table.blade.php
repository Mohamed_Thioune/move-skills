<div class="table-responsive">
    <table class="table" id="sentMails-table">
        <thead>
        <tr>
            <th>Type</th>
        <th>User Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($sentMails as $sentMail)
            <tr>
                <td>{{ $sentMail->type }}</td>
            <td>{{ $sentMail->user_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['sentMails.destroy', $sentMail->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('sentMails.show', [$sentMail->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('sentMails.edit', [$sentMail->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
