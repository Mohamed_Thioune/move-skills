<!doctype html>
<html lang="en">
<head>
    <title>Login 08</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">

</head>
<body>
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 text-center mb-4">
                <h2 class="heading-section">Inscrivez-vous pour commencer !</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-5">
                <div class="login-wrap">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <img src="/img/Logo.png" alt="">
                    </div>
                    <h3 class="text-center mb-4">Débuter</h3>
                    <h6> 
                        @if (session('message'))
                            <div class="alert alert-success" style="background-color:lightgreen" role="alert">
                                {{ session('message') }}
                            </div>  
                        @endif
                        </h6>
                    <form action="{{route('inscription_store')}}" method="post" class="login-form">
                        @csrf
                       
                         <div class="form-group">
                            <input type="text" class="form-control rounded-left @error('prenom') is-invalid @enderror" name="prenom" value="{{ old('prenom') }}" required autocomplete="prenom" autofocus placeholder="prenom d'utilisateur">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control rounded-left @error('nom') is-invalid @enderror" name="nom" value="{{ old('nom') }}" required autocomplete="nom" autofocus placeholder="Nom d'utilisateur">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control rounded-left @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="emil" autofocus placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control rounded-left @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="password" autofocus placeholder="Mot de passe">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control rounded-left @error('password_confirmation') is-invalid @enderror" name="password_confirmation" value="{{ old('password_confirmation') }}" required autocomplete="new-password" autofocus placeholder="Répéter le mot de pass">
                        </div>
                      
                        <div class="form-group d-md-flex">
                            <div class=" text-md-right">
                                <a href="{{ ('connexion') }}" >Vous avez déja un compte ?</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary rounded submit p-3 px-5">S'inscrire</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="/js/jquery.min.js"></script>
<script src="/js/popper.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script>

</body>
</html>

