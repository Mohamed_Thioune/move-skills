<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <title>{{$user->prenom}}, viens découvrir les 12 outils clés pour réussir ton année 2023 !</title>
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <style type="text/css">
        #outlook a {
            padding: 0;
        }
        
        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        
        table,
        td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
        
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }
        
        p {
            display: block;
            margin: 13px 0;
        }
        
        .card-roue-de-la-vie {
            background: #9BA1FF;
            margin: 0 15px;
            padding: 20px 20px 23px;
            width: 100%;
            border-radius: 10px;
            text-align: center;
            margin-bottom: 30px;
        }
        
        .card-roue-de-la-vie h3 {
            margin-bottom: 35px;
            font-weight: 600;
            color: white;
        }
     		/******************************************** LEFT CONTAINER *****************************************/
		.left-container {}
			.menu-box {
				height: 360px;
			}

			.donut-chart-block {
				overflow: hidden;
			}
				.donut-chart-block .titular {
					padding: 10px 0;
				}
				.os-percentages li {
					width: 75px;
					border-left: 1px solid #394264;
					text-align: center;					
					background: #50597b;
				}
					.os {
						margin: 0;
						padding: 10px 0 5px;
						font-size: 15px;		
					}
						.os.ios {
							border-top: 4px solid #e64c65;
						}
						.os.mac {
							border-top: 4px solid #11a8ab;
						}
						.os.linux {
							border-top: 4px solid #fcb150;
						}
						.os.win {
							border-top: 4px solid #4fc4f6;
						}
					.os-percentage {
						margin: 0;
						padding: 0 0 15px 10px;
						font-size: 25px;
					}
			.line-chart-block, .bar-chart-block {
				height: 400px;
			}
				.line-chart {
					height: 200px;
					background: #11a8ab;
				}
				.time-lenght {
					padding-top: 22px;
					padding-left: 38px;
          overflow: hidden;
				}
					.time-lenght-btn {
						display: block;
						width: 70px;
						line-height: 32px;
						background: #50597b;
						border-radius: 5px;
						font-size: 14px;
						text-align: center;
						margin-right: 5px;
						-webkit-transition: background .3s;
						transition: background .3s;
					}
						.time-lenght-btn:hover {
							text-decoration: none;
							background: #e64c65;
						}
				.month-data {
					padding-top: 28px;
				}
					.month-data p {
						display: inline-block;
						margin: 0;
						padding: 0 25px 15px;            
						font-size: 16px;
					}
						.month-data p:last-child {
							padding: 0 25px;
              float: right;
							font-size: 15px;
						}
						.increment {
							color: #e64c65;
						}

/******************************************
↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
ESTILOS PROPIOS DE LOS GRÄFICOS
↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ 
GRAFICO LINEAL
******************************************/

.grafico {
  padding: 2rem 1rem 1rem;
  width: 100%;
  height: 100%;
  position: relative;
  color: #fff;
  font-size: 80%;
}
.grafico span {
  display: block;
  position: absolute;
  bottom: 3rem;
  left: 2rem;
  height: 0;
  border-top: 2px solid;
  transform-origin: left center;
}
.grafico span > span {
  left: 100%; bottom: 0;
}
[data-valor='25'] {width: 75px; transform: rotate(-45deg);}
[data-valor='8'] {width: 24px; transform: rotate(65deg);}
[data-valor='13'] {width: 39px; transform: rotate(-45deg);}
[data-valor='5'] {width: 15px; transform: rotate(50deg);}
[data-valor='23'] {width: 69px; transform: rotate(-70deg);}
[data-valor='12'] {width: 36px; transform: rotate(75deg);}
[data-valor='15'] {width: 45px; transform: rotate(-45deg);}

[data-valor]:before {
  content: '';
  position: absolute;
  display: block;
  right: -4px;
  bottom: -3px;
  padding: 4px;
  background: #fff;
  border-radius: 50%;
}
[data-valor='23']:after {
  content: '+' attr(data-valor) '%';
  position: absolute;
  right: -2.7rem;
  top: -1.7rem;
  padding: .3rem .5rem;
  background: #50597B;
  border-radius: .5rem;
  transform: rotate(45deg);  
}
[class^='eje-'] {
  position: absolute;
  left: 0;
  bottom: 0rem;
  width: 100%;
  padding: 1rem 1rem 0 2rem;
  height: 80%;
}
.eje-x {
  height: 2.5rem;
}
.eje-y li {
  height: 25%;
  border-top: 1px solid #777;
}
[data-ejeY]:before {
  content: attr(data-ejeY);
  display: inline-block;
  width: 2rem;
  text-align: right;
  line-height: 0;
  position: relative;
  left: -2.5rem;
  top: -.5rem;
} 
.eje-x li {
  width: 33%;
  float: left;
  text-align: center;
}

/******************************************
GRAFICO CIRCULAR PIE CHART
******************************************/
.donut-chart {
  position: relative;
	width: 200px;
  height: 200px;
	margin: 0 auto 2rem;
	border-radius: 100%
 }
p.center-date {
  background: #394264;
  position: absolute;
  text-align: center;
	font-size: 28px;
  top:0;left:0;bottom:0;right:0;
  width: 130px;
  height: 130px;
  margin: auto;
  border-radius: 50%;
  line-height: 35px;
  padding: 15% 0 0;
}
.center-date span.scnd-font-color {
 line-height: 0; 
}
.recorte {
    border-radius: 50%;
    clip: rect(0px, 200px, 200px, 100px);
    height: 100%;
    position: absolute;
    width: 100%;
  }
.quesito {
    border-radius: 50%;
    clip: rect(0px, 100px, 200px, 0px);
    height: 100%;
    position: absolute;
    width: 100%;
    font-family: monospace;
    font-size: 1.5rem;
  }
#porcion1 {
    transform: rotate(0deg);
  }

#porcion1 .quesito {
    background-color: #E64C65;
    transform: rotate(76deg);
  }
#porcion2 {
    transform: rotate(76deg);
  }
#porcion2 .quesito {
    background-color: #11A8AB;
    transform: rotate(140deg);
  }
#porcion3 {
    transform: rotate(215deg);
  }
#porcion3 .quesito {
    background-color: #4FC4F6;
    transform: rotate(113deg);
  }
#porcion3 .quesito {
background-color: #4FC4F6;
transform: rotate(113deg);
}
#porcion4 .quesito {
background-color: #f64fc4;
transform: rotate(113deg);
}
#porcion5 .quesito {
background-color: #554ff6;
transform: rotate(113deg);
}

#porcion6 .quesito {
background-color: #caf64f;
transform: rotate(113deg);
}
#porcion7 .quesito {
background-color: #794ff6;
transform: rotate(113deg);
}
#porcionFin {
    transform:rotate(-32deg);
  }
#porcionFin .quesito {
    background-color: #FCB150;
    transform: rotate(32deg);
  }
.nota-final {
  clear: both;
  color: #4FC4F6;
  font-size: 1rem;
  padding: 2rem 0;
}
.nota-final strong {
  color: #E64C65;
}
.nota-final a {
  color: #FCB150;
  font-size: inherit;
}
    </style>
    <!--[if mso]>
        <noscript>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        </noscript>
        <![endif]-->
    <!--[if lte mso 11]>
        <style type="text/css">
          .mj-outlook-group-fix { width:100% !important; }
        </style>
        <![endif]-->
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
        @import url(https://fonts.googleapis.com/css?family=Montserrat);
    </style>
    <!--<![endif]-->
    <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-100 {
                width: 100% !important;
                max-width: 100%;
            }
        }
    </style>
    <style media="screen and (min-width:480px)">
        .moz-text-html .mj-column-per-100 {
            width: 100% !important;
            max-width: 100%;
        }
    </style>
    <style type="text/css">
        [owa] .mj-column-per-100 {
            width: 100% !important;
            max-width: 100%;
        }
    </style>
    <style type="text/css">
        @media only screen and (max-width:480px) {
            table.mj-full-width-mobile {
                width: 100% !important;
            }
            td.mj-full-width-mobile {
                width: auto !important;
            }
        }
    </style>
</head>

<body style="word-spacing:normal;background-color:#ffffff;">
    <div style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">Invitation à la formation</div>
    <div style="background-color:#ffffff;">
        <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div style="margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                <tbody>
                    <tr>
                        <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                            <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
                            <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width:550px;"><img alt="" height="auto" src="https://xtxky.mjt.lu/tplimg/xtxky/b/luj30/i7xu7.png" style="border:none;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;"
                                                                    width="550"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
                <tbody>
                    <tr>
                        <td style="direction:ltr;font-size:0px;padding:20px 0px 20px 0px;padding-left:0px;padding-right:0px;text-align:center;">
                            <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
                            <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="left" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;">
                                                <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:16px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                                                    <p class="text-build-content" data-testid="8Ag8hxBro" style="margin: 10px 0; margin-top: 10px;"><span style="background-color:#ffffff;font-family:Montserrat, Arial, Helvetica, sans-serif;font-size:16px;">Hello <b> {{$user->prenom}} !</b></span></p>
                                                    <p class="text-build-content" data-testid="8Ag8hxBro" style="margin: 10px 0;"><span style="background-color:#ffffff;font-family:Montserrat, Arial, Helvetica, sans-serif;font-size:16px;"><b>Tu as passé les </b></span><span style="background-color:#ffffff;color:#109b39;font-family:Montserrat, Arial, Helvetica, sans-serif;font-size:16px;"><b>3 tests de personnalité avec Brio !</b></span></p>
                                                    <p class="text-build-content" style="line-height: 23px; margin: 10px 0; margin-bottom: 10px;" data-testid="8Ag8hxBro"><span style="background-color:#ffffff;color:#000000;font-family:Montserrat, Arial, Helvetica, sans-serif;font-size:16px;"><b>Nous te partageons les résultats de ton profiling MoveSkills !</b></span></p>

                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div style="margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                <tbody>
                    <tr>
                        <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                            <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
                            <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="left" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;">
                                                <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:20px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                                                    <p class="text-build-content" style="text-align: center; margin: 10px 0; margin-top: 10px; margin-bottom: 20px;" data-testid="Ae6DdhzxS"><span style="color:#109b39;font-family:Montserrat, Arial, Helvetica, sans-serif;font-size:20px;"><b>Voici ta roue actuelle !</b></span></p>
                                                    <!-- https://codepen.io/jlalovi/details/bIyAr -->
                                                        <div class="container">
                                                           
                                                        <!-- PORCIONES GRAFICO CIRCULAR
                                                                ELIMINADO #donut-chart
                                                                MODIFICADO CSS d .donut-chart -->
                                                            <div id="porcion1" class="recorte"><div class="@php echo ((isset($wheel[0]->libelle)) ? $wheel[0]->libelle : ""); @endphp" data-rel="@php echo  ((isset($wheel[0]->note)) ? $wheel[0]->note : ""); @endphp"></div></div>
                                                            <div id="porcion2" class="recorte"><div class="@php echo ((isset($wheel[1]->libelle)) ? $wheel[1]->libelle : ""); @endphp" data-rel="@php echo  ((isset($wheel[1]->note)) ? $wheel[1]->note : ""); @endphp"></div></div>
                                                            <div id="porcion3" class="recorte"><div class="@php echo ((isset($wheel[2]->libelle)) ? $wheel[2]->libelle : ""); @endphp" data-rel="@php echo  ((isset($wheel[2]->note)) ? $wheel[2]->note : ""); @endphp"></div></div>
                                                            <div id="porcion4" class="recorte"><div class="@php echo ((isset($wheel[3]->libelle)) ? $wheel[3]->libelle : ""); @endphp" data-rel="@php echo  ((isset($wheel[3]->note)) ? $wheel[3]->note : ""); @endphp"></div></div>
                                                            <div id="porcion5" class="recorte"><div class="@php echo ((isset($wheel[4]->libelle)) ? $wheel[4]->libelle : ""); @endphp" data-rel="@php echo  ((isset($wheel[4]->note)) ? $wheel[4]->note : ""); @endphp"></div></div>
                                                            <div id="porcion6" class="recorte"><div class="@php echo ((isset($wheel[5]->libelle)) ? $wheel[5]->libelle : ""); @endphp" data-rel="@php echo  ((isset($wheel[5]->note)) ? $wheel[5]->note : ""); @endphp"></div></div>
                                                            <div id="porcion7" class="recorte"><div class="@php echo ((isset($wheel[6]->libelle)) ? $wheel[6]->libelle : ""); @endphp" data-rel="@php echo  ((isset($wheel[6]->note)) ? $wheel[6]->note : ""); @endphp"></div></div>
                                                            <div id="porcionFin" class="recorte"><div class="@php echo ((isset($wheel[7]->libelle)) ? $wheel[7]->libelle : ""); @endphp" data-rel="@php echo ((isset($wheel[7]->note)) ? $wheel[7]->note : ""); @endphp"></div></div>
                                                        <!-- FIN AÑADIDO GRÄFICO -->
                                                            </div>
                                                              
                                                                    <p class="ios os scnd-font-color">@php echo ((isset($wheel[0]->libelle)) ? $wheel[0]->libelle : ""); @endphp</p>
                                                                    <p class="os-percentage">@php echo ((isset($wheel[0]->note)) ? $wheel[0]->note : ""); @endphp<sup> / 10</sup></p>
                                                                
                                                                    <p class="mac os scnd-font-color">@php echo ((isset($wheel[1]->libelle)) ? $wheel[1]->libelle : "");  @endphp</p>
                                                                    <p class="os-percentage">@php echo ((isset($wheel[1]->note)) ? $wheel[1]->note : "");  @endphp<sup> / 10</sup></p>
                            
                                                                    <p class="linux os scnd-font-color">@php echo ((isset($wheel[2]->libelle)) ? $wheel[2]->libelle : "");  @endphp</p>
                                                                    <p class="os-percentage">@php echo ((isset($wheel[2]->note)) ? $wheel[2]->note : ""); @endphp<sup> / 10</sup></p>
                                                                    
                                                                    <p class="win os scnd-font-color">@php echo ((isset($wheel[3]->libelle)) ? $wheel[3]->libelle : "");  @endphp</p>
                                                                    <p class="os-percentage">@php echo ((isset($wheel[3]->note)) ? $wheel[3]->note : ""); @endphp<sup> / 10</sup></p>
                                
                                                                    <p class="linux os scnd-font-color">@php echo ((isset($wheel[4]->libelle)) ? $wheel[4]->libelle : "");  @endphp</p>
                                                                    <p class="os-percentage">@php echo ((isset($wheel[4]->note)) ? $wheel[4]->note : ""); @endphp<sup> / 10</sup></p>
                                                                   
                                                                    <p class="win os scnd-font-color">@php echo ((isset($wheel[5]->libelle)) ? $wheel[5]->libelle : "");  @endphp</p>
                                                                    <p class="os-percentage">@php echo ((isset($wheel[5]->note)) ? $wheel[5]->note : ""); @endphp<sup> / 10</sup></p>
                                                                  
                                                                    <p class="linux os scnd-font-color">@php echo ((isset($wheel[6]->libelle)) ? $wheel[6]->libelle : "");  @endphp</p>
                                                                    <p class="os-percentage">@php echo ((isset($wheel[6]->note)) ? $wheel[6]->note : ""); @endphp<sup> / 10</sup></p>
                                                                  
                                                                    <p class="win os scnd-font-color">@php echo ((isset($wheel[7]->libelle)) ? $wheel[7]->libelle : "");  @endphp</p>
                                                                    <p class="os-percentage">@php echo ((isset($wheel[7]->note)) ? $wheel[7]->note : ""); @endphp<sup> / 10</sup></p>
                                                                
                                                                
                                                            </div> 
                                                        </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <div class="roue-de-la-roue">
                                                    <div class="card-roue-de-la-vie">
                                                        <h3 class="photo-profil-text">Roue de la vie </h3>
                                                        <div id="chartdiv"></div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="background:transparent;font-size:0px;padding:10px 25px 10px 25px;padding-right:25px;padding-left:25px;word-break:break-word;">
                                                <p style="border-top:solid 2px #109b39;font-size:1px;margin:0px auto;width:100%;"></p>
                                                <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 2px #109b39;font-size:1px;margin:0px auto;width:550px;" role="presentation" width="550px" ><tr><td style="height:0;line-height:0;"> &nbsp;
</td></tr></table><![endif]-->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div style="margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                <tbody>
                    <tr>
                        <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                            <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
                            <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="left" style="background:#f9f9f9;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-right:25px;padding-bottom:0px;padding-left:25px;word-break:break-word;">
                                                <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:20px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                                                    <p class="text-build-content" style="text-align: center; margin: 10px 0; margin-top: 10px; margin-bottom: 10px;" data-testid="LPMnvozUM"><span style="color:#109b39;font-family:Montserrat, Arial, Helvetica, sans-serif;font-size:20px;"><b>Voici ta couleur dominante&nbsp;</b></span></p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="background:#f9f9f9;font-size:0px;padding:10px 25px 10px 25px;padding-right:25px;padding-left:25px;word-break:break-word;">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width:300px;"><img alt="" height="auto" src="{{$mini_disque['couleur_image']}} " style="border:none;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;"
                                                                    width="300"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div style="margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                <tbody>
                    <tr>
                        <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                            <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
                            <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="left" style="background:#f9f9f9;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-right:25px;padding-bottom:0px;padding-left:25px;word-break:break-word;">
                                                <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:20px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                                                    <p class="text-build-content" style="text-align: center; margin: 10px 0; margin-top: 10px; margin-bottom: 10px;" data-testid="UcyLLChBp"><span style="color:#109b39;font-family:Montserrat, Arial, Helvetica, sans-serif;font-size:20px;"><b>Voici ton profil animalier dominant</b></span></p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="background:#f9f9f9;font-size:0px;padding:10px 25px 10px 25px;padding-right:25px;padding-left:25px;word-break:break-word;">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width:300px;"><img alt="" height="auto" src="{{$mini_disque['mascotte_image']}}" style="border:none;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;"
                                                                    width="300"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;">
                                                <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:18px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                                                    <p class="text-build-content" data-testid="qzvvmCYDr" style="margin: 10px 0; margin-top: 10px; margin-bottom: 10px;"><span style="font-family:Montserrat, Arial, Helvetica, sans-serif;font-size:18px;"><b>Ce qui te caractérise :&nbsp;</b></span></p>
                                                    <ul>
                                                        @php
                                                        foreach($mini_disque['caracteristiques'] as $caracteristique)
                                                            echo '<li><span style="font-family:Montserrat, Arial, Helvetica, sans-serif;font-size:18px;">' . $caracteristique->libelle . '</span></li>';
                                                        @endphp
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="background:transparent;font-size:0px;padding:10px 25px 10px 25px;padding-right:25px;padding-left:25px;word-break:break-word;">
                                                <p style="border-top:solid 2px #109b39;font-size:1px;margin:0px auto;width:100%;"></p>
                                                <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 2px #109b39;font-size:1px;margin:0px auto;width:550px;" role="presentation" width="550px" ><tr><td style="height:0;line-height:0;"> &nbsp;
</td></tr></table><![endif]-->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div style="margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                <tbody>
                    <tr>
                        <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                            <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
                            <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="left" style="background:#f9f9f9;font-size:0px;padding:0px 25px 0px 25px;padding-top:0px;padding-right:25px;padding-bottom:0px;padding-left:25px;word-break:break-word;">
                                                <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:20px;letter-spacing:normal;line-height:1;text-align:left;color:#000000;">
                                                    <p class="text-build-content" style="text-align: center; margin: 10px 0; margin-top: 10px; margin-bottom: 10px;" data-testid="SbHqm0O18"><span style="color:#109b39;font-family:Montserrat, Arial, Helvetica, sans-serif;font-size:20px;"><b>Résultats de l'évaluation de tes 12 compétences managériales clés :&nbsp;</b></span></p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:0px;padding:0px;padding-top:0px;padding-bottom:0px;word-break:break-word;">
                                                <div style="font-family:Arial, sans-serif;font-size:13px;letter-spacing:normal;line-height:1;color:#000000;">
                                                    <!doctype html>
                                                    <html lang="en">

                                                    <head>
                                                        <!-- Required meta tags -->
                                                        <meta charset="utf-8">
                                                        <meta name="viewport" content="width=device-width,initial-scale=1">
                                                        <!-- Bootstrap CSS -->
                                                        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
                                                        <title>Hello, world!</title>
                                                    </head>

                                                    <body>
                                                        <table class="table">
                                                            <thead></thead>
                                                            <tbody>
                                                                @php
                                                                    foreach($competences as $competence){
                                                                        if($competence->note < 5)
                                                                            $class = 'spinner-grow text-danger';
                                                                        else if ($competence->note >= 5 && $competence->note < 8)
                                                                            $class = 'spinner-grow text-warning';
                                                                        else if($competence->note >= 8)
                                                                            $class = 'spinner-grow text-success';
                                                                            
                                                                        echo
                                                                        '<tr>
                                                                            <td>' . $competence->libelle . '</td><td>' . $competence->note . '</td><td><div class="' . $class . '" role="status"><span  class="visually-hidden">Loading...</span></div></td>
                                                                        </tr>';
                                                                    }
                                                                @endphp
                                                            </tbody>
                                                        </table>
                                                        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
                                                        <!-- Option 2: Separate Popper and Bootstrap JS -->
                                                        <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
                                                    </body>

                                                    </html>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div style="margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                <tbody>
                    <tr>
                        <td style="direction:ltr;font-size:0px;padding:20px 0px 20px 0px;text-align:center;">
                            <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
                            <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                    <tbody>
                                        <tr>
                                            <td style="vertical-align:top;padding:0;">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                                                <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;color:#000000;">
                                                                    <p style="margin: 10px 0;">Cet email a été envoyé à [[EMAIL_TO]], <a href="[[UNSUB_LINK_FR]]" style="color:inherit;text-decoration:none;" target="_blank">cliquez ici pour vous désabonner</a>.</p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]></td></tr></table><![endif]-->
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.2/jquery.js" integrity="sha512-NMtENEqUQ8zHZWjwLg6/1FmcTWwRS2T5f487CCbQB3pQwouZfbrQfylryimT3XvQnpE7ctEKoZgQOAkWkCW/vg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
    <script>
        am4core.ready(function() {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("chartdiv", am4charts.PieChart);

            // Add and configure Series
            var pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "notes";
            pieSeries.dataFields.category = "categorie";
            pieSeries.slices.template.stroke = am4core.color("#fff");
            pieSeries.slices.template.strokeOpacity = 1;
            pieSeries.labels.template.fontSize = 16;
            pieSeries.labels.template.fill = am4core.color("white");

            // This creates initial animation
            pieSeries.hiddenState.properties.opacity = 1;
            pieSeries.hiddenState.properties.endAngle = -90;
            pieSeries.hiddenState.properties.startAngle = -90;

            chart.hiddenState.properties.radius = am4core.percent(0);

            var rgm = new am4core.RadialGradientModifier();
            pieSeries.slices.template.propertyFields.fill = "color";

        }); // end am4core.ready()
    </script>

</body>

</html>