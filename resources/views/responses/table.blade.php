<div class="table-responsive">
    <table class="table" id="responses-table">
        <thead>
        <tr>
            <th>Valeur</th>
        <th>Question Id</th>
        <th>Bonne Response</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($responses as $response)
            <tr>
                <td>{{ $response->valeur }}</td>
            <td>{{ $response->question_id }}</td>
            <td>{{ $response->bonne_response }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['responses.destroy', $response->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('responses.show', [$response->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('responses.edit', [$response->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
