<!-- Valeur Field -->
<div class="col-sm-12">
    {!! Form::label('valeur', 'Valeur:') !!}
    <p>{{ $response->valeur }}</p>
</div>

<!-- Question Id Field -->
<div class="col-sm-12">
    {!! Form::label('question_id', 'Question Id:') !!}
    <p>{{ $response->question_id }}</p>
</div>

<!-- Bonne Response Field -->
<div class="col-sm-12">
    {!! Form::label('bonne_response', 'Bonne Response:') !!}
    <p>{{ $response->bonne_response }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $response->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $response->updated_at }}</p>
</div>

