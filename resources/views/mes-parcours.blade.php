@extends('layouts.default')
@section('content')
<div class="content-mes-parcours">
    <h1>Mes parcours</h1>
   <div class="text-right">
       <button class="btn btn-parcours-formation">
           <img src="/img/btnPlus.png" alt="">
           Créer un parcours de formation
       </button>
   </div>
    <div class="card-element">
        <table id="table"

               data-search="true"
               data-filter-control="true"
               data-pagination="true"
               data-toolbar="#toolbar"
               class="table-parcours">
            <thead>
            <tr>
                <th data-field="Parcours-de-formation" data-filter-control="input" data-sortable="true" class="title-formation">Parcours de formation</th>
                <th data-field="Apprenants" data-filter-control="select" data-sortable="true" class="title-apprenants">Apprenants</th>
                <th data-field="Mes-commissions" data-filter-control="select" data-sortable="true" class="title-commission">Mes commissions</th>
                <th data-field="modifier-parcours" data-sortable="false"></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="confiance-block-td"> La confiance en soi pour les managers</td>
                <td class="text-center">75</td>
                <td class="somme-table">725 000 FCFA</td>
                <td class="text-center">
                    <button class="btn btn-delete">Delete</button>
                </td>
            </tr>
            <tr>
                <td class="management-block-td">Le management 3.0 et le bonheur au travail</td>
                <td class="text-center">50</td>
                <td class="somme-table">150 000 FCFA</td>
                <td class="text-center">
                    <button class="btn btn-delete">Delete</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


@stop
