<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{{ $caracteristique->libelle }}</p>
</div>

<!-- Couleur Id Field -->
<div class="col-sm-12">
    {!! Form::label('couleur_id', 'Couleur Id:') !!}
    <p>{{ $caracteristique->couleur_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $caracteristique->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $caracteristique->updated_at }}</p>
</div>

