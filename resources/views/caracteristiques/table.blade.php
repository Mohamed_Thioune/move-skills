<div class="table-responsive">
    <table class="table" id="caracteristiques-table">
        <thead>
        <tr>
            <th>Libelle</th>
        <th>Couleur Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($caracteristiques as $caracteristique)
            <tr>
                <td>{{ $caracteristique->libelle }}</td>
            <td>{{ $caracteristique->couleur_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['caracteristiques.destroy', $caracteristique->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('caracteristiques.show', [$caracteristique->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('caracteristiques.edit', [$caracteristique->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
