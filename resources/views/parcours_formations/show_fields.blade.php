<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{{ $parcoursFormation->libelle }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $parcoursFormation->description }}</p>
</div>

<!-- Prix Field -->
<div class="col-sm-12">
    {!! Form::label('prix', 'Prix:') !!}
    <p>{{ $parcoursFormation->prix }}</p>
</div>

<!-- Duree Field -->
<div class="col-sm-12">
    {!! Form::label('duree', 'Duree:') !!}
    <p>{{ $parcoursFormation->duree }}</p>
</div>

<!-- Formateur Id Field -->
<div class="col-sm-12">
    {!! Form::label('formateur_id', 'Formateur Id:') !!}
    <p>{{ $parcoursFormation->formateur_id }}</p>
</div>

<!-- Parcours Transformation Id Field -->
<div class="col-sm-12">
    {!! Form::label('parcours_transformation_id', 'Parcours Transformation Id:') !!}
    <p>{{ $parcoursFormation->parcours_transformation_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $parcoursFormation->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $parcoursFormation->updated_at }}</p>
</div>

