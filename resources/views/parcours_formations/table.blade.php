<div class="table-responsive">
    <table class="table" id="parcoursFormations-table">
        <thead>
        <tr>
            <th>Libelle</th>
        <th>Description</th>
        <th>Prix</th>
        <th>Duree</th>
        <th>Formateur Id</th>
        <th>Parcours Transformation Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($parcoursFormations as $parcoursFormation)
            <tr>
                <td>{{ $parcoursFormation->libelle }}</td>
            <td>{{ $parcoursFormation->description }}</td>
            <td>{{ $parcoursFormation->prix }}</td>
            <td>{{ $parcoursFormation->duree }}</td>
            <td>{{ $parcoursFormation->formateur_id }}</td>
            <td>{{ $parcoursFormation->parcours_transformation_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['parcoursFormations.destroy', $parcoursFormation->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('parcoursFormations.show', [$parcoursFormation->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('parcoursFormations.edit', [$parcoursFormation->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
