<!-- Valeur Field -->
<div class="col-sm-12">
    {!! Form::label('valeur', 'Valeur:') !!}
    <p>{{ $responsesQuizz->valeur }}</p>
</div>

<!-- Question Quizz Id Field -->
<div class="col-sm-12">
    {!! Form::label('question_quizz_id', 'Question Quizz Id:') !!}
    <p>{{ $responsesQuizz->question_quizz_id }}</p>
</div>

<!-- Bonne Reponse Field -->
<div class="col-sm-12">
    {!! Form::label('bonne_reponse', 'Bonne Reponse:') !!}
    <p>{{ $responsesQuizz->bonne_reponse }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $responsesQuizz->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $responsesQuizz->updated_at }}</p>
</div>

