<div class="table-responsive">
    <table class="table" id="responsesQuizzs-table">
        <thead>
        <tr>
            <th>Valeur</th>
        <th>Question Quizz Id</th>
        <th>Bonne Reponse</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($responsesQuizzs as $responsesQuizz)
            <tr>
                <td>{{ $responsesQuizz->valeur }}</td>
            <td>{{ $responsesQuizz->question_quizz_id }}</td>
            <td>{{ $responsesQuizz->bonne_reponse }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['responsesQuizzs.destroy', $responsesQuizz->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('responsesQuizzs.show', [$responsesQuizz->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('responsesQuizzs.edit', [$responsesQuizz->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
