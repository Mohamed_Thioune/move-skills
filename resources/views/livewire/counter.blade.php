@extends('layouts.default')
@section('content')
<div class="content-add-parcours">
    <h1>Créer un parcours de formation</h1>
    <div class="content-form">
        {{-- <form> --}}
            <div class="form-one">
                <h2>Etape 2  : Définition de l'activite </h2>
                <div class="form-group">
                    <input type="checkbox" id="" class="form-control" wire:model.lazy="video" value="Video">
                    <label for="">Video</label>

                    <input type="checkbox" id="" class="form-control" wire:model.lazy="audio" value="Audio">
                    <label for="">Audio</label>

                    <input type="checkbox" id="" class="form-control" wire:model.lazy="artikel" value="Artikel">
                    <label for="">Article</label>

                    <input type="checkbox" id="" class="form-control" wire:model.lazy="citation" value="Citation">
                    <label for="">Citation</label>

                    <input type="checkbox" id="" class="form-control" wire:model.lazy="challenge" value="Challenge">
                    <label for="">Challenge</label>
                </div>
        
                <button class="btn btn-primary" wire:click="save">Enregisrer et poursuivre</button>
            </div>
        {{-- </form> --}}

        @foreach ($videos as $video)
            {{ $video }}
        @endforeach
    </div>
</div>
@stop