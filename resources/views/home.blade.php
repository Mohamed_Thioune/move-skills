@extends('layouts.default')
@section('content')
<div class="content-dashboard-home">
    <h1>Tableau de bord</h1>
     <h6> @if (session('admin'))
            <div class="alert alert-success" role="alert">
                 {{ session('admin') }}
            </div>  
     @endif</h6>
    <div class="row rowModife2">
        <div class="col-md-3">
            <div class="card-learning-home">
                <div class="block-img-card-home">
                    <img src="/img/Vector-rose.png" alt="">
                </div>
                <div class="block-group-text">
                    <p class="number">556</p>
                    <p class="card-name-text">Total apprenants </p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-learning-home">
                <div class="block-img-card-home">
                    <img src="/img/Vector-verte.png" alt="">
                </div>
                <div class="block-group-text">
                    <p class="number">556</p>
                    <p class="card-name-text">Parcours  Vendus</p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-learning-home">
                <div class="block-img-card-home">
                    <img src="/img/Vector-bleu.png" alt="">
                </div>
                <div class="block-group-text">
                    <p class="number">556</p>
                    <p class="card-name-text">Vidéos au Total </p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-learning-home">
                <div class="block-img-card-home">
                    <img src="/img/Vector-jaune.png" alt="">
                </div>
                <div class="block-group-text">
                    <p class="number">2 345 000 fcfa</p>
                    <p class="card-name-text">Total Ventes </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row rowModife2">
        <div class="col-md-7">
            <div class="card-element">
                <div class="head-card-element">
                    <p class="title-head-card-element">Evolution des ventes des parcours </p>
                    <div>
                        <select class="form-select" aria-label="Default select example">
                            <option selected>2022</option>
                            <option value="1">2021</option>
                            <option value="2">2020</option>
                            <option value="3">2019</option>
                        </select>
                    </div>
                </div>
                    <div>
                        <canvas id="myChart"></canvas>
                    </div>

            </div>
        </div>
        <div class="col-md-5">
            <div class="card-element">
                <div class="head-card-element">
                    <p class="title-head-card-element">Mes parcours les plus suivis </p>
                </div>
                <div class="content-parcours">
                    <div class="group-one">
                        <img src="/img/icone7.svg" alt="">
                        <div>
                            <p class="title">L’Art du Focus </p>
                            <p class="number">#10</p>
                        </div>
                    </div>
                    <div class="prix-vente-block">
                        <p class="price">250 000 FCFA</p>
                        <p class="number-ventes">10 Ventes</p>
                    </div>
                </div>
                <div class="content-parcours">
                    <div class="group-one">
                        <img src="/img/icone8.png" alt="">
                        <div>
                            <p class="title">L’Art de la négociation</p>
                            <p class="number">#10</p>
                        </div>
                    </div>
                    <div class="prix-vente-block">
                        <p class="price">250 000 FCFA</p>
                        <p class="number-ventes">10 Ventes</p>
                    </div>
                </div>
                <div class="content-parcours">
                    <div class="group-one">
                        <img src="/img/icone9.png" alt="">
                        <div>
                            <p class="title">Le Leadership Personnel </p>
                            <p class="number">#10</p>
                        </div>
                    </div>
                    <div class="prix-vente-block">
                        <p class="price">250 000 FCFA</p>
                        <p class="number-ventes">10 Ventes</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row rowModife2">
        <div class="col-md-5">
            <div class="card-element">
                <div class="head-card-element">
                    <p class="title-head-card-element">Transactions</p>
                    <a href="">Voir tout</a>
                </div>
                <div class="content-parcours content-transcations">
                    <div class="group-one">
                        <div class="img-user-transcation">
                            <img src="/img/user-card.png" alt="">
                        </div>
                        <div>
                            <p class="title">10 000 FCFA </p>
                            <p class="number">De : Axel Nonguierma</p>
                        </div>
                    </div>
                    <div class="prix-vente-block">
                        <p class="number-ventes">7 Octobre</p>
                    </div>
                </div>
                <div class="content-parcours content-transcations">
                    <div class="group-one">
                        <div class="img-user-transcation">
                            <img src="/img/user-card.png" alt="">
                        </div>
                        <div>
                            <p class="title">10 000 FCFA </p>
                            <p class="number">De : Axel Nonguierma</p>
                        </div>
                    </div>
                    <div class="prix-vente-block">
                        <p class="number-ventes">7 Octobre</p>
                    </div>
                </div>
                <div class="content-parcours content-transcations">
                    <div class="group-one">
                        <div class="img-user-transcation">
                            <img src="/img/user-card.png" alt="">
                        </div>
                        <div>
                            <p class="title">10 000 FCFA </p>
                            <p class="number">De : Axel Nonguierma</p>
                        </div>
                    </div>
                    <div class="prix-vente-block">
                        <p class="number-ventes">7 Octobre</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-5">
            <div class="card-element">
                <div class="head-card-element">
                    <p class="title-head-card-element">Engagement sur mes formations </p>
                    <div class="dot-block">
                        <img src="/img/dot.png" alt="">
                    </div>
                </div>
                <div class="content-map">
                    <img src="/img/map.png" alt="">
                </div>
                <div class="footer-content-card-element">
                    <div class="legend">
                        <div class="circle canada"></div>
                        <p class="legend-pays">Canada <span>46%</span></p>
                    </div>
                    <div class="legend">
                        <div class="circle afrique"></div>
                        <p class="legend-pays">Afrique <span>24%</span></p>
                    </div>
                    <div class="legend">
                        <div class="circle chine"></div>
                        <p class="legend-pays">China <span>16%</span></p>
                    </div>
                    <div class="legend">
                        <div class="circle australie"></div>
                        <p class="legend-pays">Australia <span>14%</span></p>
                    </div class="legend-pays">
                </div>

            </div>
        </div>
    </div>
</div>
@stop
