<!-- Note Field -->
<div class="col-sm-12">
    {!! Form::label('note', 'Note:') !!}
    <p>{{ $responseByUser->note }}</p>
</div>

<!-- Question Id Field -->
<div class="col-sm-12">
    {!! Form::label('question_id', 'Question Id:') !!}
    <p>{{ $responseByUser->question_id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $responseByUser->user_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $responseByUser->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $responseByUser->updated_at }}</p>
</div>

