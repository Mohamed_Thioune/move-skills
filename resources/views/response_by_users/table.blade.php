<div class="table-responsive">
    <table class="table" id="responseByUsers-table">
        <thead>
        <tr>
            <th>Note</th>
        <th>Question Id</th>
        <th>User Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($responseByUsers as $responseByUser)
            <tr>
                <td>{{ $responseByUser->note }}</td>
            <td>{{ $responseByUser->question_id }}</td>
            <td>{{ $responseByUser->user_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['responseByUsers.destroy', $responseByUser->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('responseByUsers.show', [$responseByUser->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('responseByUsers.edit', [$responseByUser->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
