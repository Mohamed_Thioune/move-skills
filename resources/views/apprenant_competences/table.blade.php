<div class="table-responsive">
    <table class="table" id="apprenantCompetences-table">
        <thead>
        <tr>
            <th>Unsignedinteger</th>
        <th>Unsignedinteger</th>
        <th>State</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($apprenantCompetences as $apprenantCompetence)
            <tr>
                <td>{{ $apprenantCompetence->unsignedInteger }}</td>
            <td>{{ $apprenantCompetence->unsignedInteger }}</td>
            <td>{{ $apprenantCompetence->state }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['apprenantCompetences.destroy', $apprenantCompetence->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('apprenantCompetences.show', [$apprenantCompetence->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('apprenantCompetences.edit', [$apprenantCompetence->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
