<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{{ $quizz->libelle }}</p>
</div>

<!-- Video Id Field -->
<div class="col-sm-12">
    {!! Form::label('video_id', 'Video Id:') !!}
    <p>{{ $quizz->video_id }}</p>
</div>

<!-- Audio Id Field -->
<div class="col-sm-12">
    {!! Form::label('audio_id', 'Audio Id:') !!}
    <p>{{ $quizz->audio_id }}</p>
</div>

<!-- Article Id Field -->
<div class="col-sm-12">
    {!! Form::label('article_id', 'Article Id:') !!}
    <p>{{ $quizz->article_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $quizz->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $quizz->updated_at }}</p>
</div>

