<div class="table-responsive">
    <table class="table" id="quizzs-table">
        <thead>
        <tr>
            <th>Libelle</th>
        <th>Video Id</th>
        <th>Audio Id</th>
        <th>Article Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($quizzs as $quizz)
            <tr>
                <td>{{ $quizz->libelle }}</td>
            <td>{{ $quizz->video_id }}</td>
            <td>{{ $quizz->audio_id }}</td>
            <td>{{ $quizz->article_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['quizzs.destroy', $quizz->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('quizzs.show', [$quizz->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('quizzs.edit', [$quizz->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
