<div class="table-responsive">
    <table class="table" id="parcourActivites-table">
        <thead>
        <tr>
            <th>Type</th>
        <th>User Id</th>
        <th>Parcours Id</th>
        <th>Activity? Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($parcourActivites as $parcourActivite)
            <tr>
                <td>{{ $parcourActivite->type }}</td>
            <td>{{ $parcourActivite->user_id }}</td>
            <td>{{ $parcourActivite->parcours_id }}</td>
            <td>{{ $parcourActivite->activity�_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['parcourActivites.destroy', $parcourActivite->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('parcourActivites.show', [$parcourActivite->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('parcourActivites.edit', [$parcourActivite->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
