<!-- Type Field -->
<div class="col-sm-12">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $parcourActivite->type }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $parcourActivite->user_id }}</p>
</div>

<!-- Parcours Id Field -->
<div class="col-sm-12">
    {!! Form::label('parcours_id', 'Parcours Id:') !!}
    <p>{{ $parcourActivite->parcours_id }}</p>
</div>

<!-- Activity? Id Field -->
<div class="col-sm-12">
    {!! Form::label('activity�_id', 'Activity? Id:') !!}
    <p>{{ $parcourActivite->activity�_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $parcourActivite->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $parcourActivite->updated_at }}</p>
</div>

