<div class="table-responsive">
    <table class="table" id="apprenants-table">
        <thead>
        <tr>
            <th>User Id</th>
        <th>Couleur Id</th>
        <th>Telephone</th>
        <th>Whatsapp</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($apprenants as $apprenant)
            <tr>
                <td>{{ $apprenant->user_id }}</td>
            <td>{{ $apprenant->couleur_id }}</td>
            <td>{{ $apprenant->telephone }}</td>
            <td>{{ $apprenant->whatsapp }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['apprenants.destroy', $apprenant->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('apprenants.show', [$apprenant->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('apprenants.edit', [$apprenant->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
