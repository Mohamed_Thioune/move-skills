<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $apprenant->user_id }}</p>
</div>

<!-- Couleur Id Field -->
<div class="col-sm-12">
    {!! Form::label('couleur_id', 'Couleur Id:') !!}
    <p>{{ $apprenant->couleur_id }}</p>
</div>

<!-- Telephone Field -->
<div class="col-sm-12">
    {!! Form::label('telephone', 'Telephone:') !!}
    <p>{{ $apprenant->telephone }}</p>
</div>

<!-- Whatsapp Field -->
<div class="col-sm-12">
    {!! Form::label('whatsapp', 'Whatsapp:') !!}
    <p>{{ $apprenant->whatsapp }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $apprenant->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $apprenant->updated_at }}</p>
</div>

