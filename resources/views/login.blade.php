<!doctype html>
<html lang="en">
<head>
    <title>Login 08</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">

</head>
<body>
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 text-center mb-4">
                <h2 class="heading-section">Connectez-vous pour accéder au dashboard </h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-5">
                <div class="login-wrap">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <img src="/img/Logo.png" alt="">
                    </div>
                    <h3 class="text-center mb-4">Vous avez un compte ?</h3>
                    @include('flash::message')
                    {{--
                        <h6> 
                        @if (session('message'))
                            <div class="alert alert-success" style="background-color:lightgreen" role="alert">
                                {{ session('message') }}
                            </div>  
                        @endif
                        </h6> 
                    --}}
                    <form action="{{route('connecter')}}" method="post" class="login-form">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="email" class="form-control rounded-left" placeholder="Email d'utilisateur" required>
                        </div>
                        <div class="form-group d-flex">
                            <input type="password" name="password" class="form-control rounded-left" placeholder="Mot de passe" required>
                        </div>
                        <div class="form-group d-md-flex flex-wrap">
                            <div class="w-50">
                                <label class="checkbox-wrap checkbox-primary">Se souvenir de moi
                                    <input type="checkbox" checked>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="w-50 text-md-right">
                                <a href="#">Mot de passe oublié ?</a>
                            </div>
                            <div class="w-100 text-center">
                                <a href="{{ ('inscription') }}">Vous n'avez pas de compte ?</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary rounded submit p-3 px-5">Se connecter</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="/js/jquery.min.js"></script>
<script src="/js/popper.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script>

</body>
</html>

