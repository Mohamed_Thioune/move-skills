<div class="table-responsive">
    <table class="table" id="citations-table">
        <thead>
        <tr>
            <th>Content</th>
        <th>Parcours Formation Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($citations as $citation)
            <tr>
                <td>{{ $citation->content }}</td>
            <td>{{ $citation->parcours_formation_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['citations.destroy', $citation->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('citations.show', [$citation->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('citations.edit', [$citation->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
