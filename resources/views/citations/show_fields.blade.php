<!-- Content Field -->
<div class="col-sm-12">
    {!! Form::label('content', 'Content:') !!}
    <p>{{ $citation->content }}</p>
</div>

<!-- Parcours Formation Id Field -->
<div class="col-sm-12">
    {!! Form::label('parcours_formation_id', 'Parcours Formation Id:') !!}
    <p>{{ $citation->parcours_formation_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $citation->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $citation->updated_at }}</p>
</div>

