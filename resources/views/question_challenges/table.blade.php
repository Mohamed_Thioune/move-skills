<div class="table-responsive">
    <table class="table" id="questionChallenges-table">
        <thead>
        <tr>
            <th>Libelle</th>
        <th>Challenge Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($questionChallenges as $questionChallenge)
            <tr>
                <td>{{ $questionChallenge->libelle }}</td>
            <td>{{ $questionChallenge->challenge_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['questionChallenges.destroy', $questionChallenge->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('questionChallenges.show', [$questionChallenge->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('questionChallenges.edit', [$questionChallenge->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
