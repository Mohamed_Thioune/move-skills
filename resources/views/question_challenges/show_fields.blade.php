<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{{ $questionChallenge->libelle }}</p>
</div>

<!-- Challenge Id Field -->
<div class="col-sm-12">
    {!! Form::label('challenge_id', 'Challenge Id:') !!}
    <p>{{ $questionChallenge->challenge_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $questionChallenge->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $questionChallenge->updated_at }}</p>
</div>

