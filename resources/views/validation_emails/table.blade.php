<div class="table-responsive">
    <table class="table" id="validationEmails-table">
        <thead>
        <tr>
            <th>Code</th>
        <th>User Id</th>
        <th>Email</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($validationEmails as $validationEmail)
            <tr>
                <td>{{ $validationEmail->code }}</td>
            <td>{{ $validationEmail->user_id }}</td>
            <td>{{ $validationEmail->email }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['validationEmails.destroy', $validationEmail->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('validationEmails.show', [$validationEmail->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('validationEmails.edit', [$validationEmail->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
