<div class="table-responsive">
    <table class="table" id="competences-table">
        <thead>
        <tr>
            <th>Type</th>
        <th>Content</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($competences as $competence)
            <tr>
                <td>{{ $competence->type }}</td>
            <td>{{ $competence->content }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['competences.destroy', $competence->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('competences.show', [$competence->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('competences.edit', [$competence->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
