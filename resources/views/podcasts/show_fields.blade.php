<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{{ $podcast->libelle }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $podcast->description }}</p>
</div>

<!-- Podcast Field -->
<div class="col-sm-12">
    {!! Form::label('podcast', 'Podcast:') !!}
    <p>{{ $podcast->podcast }}</p>
</div>

<!-- Parcours Formation Id Field -->
<div class="col-sm-12">
    {!! Form::label('parcours_formation_id', 'Parcours Formation Id:') !!}
    <p>{{ $podcast->parcours_formation_id }}</p>
</div>

<!-- Formateur Id Field -->
<div class="col-sm-12">
    {!! Form::label('formateur_id', 'Formateur Id:') !!}
    <p>{{ $podcast->formateur_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $podcast->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $podcast->updated_at }}</p>
</div>

