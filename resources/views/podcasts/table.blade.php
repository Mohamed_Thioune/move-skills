<div class="table-responsive">
    <table class="table" id="podcasts-table">
        <thead>
        <tr>
            <th>Libelle</th>
        <th>Description</th>
        <th>Podcast</th>
        <th>Parcours Formation Id</th>
        <th>Formateur Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($podcasts as $podcast)
            <tr>
                <td>{{ $podcast->libelle }}</td>
            <td>{{ $podcast->description }}</td>
            <td>{{ $podcast->podcast }}</td>
            <td>{{ $podcast->parcours_formation_id }}</td>
            <td>{{ $podcast->formateur_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['podcasts.destroy', $podcast->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('podcasts.show', [$podcast->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('podcasts.edit', [$podcast->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
