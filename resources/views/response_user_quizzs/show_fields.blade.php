<!-- Note Field -->
<div class="col-sm-12">
    {!! Form::label('note', 'Note:') !!}
    <p>{{ $responseUserQuizz->note }}</p>
</div>

<!-- Question Quizz Id Field -->
<div class="col-sm-12">
    {!! Form::label('question_quizz_id', 'Question Quizz Id:') !!}
    <p>{{ $responseUserQuizz->question_quizz_id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $responseUserQuizz->user_id }}</p>
</div>

<!-- Passer Test Id Field -->
<div class="col-sm-12">
    {!! Form::label('passer_test_id', 'Passer Test Id:') !!}
    <p>{{ $responseUserQuizz->passer_test_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $responseUserQuizz->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $responseUserQuizz->updated_at }}</p>
</div>

