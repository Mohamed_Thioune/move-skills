<div class="table-responsive">
    <table class="table" id="responseUserQuizzs-table">
        <thead>
        <tr>
            <th>Note</th>
        <th>Question Quizz Id</th>
        <th>User Id</th>
        <th>Passer Test Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($responseUserQuizzs as $responseUserQuizz)
            <tr>
                <td>{{ $responseUserQuizz->note }}</td>
            <td>{{ $responseUserQuizz->question_quizz_id }}</td>
            <td>{{ $responseUserQuizz->user_id }}</td>
            <td>{{ $responseUserQuizz->passer_test_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['responseUserQuizzs.destroy', $responseUserQuizz->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('responseUserQuizzs.show', [$responseUserQuizz->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('responseUserQuizzs.edit', [$responseUserQuizz->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
