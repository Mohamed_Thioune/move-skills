<div class="table-responsive">
    <table class="table" id="canvaMiniDisqs-table">
        <thead>
        <tr>
            <th>Row</th>
        <th>Column</th>
        <th>Couleur Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($canvaMiniDisqs as $canvaMiniDisq)
            <tr>
                <td>{{ $canvaMiniDisq->row }}</td>
            <td>{{ $canvaMiniDisq->column }}</td>
            <td>{{ $canvaMiniDisq->couleur_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['canvaMiniDisqs.destroy', $canvaMiniDisq->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('canvaMiniDisqs.show', [$canvaMiniDisq->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('canvaMiniDisqs.edit', [$canvaMiniDisq->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
