<!-- Row Field -->
<div class="col-sm-12">
    {!! Form::label('row', 'Row:') !!}
    <p>{{ $canvaMiniDisq->row }}</p>
</div>

<!-- Column Field -->
<div class="col-sm-12">
    {!! Form::label('column', 'Column:') !!}
    <p>{{ $canvaMiniDisq->column }}</p>
</div>

<!-- Couleur Id Field -->
<div class="col-sm-12">
    {!! Form::label('couleur_id', 'Couleur Id:') !!}
    <p>{{ $canvaMiniDisq->couleur_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $canvaMiniDisq->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $canvaMiniDisq->updated_at }}</p>
</div>

