<!-- Video Id Field -->
<div class="col-sm-12">
    {!! Form::label('video_id', 'Video Id:') !!}
    <p>{{ $commentaire->video_id }}</p>
</div>

<!-- Audio Id Field -->
<div class="col-sm-12">
    {!! Form::label('audio_id', 'Audio Id:') !!}
    <p>{{ $commentaire->audio_id }}</p>
</div>

<!-- Article Id Field -->
<div class="col-sm-12">
    {!! Form::label('article_id', 'Article Id:') !!}
    <p>{{ $commentaire->article_id }}</p>
</div>

<!-- Text Field -->
<div class="col-sm-12">
    {!! Form::label('text', 'Text:') !!}
    <p>{{ $commentaire->text }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $commentaire->user_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $commentaire->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $commentaire->updated_at }}</p>
</div>

