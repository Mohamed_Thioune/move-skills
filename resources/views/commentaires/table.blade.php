<div class="table-responsive">
    <table class="table" id="commentaires-table">
        <thead>
        <tr>
            <th>Video Id</th>
        <th>Audio Id</th>
        <th>Article Id</th>
        <th>Text</th>
        <th>User Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($commentaires as $commentaire)
            <tr>
                <td>{{ $commentaire->video_id }}</td>
            <td>{{ $commentaire->audio_id }}</td>
            <td>{{ $commentaire->article_id }}</td>
            <td>{{ $commentaire->text }}</td>
            <td>{{ $commentaire->user_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['commentaires.destroy', $commentaire->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('commentaires.show', [$commentaire->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('commentaires.edit', [$commentaire->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
