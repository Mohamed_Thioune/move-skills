@extends('layouts.default')
@section('content')
<div class="content-mes-parcours">
    <h1>Mes activités</h1>
   <div class="text-right">
       <a href="/creation-activites">
        <button class="btn btn-parcours-formation">
           <img src="/img/btnPlus.png" alt="">
           Créer une activité
       </button>
        </a>
   </div>
    <div class="card-element">
        <table id="table"

               data-search="true"
               data-filter-control="true"
               data-pagination="true"
               data-toolbar="#toolbar"
               class="table-parcours">
            <thead>
            <tr>
                <th data-field="Parcours-de-formation" data-filter-control="input" data-sortable="true" class="title-formation">Activités</th>
                <th data-field="Apprenants" data-filter-control="select" data-sortable="true" class="title-apprenants">Parcours de formation</th>
                <th data-field="Mes-commissions" data-filter-control="select" data-sortable="true" class="title-commission">Type</th>
                <!-- <th data-field="Mes-commissions" data-filter-control="select" data-sortable="true" class="title-commission">Contenu</th> -->
                <th data-field="modifier-parcours" data-sortable="false"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($activites as $activite)
            <tr>
                <td class="confiance-block-td"> {{($activite->libelle) ? $activite->libelle : '--'}}</td>
                <td class="text-center">{{($activite->libelle_perfo) ? $activite->libelle_perfo : '--'}}</td>
                <td class="text-center">{{($activite->libelle_type) ? $activite->libelle_type : '--'}}</td>
                <!-- @if($activite->libelle_type == "video")
                <td class="somme-table">{{($activite->video) ? $activite->video : '--'}}</td>
                @elseif($activite->libelle_type == "audios")
                <td class="somme-table">{{($activite->audios) ? $activite->audios : '--'}}</td>
                @elseif($activite->libelle_type == "articles")
                <td class="somme-table">{{($activite->articles) ? $activite->articles : '--'}}</td>
                @endif -->
                <td class="text-center">
                <form action="{{ route('delete_activites',$activite->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                    <button type="submit" class="btn btn-delete">Delete</button>
                </form>
                </td>
            </tr>
            @endforeach
            <!-- <tr>
                <td class="management-block-td">Le management 3.0 et le bonheur au travail</td>
                <td class="text-center">50</td>
                <td class="somme-table">150 000 FCFA</td>
                <td class="text-center">
                    <button class="btn btn-delete">Delete</button>
                </td>
            </tr> -->
            </tbody>
        </table>
    </div>
</div>


@stop
