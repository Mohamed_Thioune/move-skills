@extends('layouts.default')
@section('content')
<div class="content-add-parcours">
    <h1>Créer une activité</h1>
    <div class="content-form">
    <h6> 
              @if (session('message'))
                  <div class="alert alert-success" style="background-color:lightgreen" role="alert">
                      {{ session('message') }}
                  </div>  
              @endif
            </h6>
    <form action="{{route('store_activites')}}" method="post" class="login-form">
                        @csrf
            <div class="form-one">
                <h2>Etape 1  : Configuration de base de l' activité  </h2>
                <div class="form-group">
                    <label for="name">Nom du parcours de l' activité</label>
                    <input type="text" class="form-control" name="libelle" id="name" aria-describedby="name" placeholder="">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect2">Type</label>
                    <select multiple class="form-control" name="type_id" id="exampleFormControlSelect2">
                        <option id="check" value="">Sélectionner le type</option>
                        @foreach($types as $type)
                        <option idval="{{$type->id}}" value="{{$type->id}}">{{$type->libelle}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect2">Parcours de formations</label>
                    <select multiple class="form-control" name="parcours_formation_id" id="exampleFormControlSelect2">
                        <option value="">Sélectionner le Parcours de formations</option>
                        @foreach($parcours as $parcour)
                        <option value="{{$parcour->id}}">{{$parcour->libelle}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="videos">
                    <label for="name" >Lien vidéo</label>
                    <input type="text" class="form-control" name="video" id="name" aria-describedby="name" placeholder="">
                </div>
                <div class="form-group" id="audios">
                    <label for="name">Lien audio</label>
                    <input type="text" class="form-control" name="audios" id="audio" aria-describedby="name" placeholder="">
                </div>
                <div class="form-group" id="articles">
                    <label for="exampleFormControlTextarea1">Artucles</label>
                    <textarea class="form-control" name="articles" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <!-- <div class="form-group">
                    <label for="exampleFormControlSelect2">Example multiple select</label>
                    <select multiple class="form-control" id="exampleFormControlSelect2">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div> -->
                

                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>


    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

            <script>
            $("#videos").hide();
            $("#audios").hide();
            $("#articles").hide();
            </script>
             <script>
            $(document).ready(function(){
              $("select").change(function(){
                    $( "select option:selected").each(function(){
                        //enter bengal districts
                        if($(this).attr("idval")=="1"){
                            $("#audios").hide();
                            $("#articles").hide();
                            $("#videos").show();
                        }
                        if($(this).attr("idval")=="2"){
                            $("#articles").show();
                            $("#audios").hide();
                            $("#videos").hide();
                        }
                        if($(this).attr("idval")=="3"){
                            $("#articles").hide();
                            $("#audios").show();
                            $("#videos").hide();
                        }
                        //enter other states
                       
                    });
                });  
            }); 

                </script>


@stop
