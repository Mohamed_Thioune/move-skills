<div class="table-responsive">
    <table class="table" id="points-table">
        <thead>
        <tr>
            <th>Value</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($points as $points)
            <tr>
                <td>{{ $points->value }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['points.destroy', $points->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('points.show', [$points->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('points.edit', [$points->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
