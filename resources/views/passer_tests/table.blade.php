<div class="table-responsive">
    <table class="table" id="passerTests-table">
        <thead>
        <tr>
            <th>Roue De Vie Id</th>
        <th>Canva Mini Disq Id</th>
        <th>Quizz Id</th>
        <th>Competence Id</th>
        <th>User Id</th>
        <th>Point Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($passerTests as $passerTest)
            <tr>
                <td>{{ $passerTest->roue_de_vie_id }}</td>
            <td>{{ $passerTest->canva_mini_disq_id }}</td>
            <td>{{ $passerTest->quizz_id }}</td>
            <td>{{ $passerTest->competence_id }}</td>
            <td>{{ $passerTest->user_id }}</td>
            <td>{{ $passerTest->point_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['passerTests.destroy', $passerTest->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('passerTests.show', [$passerTest->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('passerTests.edit', [$passerTest->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
