<!-- Roue De Vie Id Field -->
<div class="col-sm-12">
    {!! Form::label('roue_de_vie_id', 'Roue De Vie Id:') !!}
    <p>{{ $passerTest->roue_de_vie_id }}</p>
</div>

<!-- Canva Mini Disq Id Field -->
<div class="col-sm-12">
    {!! Form::label('canva_mini_disq_id', 'Canva Mini Disq Id:') !!}
    <p>{{ $passerTest->canva_mini_disq_id }}</p>
</div>

<!-- Quizz Id Field -->
<div class="col-sm-12">
    {!! Form::label('quizz_id', 'Quizz Id:') !!}
    <p>{{ $passerTest->quizz_id }}</p>
</div>

<!-- Competence Id Field -->
<div class="col-sm-12">
    {!! Form::label('competence_id', 'Competence Id:') !!}
    <p>{{ $passerTest->competence_id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $passerTest->user_id }}</p>
</div>

<!-- Point Id Field -->
<div class="col-sm-12">
    {!! Form::label('point_id', 'Point Id:') !!}
    <p>{{ $passerTest->point_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $passerTest->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $passerTest->updated_at }}</p>
</div>

