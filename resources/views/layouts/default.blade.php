@include('layouts.head' )

<div class="content-dashboard">
    <div class="theme-side-menu">
        @include('layouts.left-menu' )
    </div>
    <div class="theme-element-dashboard">
        @include('layouts.navbar' )
        @yield('content' )
    </div>
</div>

<script src="/js/jquery.min.js"></script>
<script src="/js/popper.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.0/bootstrap-table.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/extensions/editable/bootstrap-table-editable.js"></script>

<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line', // also try bar or other graph types

        // The data for our dataset
        data: {
            labels: ["Jan", "Fev", "Mars", "Avr", "Mai", "Juin", "Juil"],
            // Information about the dataset
            datasets: [{
                label: "Vente",
                backgroundColor: '#4dc5914d',
                borderColor: '#5FB512',
                data: [20, 50, 40, 60, 80, 90, 100],
            }]
        },

        // Configuration options
        options: {
            layout: {
                padding: 10,
            },
            legend: {
                position: 'bottom',
                display: false,
            },
            title: {
                display: false,
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'pourcentage de vente'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Month of the Year'
                    }
                }]
            }
        }
    });

</script>
