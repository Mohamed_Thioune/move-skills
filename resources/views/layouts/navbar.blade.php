<nav class="navbar navbar-static-top" role="navigation">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a href="#" class="nav-link" data-widget="pushmenu" role="button">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="" class="nav-link">Accueil</a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto ">
        <li class="nav-item dropdown notification">
            <a href="#" class="dropdown-toggle" id="dropdownNotification" data-toggle="dropdown">
                <img src="/img/Notification.png" alt="">
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownNotification">
                <a class="dropdown-item" href="#">24 Notifications</a>
            </div>

        </li>
        <!-- User Account Menu -->
        <li class="dropdown user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" id="dropdownUser" data-toggle="dropdown">
                <div class="userProfil">
                    <img src="/img/user.png" alt="">
                </div>
                <span class="hidden-xs">{{Auth::user()->prenom}}</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownUser">
                <a class="dropdown-item" href="#">Profil</a>
                <a class="dropdown-item" href="#">Parametre</a>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                   {{ __('Déconnexion') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</nav>
