<div class="w-100">
    <div class="content-logo">
        <img src="/img/Logo.png" alt="">
    </div>
    <ul>
        <li class="element-content-side-menu active">
            <a href="/admin/dashboard">
                <img src="/img/Category.png" alt="">
                <span>Tableau de Bord</span>
            </a>
        </li>
        <li class="element-content-side-menu">
            <a href="/mes-parcours">
                <img src="/img/Document.png" alt="">
                <span>Mes parcours</span>
            </a>
        </li>
        <li class="element-content-side-menu">
            <a href="/creation-parcours-formation">
                <img src="/img/Document.png" alt="">
                <span>Ajouter parcours</span>
            </a>
        </li>
        <li class="element-content-side-menu">
            <a href="/mes-activites">
                <img src="/img/Document.png" alt="">
                <span>Mes activités</span>
            </a>
        </li>
        <li class="element-content-side-menu">
            <a href="/creation-activites">
                <img src="/img/Document.png" alt="">
                <span>Ajouter activités</span>
            </a>
        </li>
        <li class="element-content-side-menu">
            <a href="">
                <img src="/img/carbon_crop-growth.png" alt="">
                <span>Mes programmes</span>
            </a>
        </li>
        <li class="element-content-side-menu">
            <a href="">
                <img src="/img/group-line-1.png" alt="">
                <span>Mes Apprenants</span>
            </a>
        </li>
        <li class="element-content-side-menu">
            <a href="">
                <img src="/img/carbon_location-company-filled.png" alt="">
                <span>Mes Clients</span>
            </a>
        </li>
        <li class="element-content-side-menu">
            <a href="">
                <img src="/img/la_wallet-solid.png" alt="">
                <span>Transactions</span>
            </a>
        </li>
        <li class="element-content-side-menu">
            <a href="">
                <img src="/img/Chat.png" alt="">
                <span>Chat</span>
            </a>
        </li>
        <li class="element-content-side-menu">
            <a href="">
                <img src="/img/Setting.png" alt="">
                <span>Paramétres</span>
            </a>
        </li>
    </ul>
</div>
