<!-- Valeur Field -->
<div class="col-sm-12">
    {!! Form::label('valeur', 'Valeur:') !!}
    <p>{{ $responseUserChallenge->valeur }}</p>
</div>

<!-- Question Challenge Id Field -->
<div class="col-sm-12">
    {!! Form::label('question_challenge_id', 'Question Challenge Id:') !!}
    <p>{{ $responseUserChallenge->question_challenge_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $responseUserChallenge->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $responseUserChallenge->updated_at }}</p>
</div>

