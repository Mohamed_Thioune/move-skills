<div class="table-responsive">
    <table class="table" id="responseUserChallenges-table">
        <thead>
        <tr>
            <th>Valeur</th>
        <th>Question Challenge Id</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($responseUserChallenges as $responseUserChallenge)
            <tr>
                <td>{{ $responseUserChallenge->valeur }}</td>
            <td>{{ $responseUserChallenge->question_challenge_id }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['responseUserChallenges.destroy', $responseUserChallenge->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('responseUserChallenges.show', [$responseUserChallenge->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('responseUserChallenges.edit', [$responseUserChallenge->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
