<!doctype html>
<html lang="en">
<head>
    <title>Login 08</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">

</head>
<body>
<section class="">
    <div class="row rowModife">
        <div class="col-lg-6 colModife">
            <div class="blockImgLogin"></div>
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="login-wrap">
                <div class="icon d-flex align-items-center justify-content-center">
                    <img src="/img/Logo.png" alt="">
                </div>
                <h3 class="text-center mb-4">Vous avez un compte ?</h3>
                <form action="#" class="login-form">
                    <div class="form-group">
                        <input type="text" class="form-control rounded-left" placeholder="Nom d'utilisateur" required>
                    </div>
                    <div class="form-group d-flex">
                        <input type="password" class="form-control rounded-left" placeholder="Mot de pass" required>
                    </div>
                    <div class="form-group d-md-flex flex-wrap">
                        <div class="w-50">
                            <label class="checkbox-wrap checkbox-primary">Se souvenir de moi
                                <input type="checkbox" checked>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="w-50 text-md-right">
                            <a href="#">Mot de pas oublié ?</a>
                        </div>
                        <div class="w-100 text-center">
                            <a href="{{ ('inscription') }}">Vous n'avez pas de compte ?</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary rounded submit p-3 px-5">Se connecter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script src="/js/jquery.min.js"></script>
<script src="/js/popper.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script>

</body>
</html>

