<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Certificate</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Certificate completion">
    <link href="https://fonts.cdnfonts.com/css/open-sans" rel="stylesheet">
</head>

<style> 
.content {
    background: #FDFDFD;
    font-family: 'Open Sans', sans-serif;
}

.certificat {
    max-width: 660px;
    margin: 0 auto;
    box-shadow: 1px 0px 5px 0px rgb(13 13 13 / 23%);
    margin-bottom: 60px;
    margin-top: 30px;
}

.head-certificat {
    background: #109b39;
    position: relative;
    text-align: center;
    padding: 10px;
}

.head-certificat img {
    position: absolute;
    left: 15px;
    width: 100px;
}

.title-certificat {
    font-size: 30px;
    font-weight: 400;
    font-style: oblique;
    color: white;
    margin-bottom: 0;
    margin-top: 0;
}

.given-text {
    font-size: 18px;
    font-weight: 600;
    margin-bottom: 1px;
}

.name-recipiendaire {
    font-size: 38px;
    font-weight: bold;
    margin-top: 5px;
    margin-bottom: 8px;
}

.text-center {
    text-align: center;
}

.cause-text {
    font-size: 20px;
    margin-top: 0;
}

.date {
    font-weight: 700;
    font-size: 16px;
    margin-top: 0;
    margin-bottom: 25px;
}

.block-admin-name {
    display: flex;
    justify-content: space-between;
    margin-top: 0;
    margin-bottom: 10px;
}

.name-administration {
    font-size: 18px;
    margin-bottom: 0;
    margin-top: 0;
}

.footer-certificat {
    height: 48px;
    background: #109b39;
    margin-top: 0;
}

.partage-text {
    max-width: 539px;
    margin: 0 auto 30px;
    width: 100%;
    font-size: 18px;
    margin-top: 0;
}

.group-btn {
    display: flex;
    justify-content: center;
    margin-top: 0;
}

.btn-telecharger {
    background: #109b39;
    margin: 0 10px;
    font-size: 18px;
    color: white;
    box-shadow: 0px 16px 30px rgb(77 197 145 / 30%);
    border-radius: 5px;
    border: none;
    padding: 8px 20px;
}

.btn-partager {
    background: #9BA1FF;
    margin: 0 10px;
    font-size: 18px;
    color: white;
    box-shadow: 0px 16px 30px rgb(77 197 145 / 30%);
    border-radius: 5px;
    padding: 8px 20px;
    border: none;
}

.body-certificat {
    padding: 30px;
}

p {
    margin-top: 0;
}
</style>

@php
$post_date = explode(' ', $certification->created_at);
$coach = DB::table('users')
        ->select('users.nom', 'users.prenom', 'users.avatar')
        ->join('formateurs', 'users.id', 'formateurs.user_id')
        ->where('formateurs.id', $certification->parcours_formation->formateur_id)
        ->first();
@endphp
<body>
    <div class="content">
        <div class="certificat">
            <div class="head-certificat"><br><br>
                <center><img src="{{ public_path('/img/Logo.png') }}" alt=""></center>
                <p class="title-certificat">CERTIFICAT D'ACHÈVEMENT</p>
            </div>
            <div class="body-certificat text-center">
                <p class="given-text">CETTE ATTESTATION EST DÉCERNÉ À </p>
                <p class="name-recipiendaire">{{ $certification->user->prenom }} {{ $certification->user->nom  }}</p>
                <p class="cause-text">Pour avoir complété le parcours de formation :<br> <b style="text-decoration: underline;">{{ $certification->parcours_formation->libelle }}</b></p>
                <p class="date"><span>Date de délivrance</span> : Le {{ $post_date[0] }}</p>
                <div class="block-admin-name">
                    <p class="name-administration"> <b>{{ $coach->nom }}</b> <br>{{ $coach->prenom }}</p>
                </div>
            </div>
            <div class="footer-certificat"></div>
        </div>

        {{--
            <p class="partage-text">Tu peux partager votre certificat sur les réseaux sociaux et <b>remporter 20 points de croissance supplémentaires</b> </p>
            <div class="group-btn">
                <button class="btn btn-telecharger">Télécharger </button>
                <button class="btn btn-partager">Partager </button>
            </div>
        --}}

    </div>
</body>

</html>
