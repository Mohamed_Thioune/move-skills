<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{{ $domaine->libelle }}</p>
</div>

<!-- Parent Field -->
<div class="col-sm-12">
    {!! Form::label('parent', 'Parent:') !!}
    <p>{{ $domaine->parent }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $domaine->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $domaine->updated_at }}</p>
</div>

