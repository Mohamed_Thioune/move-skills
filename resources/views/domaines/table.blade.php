<div class="table-responsive">
    <table class="table" id="domaines-table">
        <thead>
        <tr>
            <th>Libelle</th>
        <th>Parent</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($domaines as $domaine)
            <tr>
                <td>{{ $domaine->libelle }}</td>
            <td>{{ $domaine->parent }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['domaines.destroy', $domaine->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('domaines.show', [$domaine->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('domaines.edit', [$domaine->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
