<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $challenge->title }}</p>
</div>

<!-- Parcours Formation Id Field -->
<div class="col-sm-12">
    {!! Form::label('parcours_formation_id', 'Parcours Formation Id:') !!}
    <p>{{ $challenge->parcours_formation_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $challenge->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $challenge->updated_at }}</p>
</div>

