<?php

namespace Database\Factories;

use App\Models\ApprenantCompetence;
use Illuminate\Database\Eloquent\Factories\Factory;

class ApprenantCompetenceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ApprenantCompetence::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'unsignedInteger' => $this->faker->word,
        'unsignedInteger' => $this->faker->word,
        'state' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
