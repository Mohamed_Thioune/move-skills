<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParcourActivitesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcour_activites', function (Blueprint $table) {
            $table->id('id');
            $table->string('type');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('parcours_id');
            $table->unsignedInteger('activity�_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parcour_activites');
    }
}
