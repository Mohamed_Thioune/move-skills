<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/connexion', 'App\Http\Controllers\AdminController@connexion');
Route::post('/connexion', 'App\Http\Controllers\AdminController@login')->name('connecter');
Route::get('/inscription', 'App\Http\Controllers\AdminController@inscription');
Route::post('/inscription_store', 'App\Http\Controllers\AdminController@inscription_store')->name('inscription_store');

Route::group(['middleware' => 'Connecter'], function(){
    Route::get('/admin/dashboard', 'App\Http\Controllers\AdminController@dashboard');

    Route::get('/mes-parcours', 'App\Http\Controllers\ParcourFormationController@mes_parcours');
    Route::get('/creation-parcours-formation', 'App\Http\Controllers\ParcourFormationController@create_parcours');
    Route::get('/creation-parcours-formation-activite', 'App\Http\Controllers\ParcourFormationController@create_parcours_activite');
    Route::post('/creation-parcours-formationStore', 'App\Http\Controllers\ParcourFormationController@store_parcours')->name('store_parcours_formation');

    Route::get('/mes-activites', 'App\Http\Controllers\ActiviteController@mes_activites');
    Route::get('/creation-activites', 'App\Http\Controllers\ActiviteController@create_activites');
    Route::post('/creation-activitesStore', 'App\Http\Controllers\ActiviteController@store_activites')->name('store_activites');

    Route::delete('/suprime-activites/{id}', 'App\Http\Controllers\ActiviteController@destroy_activites')->name('delete_activites');
    Route::delete('/suprime-parcours/{id}', 'App\Http\Controllers\ParcourFormationController@destroy_parcour')->name('delete_parcour');
});

Route::get('/', function () {
    return view('login');
});
Route::get('/login', function () {
    return view('login');
});
Route::get('/login1', function () {
    return view('login1');
});
Route::get('/register', function () {
    return view('register');
});
Route::get('/forgetPasswoord', function () {
    return view('forgetPasswoord');
});

Route::get('/reset-code-promo', function () {
    $users = DB::table('users')->get();
     
    foreach($users as $user)
    {
        // generate a pin based on 2 * 2 digits + a random character
        $code_rand = mt_rand(10, 99)
                   . mt_rand(10, 99);
       
        //shuffle the result
        $uni_prenom = substr($user->prenom, 0, 1);
        $uni_nom = substr($user->nom, 0, 1);
        $code = strtoupper($uni_nom) . strtoupper($uni_prenom) . $code_rand;

        DB::table('users')->where('id', $user->id)->update(['code_promo' => $code]);
    }
    dd('code promo reset for all users successfully !');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('apprenants', App\Http\Controllers\ApprenantController::class);

Route::resource('/certifications', App\Http\Controllers\CertificationsController::class);

Route::resource('canvaMiniDisqs', App\Http\Controllers\CanvaMiniDisqController::class);

Route::resource('points', App\Http\Controllers\PointsController::class);

Route::resource('passerTests', App\Http\Controllers\PasserTestController::class);


Route::resource('questions', App\Http\Controllers\QuestionController::class);


Route::resource('domaines', App\Http\Controllers\DomaineController::class);


Route::resource('miniDisqs', App\Http\Controllers\MiniDisqController::class);


Route::resource('responses', App\Http\Controllers\ResponseController::class);


Route::resource('responseByUserDiscs', App\Http\Controllers\ResponseByUserDiscController::class);


Route::resource('roueVies', App\Http\Controllers\RoueVieController::class);


Route::resource('responseByUsers', App\Http\Controllers\ResponseByUserController::class);


Route::resource('apprenantDomains', App\Http\Controllers\ApprenantDomainController::class);


Route::resource('caracteristiques', App\Http\Controllers\CaracteristiqueController::class);


Route::resource('videos', App\Http\Controllers\VideoController::class);


Route::resource('audio', App\Http\Controllers\AudioController::class);


Route::resource('articles', App\Http\Controllers\ArticleController::class);


Route::resource('parcoursFormations', App\Http\Controllers\ParcoursFormationController::class);


Route::resource('quizzs', App\Http\Controllers\QuizzController::class);


Route::resource('responsesQuizzs', App\Http\Controllers\ResponsesQuizzController::class);


Route::resource('questionsQuizzs', App\Http\Controllers\QuestionsQuizzController::class);


Route::resource('responseUserQuizzs', App\Http\Controllers\ResponseUserQuizzController::class);


Route::resource('challenges', App\Http\Controllers\challengeController::class);


Route::resource('questionChallenges', App\Http\Controllers\QuestionChallengeController::class);


Route::resource('responseUserChallenges', App\Http\Controllers\ResponseUserChallengeController::class);


Route::resource('commentaires', App\Http\Controllers\CommentaireController::class);


Route::resource('citations', App\Http\Controllers\CitationController::class);



Route::resource('sentMails', App\Http\Controllers\SentMailController::class);


Route::resource('validationEmails', App\Http\Controllers\ValidationEmailController::class);


Route::resource('apprenantCompetences', App\Http\Controllers\ApprenantCompetenceController::class);


Route::resource('competences', App\Http\Controllers\CompetenceController::class);


Route::resource('podcasts', App\Http\Controllers\PodcastController::class);


Route::resource('parcourActivites', App\Http\Controllers\ParcourActiviteController::class);
