<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/logout', [App\Http\Controllers\API\LoginController::class, 'logout'])->middleware('EnsureTokenIsValid')->name('api.logout');
Route::post('/signup/me', [App\Http\Controllers\API\LoginController::class, 'register'])->name('api.signup');
Route::post('/signin/me', [App\Http\Controllers\API\LoginController::class, 'login'])->name('api.signin');
Route::post('/oauth/token', [App\Http\Controllers\API\LoginController::class, 'refresh_token'])->name('api.refresh');
Route::post('social/login/{provider}', [App\Http\Controllers\API\LoginController::class, 'socialLogin']);
Route::get('/cinetpay/response', [App\Http\Controllers\API\LoginController::class, 'callback_cinetpay'])->name('cinetpay.callback');
Route::post('/paydunya/response', [App\Http\Controllers\API\LoginController::class, 'callback_paydunya'])->name('paydunya.callback');
Route::post('/command/invoice', [App\Http\Controllers\API\LoginController::class, 'facture_commands'])->middleware('EnsureTokenIsValid')->name('commands.facture');
Route::get('/command/historique', [App\Http\Controllers\API\LoginController::class, 'historiq_commands'])->middleware('EnsureTokenIsValid')->name('commands.historique');


Route::get('/login/{provider}', [App\Http\Controllers\API\LoginController::class,'redirectToProvider']);
Route::get('/login/{provider}/callback', [App\Http\Controllers\API\LoginController::class,'handleProviderCallback']);

Route::get('/question/roueVie', [App\Http\Controllers\API\QuestionAPIController::class, 'index_roue_de_vie'])->middleware('EnsureTokenIsValid')->name('roueVie.question');
Route::post('/domain/roueVie', [App\Http\Controllers\API\ResponseByUserAPIController::class, 'domaine_roue_vie'])->middleware('EnsureTokenIsValid')->name('roueVie.domain');
Route::post('/domain/priorities', [App\Http\Controllers\API\ResponseByUserAPIController::class, 'domaine_priorities'])->middleware('EnsureTokenIsValid')->name('roueVie.priority');

Route::get('/question/disque', [App\Http\Controllers\API\QuestionAPIController::class, 'index_mini_disq'])->middleware('EnsureTokenIsValid')->name('minidisq.question');
Route::post('/getYourAnimal', [App\Http\Controllers\API\ResponseByUserDiscAPIController::class, 'get_your_animal'])->middleware('EnsureTokenIsValid')->name('minidisq.animal');
Route::get('/competence/couleur', [App\Http\Controllers\API\ResponseByUserDiscAPIController::class, 'couleur_competence'])->middleware('EnsureTokenIsValid')->name('minidisq.competence');
Route::post('/competence/disque/selection', [App\Http\Controllers\API\ResponseByUserDiscAPIController::class, 'competence_selection'])->middleware('EnsureTokenIsValid');

Route::get('/question/competence', [App\Http\Controllers\API\QuestionAPIController::class, 'index_competence'])->middleware('EnsureTokenIsValid')->name('competence.question');
Route::post('/competence/all', [App\Http\Controllers\API\ResponseByUserAPIController::class, 'competences'])->middleware('EnsureTokenIsValid')->name('competence.see');
Route::get('/competence/domaine', [App\Http\Controllers\API\ResponseByUserAPIController::class, 'domaine_competence'])->middleware('EnsureTokenIsValid')->name('competence.domaine');
Route::post('/competence/selection', [App\Http\Controllers\API\ResponseByUserAPIController::class, 'competence_selection'])->middleware('EnsureTokenIsValid');

Route::post('/quizz/stats', [App\Http\Controllers\API\ResponseUserQuizzAPIController::class, 'stats'])->middleware('EnsureTokenIsValid')->name('quizzs.stats');
Route::get('/profile/points', [App\Http\Controllers\API\PointsAPIController::class, 'profile_points'])->middleware('EnsureTokenIsValid')->name('profile.points');

Route::get('/user/dashboard', [App\Http\Controllers\API\UserAPIController::class, 'home'])->middleware('EnsureTokenIsValid')->name('users.dashboard');

Route::get('/user', [App\Http\Controllers\API\UserAPIController::class, 'show'])->middleware('EnsureTokenIsValid')->name('users.show');
Route::post('/user/update', [App\Http\Controllers\API\UserAPIController::class, 'update'])->middleware('EnsureTokenIsValid')->name('users.update');
Route::post('/user/update/password', [App\Http\Controllers\API\UserAPIController::class, 'update_password'])->middleware('EnsureTokenIsValid')->name('users.password');
Route::post('/user/forgot/password', [App\Http\Controllers\API\LoginController::class, 'forgot_password'])->name('user.password.forgot');
Route::post('/user/code/password', [App\Http\Controllers\API\LoginController::class, 'match_code'])->name('user.password.code');
Route::post('/user/apply/promo', [App\Http\Controllers\API\LoginController::class, 'apply_promo'])->middleware('EnsureTokenIsValid')->name('user.promo.code');
Route::post('/user/reset/password', [App\Http\Controllers\API\LoginController::class, 'reset_password'])->name('user.password.reset');
Route::delete('/user/delete', [App\Http\Controllers\API\UserAPIController::class, 'destroy'])->middleware('EnsureTokenIsValid')->name('users.delete');
Route::get('/user/points', [App\Http\Controllers\API\UserAPIController::class, 'get_current_points'])->middleware('EnsureTokenIsValid')->name('users.points.gained');

Route::get('passer_tests/all', [App\Http\Controllers\API\PasserTestAPIController::class, 'store_all'])->middleware('EnsureTokenIsValid')->name('passer_test.state');

Route::get('certification/{parcours_formation_id}',[App\Http\Controllers\API\CertificationAPIController::class, 'store'])->middleware('EnsureTokenIsValid');
Route::get('certification',[App\Http\Controllers\API\CertificationAPIController::class, 'index'])->middleware('EnsureTokenIsValid');

Route::get('/user/recap/tests', [App\Http\Controllers\API\UserAPIController::class, 'recap'])->middleware('EnsureTokenIsValid')->name('users.password');

Route::get('/see-my-guests', [App\Http\Controllers\API\LoginController::class, 'see_my_guests'])->middleware('EnsureTokenIsValid');

Route::post('/activity/read/{id}', [App\Http\Controllers\API\PodcastAPIController::class, 'read'])->middleware('EnsureTokenIsValid')->name('activity.read');

Route::get('/formations/list/{id}', [App\Http\Controllers\API\ParcoursFormationAPIController::class, 'formations'])->middleware('EnsureTokenIsValid')->name('formations.list');
Route::get('/podcasts/list/{id}', [App\Http\Controllers\API\ParcoursFormationAPIController::class, 'podcasts'])->middleware('EnsureTokenIsValid')->name('podcasts.list');
Route::get('/challenges/list/{id}', [App\Http\Controllers\API\ParcoursFormationAPIController::class, 'challenges'])->middleware('EnsureTokenIsValid')->name('formations.list');

 
/*
  * * Resources
*/
Route::resource('passer_tests', App\Http\Controllers\API\PasserTestAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('questions', App\Http\Controllers\API\QuestionAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('response_by_user_disque', App\Http\Controllers\API\ResponseByUserDiscAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('response_by_user', App\Http\Controllers\API\ResponseByUserAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('apprenant_domains', App\Http\Controllers\API\ApprenantDomainAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('videos', App\Http\Controllers\API\VideoAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('audios', App\Http\Controllers\API\AudioAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('articles', App\Http\Controllers\API\ArticleAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('parcours_formations', App\Http\Controllers\API\ParcoursFormationAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('response_quizzs', App\Http\Controllers\API\ResponsesQuizzAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('question_quizzs', App\Http\Controllers\API\QuestionQuizzAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('response_user_quizzs', App\Http\Controllers\API\ResponseUserQuizzAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('challenges', App\Http\Controllers\API\ChallengeAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('question_challenges', App\Http\Controllers\API\QuestionChallengeAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('response_user_challenges', App\Http\Controllers\API\ResponseUserChallengeAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('commentaires', App\Http\Controllers\API\CommentaireAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('citations', App\Http\Controllers\API\CitationAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('podcasts', App\Http\Controllers\API\PodcastAPIController::class)->middleware('EnsureTokenIsValid');
Route::resource('cinetpay_transactions', App\Http\Controllers\API\CinetpayTransactionAPIController::class)->middleware('EnsureTokenIsValid');

//Route::resource('canva_mini_disqs', App\Http\Controllers\API\CanvaMiniDisqAPIController::class);
//Route::resource('caracteristiques', App\Http\Controllers\API\CaracteristiqueAPIController::class);
//Route::resource('quizzs', App\Http\Controllers\API\QuizzAPIController::class);
//Route::resource('challenges', App\Http\Controllers\API\ChallengeAPIController::class);
//Route::resource('points', App\Http\Controllers\API\PointsAPIController::class);
//Route::resource('domaines', App\Http\Controllers\API\DomaineAPIController::class);
//Route::resource('apprenants', App\Http\Controllers\API\ApprenantAPIController::class);
//Route::resource('mini_disqs', App\Http\Controllers\API\MiniDisqAPIController::class);
//Route::resource('responses', App\Http\Controllers\API\ResponseAPIController::class);
//Route::resource('roue_vies', App\Http\Controllers\API\RoueVieAPIController::class);
//Route::resource('citations', App\Http\Controllers\API\CitationAPIController::class);
//Route::resource('sent_mails', App\Http\Controllers\API\SentMailAPIController::class);
//Route::resource('validation_emails', App\Http\Controllers\API\ValidationEmailAPIController::class);
//Route::resource('citations', App\Http\Controllers\API\CitationAPIController::class);
// Route::resource('apprenant_competences', App\Http\Controllers\API\ApprenantCompetenceAPIController::class);
// Route::resource('competences', App\Http\Controllers\API\CompetenceAPIController::class);
//Route::resource('parcour_activites', App\Http\Controllers\API\ParcourActiviteAPIController::class);
