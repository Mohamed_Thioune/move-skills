<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ValidationEmail;

class ValidationEmailApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_validation_email()
    {
        $validationEmail = ValidationEmail::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/validation_emails', $validationEmail
        );

        $this->assertApiResponse($validationEmail);
    }

    /**
     * @test
     */
    public function test_read_validation_email()
    {
        $validationEmail = ValidationEmail::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/validation_emails/'.$validationEmail->id
        );

        $this->assertApiResponse($validationEmail->toArray());
    }

    /**
     * @test
     */
    public function test_update_validation_email()
    {
        $validationEmail = ValidationEmail::factory()->create();
        $editedValidationEmail = ValidationEmail::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/validation_emails/'.$validationEmail->id,
            $editedValidationEmail
        );

        $this->assertApiResponse($editedValidationEmail);
    }

    /**
     * @test
     */
    public function test_delete_validation_email()
    {
        $validationEmail = ValidationEmail::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/validation_emails/'.$validationEmail->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/validation_emails/'.$validationEmail->id
        );

        $this->response->assertStatus(404);
    }
}
