<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SentMail;

class SentMailApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_sent_mail()
    {
        $sentMail = SentMail::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/sent_mails', $sentMail
        );

        $this->assertApiResponse($sentMail);
    }

    /**
     * @test
     */
    public function test_read_sent_mail()
    {
        $sentMail = SentMail::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/sent_mails/'.$sentMail->id
        );

        $this->assertApiResponse($sentMail->toArray());
    }

    /**
     * @test
     */
    public function test_update_sent_mail()
    {
        $sentMail = SentMail::factory()->create();
        $editedSentMail = SentMail::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/sent_mails/'.$sentMail->id,
            $editedSentMail
        );

        $this->assertApiResponse($editedSentMail);
    }

    /**
     * @test
     */
    public function test_delete_sent_mail()
    {
        $sentMail = SentMail::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/sent_mails/'.$sentMail->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/sent_mails/'.$sentMail->id
        );

        $this->response->assertStatus(404);
    }
}
