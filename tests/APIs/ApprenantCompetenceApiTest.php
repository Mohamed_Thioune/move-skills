<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ApprenantCompetence;

class ApprenantCompetenceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_apprenant_competence()
    {
        $apprenantCompetence = ApprenantCompetence::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/apprenant_competences', $apprenantCompetence
        );

        $this->assertApiResponse($apprenantCompetence);
    }

    /**
     * @test
     */
    public function test_read_apprenant_competence()
    {
        $apprenantCompetence = ApprenantCompetence::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/apprenant_competences/'.$apprenantCompetence->id
        );

        $this->assertApiResponse($apprenantCompetence->toArray());
    }

    /**
     * @test
     */
    public function test_update_apprenant_competence()
    {
        $apprenantCompetence = ApprenantCompetence::factory()->create();
        $editedApprenantCompetence = ApprenantCompetence::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/apprenant_competences/'.$apprenantCompetence->id,
            $editedApprenantCompetence
        );

        $this->assertApiResponse($editedApprenantCompetence);
    }

    /**
     * @test
     */
    public function test_delete_apprenant_competence()
    {
        $apprenantCompetence = ApprenantCompetence::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/apprenant_competences/'.$apprenantCompetence->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/apprenant_competences/'.$apprenantCompetence->id
        );

        $this->response->assertStatus(404);
    }
}
