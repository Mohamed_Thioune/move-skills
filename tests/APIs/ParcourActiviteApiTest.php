<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ParcourActivite;

class ParcourActiviteApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_parcour_activite()
    {
        $parcourActivite = ParcourActivite::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/parcour_activites', $parcourActivite
        );

        $this->assertApiResponse($parcourActivite);
    }

    /**
     * @test
     */
    public function test_read_parcour_activite()
    {
        $parcourActivite = ParcourActivite::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/parcour_activites/'.$parcourActivite->id
        );

        $this->assertApiResponse($parcourActivite->toArray());
    }

    /**
     * @test
     */
    public function test_update_parcour_activite()
    {
        $parcourActivite = ParcourActivite::factory()->create();
        $editedParcourActivite = ParcourActivite::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/parcour_activites/'.$parcourActivite->id,
            $editedParcourActivite
        );

        $this->assertApiResponse($editedParcourActivite);
    }

    /**
     * @test
     */
    public function test_delete_parcour_activite()
    {
        $parcourActivite = ParcourActivite::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/parcour_activites/'.$parcourActivite->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/parcour_activites/'.$parcourActivite->id
        );

        $this->response->assertStatus(404);
    }
}
