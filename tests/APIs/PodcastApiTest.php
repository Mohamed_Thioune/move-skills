<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Podcast;

class PodcastApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_podcast()
    {
        $podcast = Podcast::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/podcasts', $podcast
        );

        $this->assertApiResponse($podcast);
    }

    /**
     * @test
     */
    public function test_read_podcast()
    {
        $podcast = Podcast::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/podcasts/'.$podcast->id
        );

        $this->assertApiResponse($podcast->toArray());
    }

    /**
     * @test
     */
    public function test_update_podcast()
    {
        $podcast = Podcast::factory()->create();
        $editedPodcast = Podcast::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/podcasts/'.$podcast->id,
            $editedPodcast
        );

        $this->assertApiResponse($editedPodcast);
    }

    /**
     * @test
     */
    public function test_delete_podcast()
    {
        $podcast = Podcast::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/podcasts/'.$podcast->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/podcasts/'.$podcast->id
        );

        $this->response->assertStatus(404);
    }
}
